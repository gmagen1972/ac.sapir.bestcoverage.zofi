using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Dhpoware
{
    class Terrain
    {
    #region Nested types
        public struct Region
        {
            public float Min;
            public float Max;
            public Texture2D ColorMap;
        }
    #endregion

    #region Fields
        private GraphicsDevice graphicsDevice;
        private ContentManager content;
        private HeightMap heightMap;
        private List<Region> regions;
        private VertexPositionNormalTexture[] vertices;
        private ushort[] indices16Bit;
        private uint[] indices32Bit;
        private IndexBuffer indexBuffer;
        private DynamicVertexBuffer vertexBuffer;
        private BoundingBox boundingBox;
        private BoundingSphere boundingSphere;
        private Effect effect;
        private Vector3 sunlightDirection;
        private Vector4 sunlightColor;
        private Vector4 terrainAmbient;
        private Vector4 terrainDiffuse;
        private float terrainTilingFactor;
    #endregion

    #region Properties
        public Vector3 SunlightDirection
        {
            get { return sunlightDirection; }
            set { sunlightDirection = value; }
        }

        public Vector4 SunlightColor
        {
            get { return sunlightColor; }
            set { sunlightColor = value; }
        }

        public Vector4 TerrainAmbient
        {
            get { return terrainAmbient; }
            set { terrainAmbient = value; }
        }

        public BoundingBox TerrainBoundingBox
        {
            get { return boundingBox; }
        }

        public BoundingSphere TerrainBoundingSphere
        {
            get { return boundingSphere; }
        }

        public Vector3 TerrainCenter
        {
            get
            {
                Vector3 center;
                float worldSize = heightMap.Size * heightMap.GridSpacing;

                center.X = worldSize * 0.5f;
                center.Z = worldSize * 0.5f;
                center.Y = heightMap.HeightAt(center.X, center.Z);

                return center;
            }
        }

        public Vector4 TerrainDiffuse
        {
            get { return terrainDiffuse; }
            set { terrainDiffuse = value; }
        }

        public HeightMap TerrainHeightMap
        {
            get { return heightMap; }
        }

        public List<Region> TerrainRegions
        {
            get { return regions; }
        }

        public float TerrainSize
        {
            get { return heightMap.Size * heightMap.GridSpacing; }
        }

        public float TerrainTilingFactor
        {
            get { return terrainTilingFactor; }
            set { terrainTilingFactor = value; }
        }
    #endregion

    #region Public methods
        public Terrain(GraphicsDevice graphicsDevice, ContentManager content)
        {
            this.graphicsDevice = graphicsDevice;
            this.content = content;

            regions = new List<Region>();

            sunlightDirection = Vector3.Down;
            sunlightColor = new Vector4(1.0f, 1.0f, 1.0f, 0.0f );
            
            terrainAmbient = new Vector4(0.3f, 0.3f, 0.3f, 1.0f);
            terrainDiffuse = new Vector4(0.7f, 0.7f, 0.7f, 1.0f);
            terrainTilingFactor = 12.0f;
        }

        public void AddRegion(float min, float max, Texture2D colorMapAssetName)
        {
            // Add the new terrain region.
    
            Region region = new Region();

            region.Min = min;
            region.Max = max;
            //region.ColorMap = content.Load<Texture2D>(colorMapAssetName);
            region.ColorMap = colorMapAssetName;
            regions.Add(region);

            // Calculate the new terrain tiling factor.

            float averageColorMapWidth = 0.0f;
            float averageColorMapHeight = 0.0f;

            foreach (Region r in regions)
            {
                averageColorMapWidth += r.ColorMap.Width;
                averageColorMapHeight += r.ColorMap.Height;
            }

            averageColorMapWidth /= regions.Count;
            averageColorMapHeight /= regions.Count;
            
            terrainTilingFactor = (float)Math.Min(averageColorMapWidth, averageColorMapHeight);
            terrainTilingFactor = TerrainSize / terrainTilingFactor;
            terrainTilingFactor = 1;
        }

        public void Create(int size, int gridSpacing, float minHeight, float maxHeight)
        {
            heightMap = new HeightMap(size, gridSpacing, minHeight, maxHeight);
                        
            // Generate the terrain vertices.

            int totalVertices = size * size;
            int totalIndices = (size - 1) * (size - 1) * 6;

            vertices = new VertexPositionNormalTexture[totalVertices];
            vertexBuffer = new DynamicVertexBuffer(graphicsDevice, typeof(VertexPositionNormalTexture), vertices.Length, BufferUsage.WriteOnly);
            
            // Generate the terrain triangle indices.

            if (Using16BitIndices())
            {
                indices16Bit = new ushort[totalIndices];
                GenerateIndices();

                indexBuffer = new IndexBuffer(graphicsDevice, typeof(ushort), indices16Bit.Length, BufferUsage.WriteOnly);
                indexBuffer.SetData(indices16Bit);
            }
            else
            {
                indices32Bit = new uint[totalIndices];
                GenerateIndices();

                indexBuffer = new IndexBuffer(graphicsDevice, typeof(uint), indices32Bit.Length, BufferUsage.WriteOnly);
                indexBuffer.SetData(indices32Bit);
            }

            // Calculate the bounding box and bounding sphere for the terrain.

            Vector3 min, max;

            min.X = 0.0f;
            min.Y = heightMap.MinHeight;
            min.Z = 0.0f;

            max.X = heightMap.Size * heightMap.GridSpacing;
            max.Y = heightMap.MaxHeight;
            max.Z = heightMap.Size * heightMap.GridSpacing;

            boundingBox = new BoundingBox(min, max);
            boundingSphere = BoundingSphere.CreateFromBoundingBox(boundingBox);

            // Load the effect file used to render the terrain.

            try
            {
                effect = content.Load<Effect>(@"Effects\terrain");
            }
            catch (ContentLoadException)
            {
                try
                {
                    effect = content.Load<Effect>("terrain");
                }
                catch (ContentLoadException)
                {
                }
            }
        }


        

        public void Draw(Matrix world, Matrix view, Matrix projection)
        {
            if (vertexBuffer.IsContentLost)
                vertexBuffer.SetData(vertices);

            graphicsDevice.SetVertexBuffer(vertexBuffer);
            graphicsDevice.Indices = indexBuffer;

            UpdateEffect(world, view, projection);

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                graphicsDevice.DrawIndexedPrimitives(
                    PrimitiveType.TriangleList, //PrimitiveType.TriangleStrip,
                    0, 0, vertices.Length, 0,
                    Using16BitIndices() ? indices16Bit.Length / 3 //indices16Bit.Length - 2 
                                        : indices32Bit.Length / 3 //indices32Bit.Length - 2
                );
            }

            graphicsDevice.SetVertexBuffer(null);
            graphicsDevice.Indices = null;
        }



        public void GenerateUsingDiamondSquareFractal(float roughness)
        {
            heightMap.GenerateDiamondSquareFractal(roughness);
            GenerateVertices();
        }


        public void GenerateRandomSurface()
        {
            heightMap.GenerateRandomSurface();
            GenerateVertices();
        }


        public void GenerateXYHeightMap(float[,] heightData)
        {
            heightMap.GenerateXYHeightMap(heightData);
            GenerateVertices();
        }
        

        public void GenerateFlatSurface()
        {
            heightMap.GenerateFlatSurface();
            GenerateVertices();
        }

 

    #endregion

    #region Private methods
        private void GenerateIndices()
        {
            int index = 0;
            int currentVertex = 0;
            int size = heightMap.Size;

            if (Using16BitIndices())
            {
                for (int z = 0; z < size - 1; ++z)
                {
                    for (int x = 0; x < size - 1; ++x)
                    {
                        currentVertex = z * size + x;

                        indices16Bit[index++] = (ushort)(currentVertex);
                        indices16Bit[index++] = (ushort)(currentVertex + 1);
                        indices16Bit[index++] = (ushort)(currentVertex + size);

                        indices16Bit[index++] = (ushort)(currentVertex + 1);
                        indices16Bit[index++] = (ushort)(currentVertex + size + 1);
                        indices16Bit[index++] = (ushort)(currentVertex + size);
                    }
                }
            }
            else
            {
                for (int z = 0; z < size - 1; ++z)
                {
                    for (int x = 0; x < size - 1; ++x)
                    {
                        currentVertex = z * size + x;

                        indices32Bit[index++] = (uint)(currentVertex);
                        indices32Bit[index++] = (uint)(currentVertex + 1);
                        indices32Bit[index++] = (uint)(currentVertex + size);

                        indices32Bit[index++] = (uint)(currentVertex + 1);
                        indices32Bit[index++] = (uint)(currentVertex + size + 1);
                        indices32Bit[index++] = (uint)(currentVertex + size);
                    }
                }
            }
        }

        private void GenerateIndicesTriangleStrip()
        {
            if (Using16BitIndices())
            {
                int index = 0;
                int size = heightMap.Size;

                for (int z = 0; z < size - 1; ++z)
                {
                    if (z % 2 == 0)
                    {
                        for (int x = 0; x < size; ++x)
                        {
                            indices16Bit[index++] = (ushort)(x + (z + 1) * size);
                            indices16Bit[index++] = (ushort)(x + z * size);
                        }

                        // Add degenerate triangles to stitch strips together.
                        indices16Bit[index++] = (ushort)((size - 1) + (z + 1) * size);
                    }
                    else
                    {
                        for (int x = size - 1; x >= 0; --x)
                        {
                            indices16Bit[index++] = (ushort)(x + (z + 1) * size);
                            indices16Bit[index++] = (ushort)(x + z * size);
                        }

                        // Add degenerate triangles to stitch strips together.
                        indices16Bit[index++] = (ushort)((z + 1) * size);
                    }
                }
            }
            else
            {
                int index = 0;
                int size = heightMap.Size;

                for (int z = 0; z < size - 1; ++z)
                {
                    if (z % 2 == 0)
                    {
                        for (int x = 0; x < size; ++x)
                        {
                            indices32Bit[index++] = (uint)(x + (z + 1) * size);
                            indices32Bit[index++] = (uint)(x + z * size);
                        }

                        // Add degenerate triangles to stitch strips together.
                        indices32Bit[index++] = (uint)((size - 1) + (z + 1) * size);
                    }
                    else
                    {
                        for (int x = size - 1; x >= 0; --x)
                        {
                            indices32Bit[index++] = (uint)(x + (z + 1) * size);
                            indices32Bit[index++] = (uint)(x + z * size);
                        }

                        // Add degenerate triangles to stitch strips together.
                        indices32Bit[index++] = (uint)((z + 1) * size);
                    }
                }
            }
        }

        private void GenerateVertices()
        {
            int index = 0;
            int size = heightMap.Size;
            int gridSpacing = heightMap.GridSpacing;
            Vector3 position;
            Vector3 normal;
            Vector2 texCoord;

            for (int z = 0; z < size; ++z)
            {
                for (int x = 0; x < size; ++x)
                {
                    index = z * size + x;
                    
                    position.X = (float)(x * gridSpacing);
                    position.Y = heightMap.HeightAtPixel(x, z);
                    position.Z = (float)(z * gridSpacing);

                    heightMap.NormalAtPixel(x, z, out normal);

                    texCoord.X = (float)x / (float)size;
                    texCoord.Y = (float)z / (float)size;

                    vertices[index] = new VertexPositionNormalTexture(position, normal, texCoord);
                }
            }

            vertexBuffer.SetData(vertices);
        }

        private void UpdateEffect(Matrix world, Matrix view, Matrix projection)
        {
            Matrix worldInvTrans = Matrix.Transpose(Matrix.Invert(world));
            Matrix worldViewProj = world * view * projection;

            effect.Parameters["world"].SetValue(world);
            effect.Parameters["worldInvTrans"].SetValue(worldInvTrans);
            effect.Parameters["worldViewProjection"].SetValue(worldViewProj);

            effect.Parameters["sunlightDir"].SetValue(sunlightDirection);
            effect.Parameters["sunlightColor"].SetValue(sunlightColor);
            
            effect.Parameters["terrainAmbient"].SetValue(terrainAmbient);
            effect.Parameters["terrainDiffuse"].SetValue(terrainDiffuse);
            
            effect.Parameters["terrainTilingFactor"].SetValue(terrainTilingFactor);

            effect.Parameters["terrainRegion1"].SetValue(new Vector2(regions[0].Min, regions[0].Max));
            effect.Parameters["region1ColorMapTexture"].SetValue(regions[0].ColorMap);

            //effect.Parameters["terrainRegion2"].SetValue(new Vector2(regions[1].Min, regions[1].Max));
            //effect.Parameters["region2ColorMapTexture"].SetValue(regions[1].ColorMap);

            //effect.Parameters["terrainRegion3"].SetValue(new Vector2(regions[2].Min, regions[2].Max));
            //effect.Parameters["region3ColorMapTexture"].SetValue(regions[2].ColorMap);

            //effect.Parameters["terrainRegion4"].SetValue(new Vector2(regions[3].Min, regions[3].Max));
            //effect.Parameters["region4ColorMapTexture"].SetValue(regions[3].ColorMap);

            effect.CurrentTechnique = effect.Techniques["TerrainTexturing"];
        }

        private bool Using16BitIndices()
        {
            return vertices.Length <= ushort.MaxValue;
        }
    #endregion
    }
}