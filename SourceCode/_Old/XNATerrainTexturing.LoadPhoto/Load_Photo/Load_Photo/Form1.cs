﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Load_Photo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Load_Photo_to_Project(object sender, EventArgs e)
        {
            Process process1 = new Process();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string[] StrFiles = new string[2];
            string[] FilesName = new string[] { "HeightMap.asc", "HeightMap.jpg" };
            int i;
            string destFile;
            string str = "XNATerrainTexturing.exe";
            destFile = Application.StartupPath + "\\Maps";
            // Set filter options and filter index.
            openFileDialog1.Filter = "Photo (*.asc;*.jpg)|*.asc;*.jpg";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;
            openFileDialog1.ShowDialog();
            StrFiles = openFileDialog1.FileNames;
           
            i=0;
            foreach (string s in StrFiles)
            {
                // Use static Path methods to extract only the file name from the path.
                //fileName = System.IO.Path.GetFileName(s);
                //destFile = System.IO.Path.Combine(targetPath, fileName);
                try
                {
                    System.IO.File.Copy(s, destFile + "\\" + FilesName[i], true);
                    i++;
                }
                catch (System.IO.IOException err)
                {
                    System.Windows.Forms.MessageBox.Show(err.Message);
                    return;
                }

                if (i == 2)
                {
                    System.Diagnostics.Process.Start("XnaTerrainTexturing.exe");
                    //System.Windows.Forms.MessageBox.Show("All Files have been uploaded successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (i == 2)
                {
                    // System.Windows.Forms.MessageBox.Show("All Files have been uploaded successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    process1.StartInfo.FileName = str;
                    process1.Start();
                }

            }
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Left = 50;
            this.Top = 50;
        }
   
    }
}
