﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class FrmMFirearm : Form
    {
        DataSet2TableAdapters.FirearmsTableAdapter Ds = new DataSet2TableAdapters.FirearmsTableAdapter();
        int Count;
        CheckBox[] CB;
        TextBox[] TB;

        public FrmMFirearm()
        {
            InitializeComponent();
        }

        private void FrmMFirearm_Load(object sender, EventArgs e)
        {
            Count=Convert.ToInt16( Ds.GetCount());
             CB= new CheckBox[Count];
             TB= new TextBox[Count];

            for (int x = 0; x < Count; x++)
            {
              

                CB[x] = new CheckBox();
                CB[x].Top = 30 + x * 30;
                CB[x].Left = 70;
                CB[x].Width = 300;
                CB[x].Text = ClsGlobal.FireArmTable[x, 0];

                TB[x] = new TextBox();
                TB[x].Top = 30 + x * 30;
                TB[x].Left =30;
                TB[x].Width = 20;
                TB[x].Height = 6;
                TB[x].BackColor = Color.FromName(ClsGlobal.FireArmTable[x, 3]);
                TB[x].Enabled = false;


                this.Controls.Add(CB[x]);
                this.Controls.Add(TB[x]);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int x = 0; x < Count; x++)
            {
                ClsGlobal.FireArmTable[x, 5] = CB[x].Checked.ToString();
               
            }

            this.Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
