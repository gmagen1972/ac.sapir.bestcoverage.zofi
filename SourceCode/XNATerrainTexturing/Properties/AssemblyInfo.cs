﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("XNA 3.0 Terrain Texturing")]
[assembly: AssemblyProduct("XNATerrainTexturing")]
[assembly: AssemblyDescription("XNA 3.0 Terrain Texturing")]
[assembly: AssemblyCompany("dhpoware")]

[assembly: AssemblyCopyright("Copyright © 2008 dhpoware")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fb6faea6-5ac0-438f-91f0-57517828dfb8")]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
