﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class FrmFirearms : Form
    {
        public FrmFirearms()
        {
            InitializeComponent();
        }

        private void firearmsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.firearmsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet2);

        }

        private void FrmFirearms_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet2.Firearms' table. You can move, or remove it, as needed.
            this.firearmsTableAdapter.Fill(this.dataSet2.Firearms);

        }
    }
}
