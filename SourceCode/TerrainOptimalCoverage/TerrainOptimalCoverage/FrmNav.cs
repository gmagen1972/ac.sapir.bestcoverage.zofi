﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class FrmNav : Form
    {
        public string AscFileName;
        ClsAscMap CAM = new ClsAscMap();

        public FrmNav()
        {
            InitializeComponent();
        }

        private void FrmNav_Load(object sender, EventArgs e)
        {
            ClsShader Pal;
            Pal = new ClsShader("Pallete01.clr");

            ClsAscMap.AscFileStruct AFS;
            ClsGrafix GR = new ClsGrafix(pictureBox1);

            AFS = CAM.GetAscFileInfo(AscFileName);
            LblInfo.Text = "File Size: " + AFS.AscFileSize + " Bytes  File name: " + AFS.AscFileName + "\r\nNCols: " + AFS.NCols + "  NRows: " + AFS.NRows + "  Cell Size: " + AFS.CellSize + "m";

            GR.FillRectangle(10, 10, 10, 10, 255, 255, 0);

            ClsAscMap.AscFileStruct _AFS;
            System.IO.StreamReader File = new System.IO.StreamReader(AscFileName);
            string _T = File.ReadLine();
            _T = _T.Replace("ncols", "");
            _AFS.NCols = Convert.ToInt32(_T);
            _T = File.ReadLine();
            _T = _T.Replace("nrows", "");
            _T = _T.Replace(" ", "");
            _AFS.NRows = Convert.ToUInt32(_T);
            _T = File.ReadLine();
            _T = File.ReadLine();
            _T = File.ReadLine();
            _T = _T.Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            _AFS.CellSize = Convert.ToSingle(_T);
            _T = File.ReadLine();

            ////////////////////////////////////////
            int ResX = (int)(_AFS.NCols / 500) + 1;
            int ResY = (int)(_AFS.NRows / 500);

            ////////////////////////////////////////
            //int ResX = 3;
            //int ResY = 3;

            ////////////////////////////////////////
            int _y = 1;

            while (!File.EndOfStream)
            {
                string _row = File.ReadLine();
                string[] __row = _row.Split();
                int Height, r, g, b;
                for (int x = 0; x < __row.Length - 1; x += ResX)
                {
                    Height = (int)(Convert.ToSingle(__row[x]));
                    if (Height != (-9999))
                    {
                        Height -= Pal.GetMinHeight();
                        r = Pal.ColorArr[Height].r;
                        g = Pal.ColorArr[Height].g;
                        b = Pal.ColorArr[Height].b;

                        GR.FillRectangle(x / ResX + 1, _y, 1, 1, r, g, b);
                    }
                }
                for (int y = 0; y < ResY; y++)
                {
                    _T = File.ReadLine();
                }
                _y++;
            }
            File.Close();

        }
    }
}
