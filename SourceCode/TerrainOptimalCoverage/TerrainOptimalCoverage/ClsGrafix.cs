﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    class ClsGrafix
    {
        Bitmap _bmp;
        Graphics _GR;
        Pen _PT;
        PictureBox _pic;
        float _PenWidth = 2;

        public ClsGrafix(PictureBox Pic)
        {
            _pic = Pic;
            if (_pic.Image != null) _bmp = new Bitmap(_pic.Image);
            else _bmp = new Bitmap(100, 100);
            _GR = _pic.CreateGraphics();
            _GR = Graphics.FromImage(_bmp);
            _PT = new Pen(Color.Black, _PenWidth);
        }

        public ClsGrafix(PictureBox Pic, float PenWidth)
        {
            _pic = Pic;
            if (_pic.Image != null) _bmp = new Bitmap(_pic.Image);
            else _bmp = new Bitmap(100, 100);
            _GR = _pic.CreateGraphics();
            _GR = Graphics.FromImage(_bmp);
            _PenWidth = PenWidth;
            _PT = new Pen(Color.Black, _PenWidth);
        }

        public ClsGrafix(PictureBox Pic, float PenWidth, int Width, int Height)
        {
            _pic = Pic;
            if (_pic.Image != null) _bmp = new Bitmap(_pic.Image);
            else _bmp = new Bitmap(Width, Height);
            _GR = _pic.CreateGraphics();
            _GR = Graphics.FromImage(_bmp);
            _PenWidth = PenWidth;
            _PT = new Pen(Color.Black, _PenWidth);
        }

        public void Rectangle(int X, int Y, int Width, int Height, Color color)
        {
            _PT = new Pen(color, _PenWidth);
            _GR.DrawRectangle(_PT, X, Y, Width, Height);
            _pic.Image = _bmp;
        }


        public void _DrawImage(PictureBox pic)
        {
            _GR.DrawImage(pic.Image, 0, 0);
        }


        public void FillRectangle(SolidBrush b, int X, int Y, int Width, int Height, Color color)
        {
            _PT = new Pen(Color.FromArgb(100, Color.Red), 2);//show tranparent
            //_PT = new Pen(color);
            _GR.FillRectangle(b, X, Y, Width, Height);
            _pic.Image = _bmp;
        }
        //public void FillRectangleForRoutTraffic(SolidBrush b, int X, int Y, int Width, int Height, Color color)
        //{
        //    _PT = new Pen(color);
        //    _GR.FillRectangle(b, X, Y, Width, Height);
        //    _pic.Image = _bmp;
        //}

        public void FillRectangle(SolidBrush b, int X, int Y, int Width, int Height)
        {
            _GR.FillRectangle(b, X, Y, Width, Height);
            _pic.Image = _bmp;
        }
        public void FillRectangle(int X, int Y, int Width, int Height, int RColor, int GColor, int BColor)
        {
            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(Color.FromArgb(RColor, GColor, BColor));

            _GR.FillRectangle(myBrush, X, Y, Width, Height);
            _pic.Image = _bmp;
        }

        public void Rectangle(int X, int Y, int Width, int Height, Color color, float PenWidth)
        {
            _PT = new Pen(color, PenWidth);
            _GR.DrawRectangle(_PT, X, Y, Width, Height);
            _pic.Image = _bmp;
        }

        public void Rectangle(int X, int Y, int Width, int Height)
        {
            _GR.DrawRectangle(_PT, X, Y, Width, Height);
            _pic.Image = _bmp;
        }


        public void Line(int X1, int Y1, int X2, int Y2, Color color, float PenWidth)
        {
            _PT = new Pen(color, PenWidth);
            _GR.DrawLine(_PT, X1, Y1, X2, Y2);
            _pic.Image = _bmp;
        }


        public void Line(int X1, int Y1, int X2, int Y2, Color color)
        {
            _PT = new Pen(color, _PenWidth);
            _GR.DrawLine(_PT, X1, Y1, X2, Y2);
            _pic.Image = _bmp;
        }

        public void Line(int X1, int Y1, int X2, int Y2)
        {
            _GR.DrawLine(_PT, X1, Y1, X2, Y2);
            _pic.Image = _bmp;
        }



        public void SavePic(string FileName)
        {
            _pic.Image.Save(FileName, ImageFormat.Jpeg);
        }

        public void SavePic(string FileName, ImageFormat imageFormat)
        {
            _pic.Image.Save(FileName, imageFormat);
        }
    }
}
