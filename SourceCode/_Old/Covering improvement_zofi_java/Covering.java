import java.io.*;
import java.util.*;
import java.math.*;

public class Covering
{
	private int Ymax, Xmax;
	private AreaMap theMap;
	private Visibility visibility;
	int orderedList[][];
	public Covering(AreaMap map, int Y, int X,Visibility theVisibility  )
	{
		theMap = map;
		visibility = theVisibility;
		Ymax=Y;
		Xmax=X;
		orderedList=new int [(Ymax+1)*(Xmax+1)][3];
	}
	
	public void createInitialList()
	{
		//first stage: summing the total number of visible points for each point
		//copying the total values and initialyzing the array
		int sum;
		
		for(int i=0; i<(Ymax+1)*(Xmax+1);i++)
		{
			sum=0;
			
			for(int j=0; j<(Ymax+1)*(Xmax+1);j++)
			{
				sum=sum+visibility.visibilityMatrix[i][j];
			}
			orderedList[i][0]=i;
			orderedList[i][1]= sum; // the last cell contains the total of visible points
			orderedList[i][2]=0;
		}

	}
		// sorting the values using bubble sort
	public void sortTheList()
	{
		int tempName;
		int tempValue;
		int tempStatus;
		
		for (int k=(Ymax+1)*(Xmax+1)-1;k>=0;k--)
		{
			for(int j=0; j<k;j++)
			{
				if (orderedList[j][1]<orderedList[j+1][1])
				{
					tempName=orderedList[j][0];
					tempValue=orderedList[j][1];
					tempStatus=orderedList[j][2];
					orderedList[j][0]=orderedList[j+1][0];
					orderedList[j][1]=orderedList[j+1][1];
					orderedList[j][2]=orderedList[j+1][2];
					orderedList[j+1][0]=tempName;
					orderedList[j+1][1]=tempValue;
					orderedList[j+1][2]=tempStatus;
				}
			}
		}
		//printing for validation:
		
		/*
		System.out.print("\n"+"Y  X  total");
		for (int i=0;i<(Ymax+1)*(Xmax+1); i++)
		{
			
			int y=orderedList[i][0]/(Xmax+1);
			int x=orderedList[i][0]-((Xmax+1)*y);
			System.out.print("\n"+y+"  "+x+"  "+orderedList[i][1]);
		}
		
		*/
	
	}//end of function
	
	void clearTheList()
	{
		for(int i=0; i<(Ymax+1)*(Xmax+1);i++)
		{
			orderedList[i][0]=0;
			orderedList[i][1]=0;
			orderedList[i][2]=0;
		}
	}
	
	
	
	void findTheBestQpoints(int q)
	{
		createInitialList();
		sortTheList();
		
		int counter=1;

			
		for (int i=0;i<(Ymax+1)*(Xmax+1); i++)// run through all the points on the sorted array
		{
			if(orderedList[i][2]==0)
			{
				//find the current point
				for (int j=0;j<(Ymax+1)*(Xmax+1); j++) // for every point
				{
					//print?
				
					if(visibility.visibilityMatrix[orderedList[i][0]][orderedList[j][0]]==1) // if it is visible to i
					{
						orderedList[j][2]=1; // update the array: this point was already seen
						for (int k=0;k<(Ymax+1)*(Xmax+1); k++) // decrease 1from every point which sees it
						{
							if(visibility.visibilityMatrix[orderedList[k][0]][orderedList[j][0]]==1)
								orderedList[k][1]=orderedList[k][1]-1;
						}
					
					}
				   
				}
				int y=orderedList[i][0]/(Xmax+1);
				int x=orderedList[i][0]-((Xmax+1)*y);
				System.out.print("\n"+"point number"+counter+" is: "+y+" "+x);
				// remember to remove from here
				//visibility.DisplayVisibilityOfPoint(y,x);
				// till here
				counter=counter+1;
				sortTheList();
				i=0;
			}
			
		}
		
			
			
			
	}//end of function
		
		
	
	
	
	
	
}// end of class
