using System;
using System.Text;
using Dhpoware;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XNATerrainTexturing
{

    public class MainProg : Microsoft.Xna.Framework.Game
    {
        private static void Main()
        {
            using (MainProg demo = new MainProg())
            {
                demo.Run();
            }
        }

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private SpriteFont spriteFont;
        private Vector2 fontPos;
        private KeyboardState currentKeyboardState;
        private KeyboardState prevKeyboardState;
        private int windowWidth;
        private int windowHeight;
        private int TerrainWidth;
        private int TerrainHeight;
        private int frames;
        private int framesPerSecond;
        private TimeSpan elapsedTime = TimeSpan.Zero;
        private bool displayHelp;
        private CameraComponent camera;
        private Terrain terrain;
        float[,] heightData;
        float HeightMin, HeightMax;
        float ScaleHeightTo = 500;
        private float  CellSize;
        string HeightMapFile=".\\maps\\HeightMap.asc";
        string TextureFile = ".\\maps\\HeightMap.jpg";
        Texture2D TextureFile2D;
        
        public static int DX=0, DY=0,MDX=0,MDY=0,MDZ=0,SmoothV=3,SmoothM=10;

        Texture2D GNavBarMove;
        Vector2 GNavBarMovePos;
        Texture2D GNavBarView;
        Vector2 GNavBarViewPos;
        Texture2D GNavUD;
        Vector2 GNavUDPos;
        
        Texture2D IconFullScr;
        Vector2 IconFullScrPos;
        Texture2D IconTerrain;
        Vector2 IconTerrainPos;

        Texture2D TScaleH;
        Vector2 TScaleHPos;

        Texture2D SkyBackground;
        Vector2 SkyBackgroundPos;

        MouseState MS = new MouseState();
        string MouseKey="None";
        string GNavBarArea = "None";

        int GNavViewOffsetX = 10;
        int GNavViewOffsetY = 10;
        int GNavMoveOffsetX = 10;
        int GNavMoveOffsetY = 80;

        Rectangle GNavBarViewUp;
        Rectangle GNavBarViewLeft;
        Rectangle GNavBarViewRight;
        Rectangle GNavBarViewDown;

        Rectangle GNavBarMoveUp ;
        Rectangle GNavBarMoveLeft;
        Rectangle GNavBarMoveRight;
        Rectangle GNavBarMoveDown;

        Rectangle GNavUDMoveUp;
        Rectangle GNavUDMoveDown;
        Rectangle RFullScr;

        Rectangle TerrainTicknessMore;
        Rectangle TerrainTicknessLess;

        public MainProg()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Window.Title = "XNA 4.0 Terrain Texturing";
            IsMouseVisible = true;
            IsFixedTimeStep = false;

            camera = new CameraComponent(this);
            Components.Add(camera);
        }     

        protected override void Initialize()
        {
            // Setup the window to be a quarter the size of the desktop.
            windowWidth = GraphicsDevice.DisplayMode.Width / 2;
            windowHeight = GraphicsDevice.DisplayMode.Height / 2;

            // Setup frame buffer.
            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.PreferredBackBufferWidth = windowWidth;
            graphics.PreferredBackBufferHeight = windowHeight;
            graphics.PreferMultiSampling = true;
            graphics.ApplyChanges();

            // Position the text.
            //fontPos = new Vector2(5.0f, 1.0f);
            fontPos = new Vector2(90,30);

            // Setup the initial input states.
            currentKeyboardState = Keyboard.GetState();

            //this.IsMouseVisible = true;

            


            base.Initialize();
        }

        private bool KeyJustPressed(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key) && prevKeyboardState.IsKeyUp(key);
        }


        private void LoadHeightDataAscFile(string file,float ScaleTo )
        {
            //terrainWidth =  heightMap.Width;
            //terrainHeight =  heightMap.Height;
            int x=0,y=0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight = Convert.ToInt16(_T);
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle (_T);

            heightData = new float[TerrainWidth, TerrainHeight];
            
            //LoadAddData

            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    __T=Convert.ToSingle(_x[x]) ;
                    if (__T != -9999F) heightData[x, y] = __T;
                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = heightData[x, y];
                        HeightMin = heightData[x, y];    
                    }
                    HeightMin= Math.Min(heightData[x, y], HeightMin);
                    HeightMax = Math.Max(heightData[x, y], HeightMax);
                }
            }

            //reduce minimum and Scale to 1024
            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    heightData[x, y] = Convert.ToSingle(_x[x]) * ScaleHeightTo/(HeightMax-HeightMin) - HeightMin;
                    
                }
            }
            
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>(@"Fonts\DemoFont");
            /////////////////////////////////////////////////////////////////////////////////
            GNavBarViewUp = new Rectangle(GNavViewOffsetX + 20, GNavViewOffsetY, 20, 20);
            GNavBarViewLeft = new Rectangle(GNavViewOffsetX, GNavViewOffsetY + 20, 20, 20);
            GNavBarViewRight = new Rectangle(GNavViewOffsetX + 40, GNavViewOffsetY + 20, 20, 20);
            GNavBarViewDown = new Rectangle(GNavViewOffsetX + 20, GNavViewOffsetY + 40, 20, 20);

            GNavBarMoveUp = new Rectangle(GNavMoveOffsetX + 20, GNavMoveOffsetY, 20, 20);
            GNavBarMoveLeft = new Rectangle(GNavMoveOffsetX, GNavMoveOffsetY + 20, 20, 20);
            GNavBarMoveRight = new Rectangle(GNavMoveOffsetX + 40, GNavMoveOffsetY + 20, 20, 20);
            GNavBarMoveDown = new Rectangle(GNavMoveOffsetX + 20, GNavMoveOffsetY + 40, 20, 20);

            GNavUDMoveUp = new Rectangle(25, 150, 30, 30);
            GNavUDMoveDown = new Rectangle(25, 210, 30, 30);
            RFullScr = new Rectangle(17, 250, 46, 38);

            TerrainTicknessMore = new Rectangle(Window.ClientBounds.Width - 50, 60, 30, 30);
            TerrainTicknessLess = new Rectangle(Window.ClientBounds.Width - 50, 120, 30, 30);
            /////////////////////////////////////////////////////////////////////////////////

            GNavBarView = Content.Load<Texture2D>("GNavBarView");
            GNavBarViewPos.X = 10;
            GNavBarViewPos.Y = 10;
            GNavBarMove = Content.Load<Texture2D>("GNavBarMove");
            GNavBarMovePos.X = 10;
            GNavBarMovePos.Y = 80;
            GNavUD = Content.Load<Texture2D>("GNavUD");
            GNavUDPos.X = 25;
            GNavUDPos.Y = 150;
            IconFullScr = Content.Load<Texture2D>("FullScreenIcon");
            IconFullScrPos = new Vector2(17, 250);
            IconTerrain = Content.Load<Texture2D>("TerrainIcon");
            IconTerrainPos = new Vector2(Window.ClientBounds.Width - 65, 10);
            TScaleH = Content.Load<Texture2D>("GNavUD");
            TScaleHPos = new Vector2(Window.ClientBounds.Width - 50, 60);
            SkyBackground = Content.Load<Texture2D>("SkyBackground1");
            SkyBackgroundPos = new Vector2(0, 0);
            /////////////////////////////////////////////////////////////////////////////////
            // Setup terrain.
            
            LoadHeightDataAscFile(HeightMapFile , ScaleHeightTo);
            
            terrain = new Terrain(GraphicsDevice, Content);
            
            terrain.Create(Math.Max( TerrainWidth,TerrainHeight),(int)CellSize, 0.0f, ScaleHeightTo);

            var s = System.IO.File.OpenRead(TextureFile);
            TextureFile2D = Texture2D.FromStream(GraphicsDevice, s);
            s.Close();

            //terrain.AddRegion(  0.0f,  ScaleHeightTo/2, @"Textures\region1");
            float tMax = ScaleHeightTo/1.5F ;
            terrain.AddRegion(0.0f, tMax, TextureFile2D);
            //terrain.AddRegion(301.0f,  501.0f, @"Textures\region2");
            //terrain.AddRegion(502.0f,  802.0f, @"Textures\region3");
            //terrain.AddRegion(803.0f, 1024.0f, @"Textures\region4");

            terrain.SunlightDirection = Vector3.Down;
            terrain.SunlightColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
            terrain.TerrainAmbient = new Vector4(0.3f, 0.3f, 0.3f, 1.0f);
            terrain.TerrainDiffuse = new Vector4(0.7f, 0.7f, 0.7f, 1.0f);

            //terrain.GenerateUsingDiamondSquareFractal(1.2f);
            //terrain.GenerateFlatSurface();
            //terrain.GenerateRandomSurface();
            terrain.GenerateXYHeightMap(heightData);

            
            // Setup camera.

            camera.CurrentBehavior = Camera.Behavior.Spectator;
            camera.Velocity = new Vector3(300.0f, 300.0f, 300.0f);
            camera.Acceleration = new Vector3(600.0f, 600.0f, 600.0f);
            
            camera.Perspective(85.0f, (float)windowWidth / (float)windowHeight,
                1.0f, terrain.TerrainBoundingSphere.Radius * 4.0f);
            
            Vector3 cameraPos = new Vector3();
            cameraPos.X = terrain.TerrainSize / 2.0f;
            cameraPos.Z = terrain.TerrainSize * 0.75f;
            cameraPos.Y = terrain.TerrainHeightMap.HeightAt(cameraPos.X, cameraPos.Z) * 1.5f;

            camera.LookAt(cameraPos, terrain.TerrainCenter, Vector3.Up);
        }

        private void ProcessKeyboard()
        {
            prevKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            if (KeyJustPressed(Keys.Escape))
                this.Exit();

            if (KeyJustPressed(Keys.H))
                displayHelp = !displayHelp;

            if (currentKeyboardState.IsKeyDown(Keys.LeftAlt) ||
                currentKeyboardState.IsKeyDown(Keys.RightAlt))
            {
                if (KeyJustPressed(Keys.Enter))
                    ToggleFullScreen();
            }

            if (KeyJustPressed(Keys.Space))
                terrain.GenerateUsingDiamondSquareFractal(1.2f);
            

            if (KeyJustPressed(Keys.D1))
            {
                camera.CurrentBehavior = Camera.Behavior.FirstPerson;
                camera.Velocity = new Vector3(200.0f, 200.0f, 200.0f);
                camera.Acceleration = new Vector3(400.0f, 400.0f, 400.0f);
            }

            if (KeyJustPressed(Keys.D2))
            {
                camera.CurrentBehavior = Camera.Behavior.Spectator;
                camera.Velocity = new Vector3(300.0f, 300.0f, 300.0f);
                camera.Acceleration = new Vector3(600.0f, 600.0f, 600.0f);
            }

            if (KeyJustPressed(Keys.D3))
            {
                camera.CurrentBehavior = Camera.Behavior.Flight;
                camera.Velocity = new Vector3(300.0f, 300.0f, 300.0f);
                camera.Acceleration = new Vector3(600.0f, 600.0f, 600.0f);
            }
        }

        private void ToggleFullScreen()
        {
            int newWidth = 0;
            int newHeight = 0;

            graphics.IsFullScreen = !graphics.IsFullScreen;

            if (graphics.IsFullScreen)
            {
                newWidth = GraphicsDevice.DisplayMode.Width;
                newHeight = GraphicsDevice.DisplayMode.Height;
            }
            else
            {
                newWidth = windowWidth;
                newHeight = windowHeight;
            }

            graphics.PreferredBackBufferWidth = newWidth;
            graphics.PreferredBackBufferHeight = newHeight;
            graphics.ApplyChanges();
            
            LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (!IsActive)
            {
                
                return;
            }

            
            base.Update(gameTime);

            ProcessKeyboard();
            UpdateCamera(gameTime);

            UpdateCamera(gameTime);
            
            UpdateFrameRate(gameTime);
            MS = Mouse.GetState();

            if (MS.LeftButton == ButtonState.Pressed) MouseKey = "Left";
            else if (MS.MiddleButton  == ButtonState.Pressed) MouseKey = "Mid";
            else if (MS.RightButton  == ButtonState.Pressed) MouseKey = "Right";
            else MouseKey = "None";

            Point mousePos = new Point(MS.X, MS.Y);

            if (MouseKey == "Left")
            {

                if (GNavBarViewUp.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(camera.Position.X , camera.Position.Y+10,camera.Position.Z);
                    //Vector3 Eye=new Vector3(0,0,0);
                    ////camera.LookAt(camera.Position, TPos, TPos);
                    //camera.LookAt(camera.Position);
                    GNavBarArea = "View Up";
                    DY = SmoothV;
                }
                else if (GNavBarViewDown.Contains(mousePos))
                {
                    GNavBarArea = "View Down";
                    DY = -SmoothV;
                }
                else if (GNavBarViewLeft.Contains(mousePos))
                {
                    GNavBarArea = "View Left";
                    DX = SmoothV;
                }
                else if (GNavBarViewRight.Contains(mousePos))
                {
                    GNavBarArea = "View Right";
                    DX = -SmoothV;
                }

                else if (GNavUDMoveUp.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(0, 1F, 0);
                    //Vector3 TDist = new Vector3(5, 5, 5);
                    //camera.Move(TPos, TDist);
                    MDY = SmoothM;
                    GNavBarArea = "Move UP";
                }
                else if (GNavUDMoveDown.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(0, -1F, 0);
                    //Vector3 TDist = new Vector3(5, 5, 5);
                    //camera.Move(TPos, TDist);
                    MDY = -SmoothM;
                    GNavBarArea = "Move Down";
                }
                else if (GNavBarMoveDown.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(0, 0, +1F);
                    //Vector3 TDist = new Vector3(10, 10, 10);
                    //camera.Move(TPos, TDist);
                    MDZ = -SmoothM;
                    GNavBarArea = "Move Backward";
                }
                else if (GNavBarMoveUp.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(0, 0, -1F);
                    //Vector3 TDist = new Vector3(10, 10, 10);
                    //camera.Move(TPos, TDist);
                    MDZ = SmoothM;
                    GNavBarArea = "Move Farward";
                }
                else if (GNavBarMoveLeft.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(-1F, 0, 0);
                    //Vector3 TDist = new Vector3(10, 10, 10);
                    //camera.Move(TPos, TDist);
                    MDX = -SmoothM;
                    GNavBarArea = "Move Left";
                }
                else if (GNavBarMoveRight.Contains(mousePos))
                {
                    //Vector3 TPos = new Vector3(1F, 0, 0);
                    //Vector3 TDist = new Vector3(10, 10, 10);
                    //camera.Move(TPos, TDist);
                    MDX = SmoothM;
                    GNavBarArea = "Move Right";
                }
                else if (RFullScr.Contains(mousePos))
                {
                    ToggleFullScreen();
                }
                else if (TerrainTicknessMore.Contains(mousePos))
                {
                    ScaleHeightTo += 10;
                    LoadContent();
                    GNavBarArea = "Terrain Tick More";
                }
                else if (TerrainTicknessLess.Contains(mousePos))
                {
                    ScaleHeightTo -= 10;
                    LoadContent();
                    GNavBarArea = "Terrain Tick Less";
                }
                else
                {
                    GNavBarArea = "None";
                }
            }
            

        }

        private void UpdateCamera(GameTime gameTime)
        {
            Vector3 pos = camera.Position;
            float gridSpacing = terrain.TerrainHeightMap.GridSpacing;
            float size = terrain.TerrainHeightMap.Size * gridSpacing;
            float lowerBounds = 2.0f * gridSpacing;
            float upperBounds = size - (2.0f * gridSpacing);
            float height = terrain.TerrainHeightMap.HeightAt(pos.X, pos.Z) + 25.0f;

            if (pos.X < lowerBounds)
                pos.X = lowerBounds;

            if (pos.X > upperBounds)
                pos.X = upperBounds;
            
            switch (camera.CurrentBehavior)
            {
            case Camera.Behavior.FirstPerson:
                pos.Y = height;
                break;

            case Camera.Behavior.Spectator:

            case Camera.Behavior.Flight:
                if (pos.Y < height)
                    pos.Y = height;

                if (pos.Y > terrain.TerrainSize * 0.5f)
                    pos.Y = terrain.TerrainSize * 0.5f;
                break;

            default:
                break;
            }
            
            if (pos.Z < lowerBounds)
                pos.Z = lowerBounds;

            if (pos.Z > upperBounds)
                pos.Z = upperBounds;
            
            camera.Position = pos;
            
        }

        private void UpdateFrameRate(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                framesPerSecond = frames;
                frames = 0;
            }
        }

        private void IncrementFrameCounter()
        {
            ++frames;
        }
                
        private void Draw2D()
        {
            StringBuilder buffer = new StringBuilder();
            

            if (displayHelp)
            {
                string cameraMode = null;
                buffer.AppendLine("Keys:");
                buffer.AppendLine("=============================");
                buffer.AppendLine("1 - camera mode: first person");
                buffer.AppendLine("2 - camera mode: spectator");
                buffer.AppendLine("3 - camera mode: flight");
                buffer.AppendLine("SPACE - generate a random terrain");
                buffer.AppendLine("ALT & ENTER - toggle full screen");
                buffer.AppendLine("H - Help");
                buffer.AppendLine("Esc - Exit");
                buffer.AppendLine("=============================");
                buffer.AppendLine("Info:");
                buffer.AppendLine("=============================");
                buffer.AppendLine("FPS: "+ framesPerSecond);
                buffer.AppendLine("Camera mode: " + camera.CurrentBehavior);
                buffer.AppendLine("Mouse X: " + MS.X.ToString());
                buffer.AppendLine("Mouse Y: " + MS.Y.ToString());
                buffer.AppendLine("Mouse Key: " + MouseKey);
                buffer.AppendLine("NavBar: " + GNavBarArea);
                buffer.AppendLine("CamPosX: " + camera.Position.X);
                buffer.AppendLine("CamPosY: " + camera.Position.Y);
                buffer.AppendLine("CamPosZ: " + camera.Position.Z);
                buffer.AppendLine("ScaleHeightTo: " + ScaleHeightTo);
            }
            else
            {
                //buffer.AppendLine("H - help");
            }

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            spriteBatch.DrawString(spriteFont, buffer.ToString(), fontPos, Color.Yellow);
            spriteBatch.Draw(GNavBarView, GNavBarViewPos, Color.BlanchedAlmond);
            spriteBatch.Draw(GNavBarMove, GNavBarMovePos, Color.BlanchedAlmond);
            spriteBatch.Draw(GNavUD, GNavUDPos, Color.BlanchedAlmond);
            spriteBatch.Draw(IconFullScr,IconFullScrPos, Color.BlanchedAlmond);
            spriteBatch.Draw(IconTerrain , IconTerrainPos, Color.BlanchedAlmond);
            spriteBatch.Draw(TScaleH, TScaleHPos, Color.BlanchedAlmond);
            spriteBatch.End();
        }

        private void Draw2DBackground()
        {
        
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            spriteBatch.Draw(SkyBackground , SkyBackgroundPos, Color.BlanchedAlmond);
            spriteBatch.End();
        }

        protected override void Draw(GameTime gameTime)
        {
            if (!IsActive)
                return;

            GraphicsDevice.Clear(Color.CornflowerBlue);

            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            GraphicsDevice.SamplerStates[1] = SamplerState.LinearWrap;
            GraphicsDevice.SamplerStates[2] = SamplerState.LinearWrap;
            GraphicsDevice.SamplerStates[3] = SamplerState.LinearWrap;
            Draw2DBackground();
            terrain.Draw(Matrix.Identity, camera.ViewMatrix, camera.ProjectionMatrix);
            Draw2D();

            base.Draw(gameTime);
            IncrementFrameCounter();
        }
    }
}