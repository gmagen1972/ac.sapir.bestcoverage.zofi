﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class MdiMain : Form
    {
        //public float[,] heightData;
        private int childFormNumber = 0;
        //float HeightMin, HeightMax;
        //float ScaleHeightTo = 500;
        //private float CellSize;
       // private int TerrainWidth;
        //private int TerrainHeight;
       // float[,] heightDataActiveForm;
        public MdiMain()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
           
            Load_Photo();
            FrmMap  FMap = new FrmMap();
            FMap.MdiParent = this;
            FMap.Show();
           // heightDataActiveForm = FMap.heightData;
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAbout FAbout = new FrmAbout();
            FAbout.MdiParent = this;
            FAbout.Show();

        }

        private void MdiMain_Load(object sender, EventArgs e)
        {

        }

        private void Load_Photo()
        {
            
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string[] StrFiles = new string[2];
            string[] FilesNameAray = new string[] { "", "" };
            int i,len;
            string destFile;
            string strFileName;
          
            destFile = Application.StartupPath + "\\Maps";
            // Set filter options and filter index.
            openFileDialog1.Filter = "Photo (*.asc;*.jpg)|*.asc;*.jpg";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;
            openFileDialog1.ShowDialog();
          

            StrFiles[0] = openFileDialog1.FileName;

            if (StrFiles[0].EndsWith(".asc"))
            {
                len = StrFiles[0].Length;
                strFileName = StrFiles[0].Remove(len - 3);
                StrFiles[1] = strFileName + "jpg";
                 FilesNameAray[0]="HeightMap.asc";
                 FilesNameAray[1]="HeightMap.jpg";
            }
            else
            {
              len = StrFiles[0].Length;
              strFileName = StrFiles[0].Remove(len - 3);
              StrFiles[1] = strFileName + "asc";
              FilesNameAray[0] = "HeightMap.jpg";
              FilesNameAray[1] = "HeightMap.asc";
            }   

            i = 0;
            foreach (string s in StrFiles)
            {
                // Use static Path methods to extract only the file name from the path.
                //fileName = System.IO.Path.GetFileName(s);
                //destFile = System.IO.Path.Combine(targetPath, fileName);
                try
                {
                    System.IO.File.Copy(s, destFile + "\\" + FilesNameAray[i], true);
                    i++;
                }
                catch (System.IO.IOException err)
                {
                    System.Windows.Forms.MessageBox.Show(err.Message);
                    return;
                }
                if (i == 2)
                {
                   // System.Windows.Forms.MessageBox.Show("All Files have been uploaded successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // process1.StartInfo.FileName = str;
                   //process1.Start();
                     
                }
            }

        
        }

        //private void toolStripButton1_Click(object sender, EventArgs e)
        //{
        // string strActiveFormName;
        // strActiveFormName = ActiveMdiChild.Text;
        // //System.Windows.Forms.MessageBox.Show(strActiveFormName); 
        // LoadHeightDataAscFile(Application.StartupPath + "\\Maps\\" + strActiveFormName, ScaleHeightTo);
          
        //}

        //private void LoadHeightDataAscFile(string file, float ScaleTo)
        //{
        //    //terrainWidth =  heightMap.Width;
        //    //terrainHeight =  heightMap.Height;
        //    int x = 0, y = 0;
        //    float __T;
        //    string[] T = System.IO.File.ReadAllLines(file);//);
        //    string _T = T[0].Replace("ncols", "");
        //    _T = _T.Replace(" ", "");
        //    TerrainWidth = Convert.ToInt16(_T);
        //    _T = T[1].Replace("nrows", "");
        //    _T = _T.Replace(" ", "");
        //    TerrainHeight = Convert.ToInt16(_T);
        //    _T = T[4].Replace("cellsize", "");
        //    _T = _T.Replace(" ", "");
        //    CellSize = Convert.ToSingle(_T);

        //    heightData = new float[TerrainWidth, TerrainHeight];

        //    //LoadAddData

        //    for (y = 0; y < TerrainHeight; y++)
        //    {
        //        string[] _x = T[6 + y].Split();
        //        for (x = 0; x < TerrainWidth; x++)
        //        {
        //            __T = Convert.ToSingle(_x[x]);
        //            if (__T != -9999F) heightData[x, y] = __T;
        //            if ((x == 0) && (y == 0))
        //            {
        //                HeightMax = heightData[x, y];
        //                HeightMin = heightData[x, y];
        //            }
        //            HeightMin = Math.Min(heightData[x, y], HeightMin);
        //            HeightMax = Math.Max(heightData[x, y], HeightMax);
        //        }
        //    }

        //    //reduce minimum and Scale to 1024
        //    for (y = 0; y < TerrainHeight; y++)
        //    {
        //        string[] _x = T[6 + y].Split();
        //        for (x = 0; x < TerrainWidth; x++)
        //        {
        //            heightData[x, y] = Convert.ToSingle(_x[x]) * ScaleHeightTo / (HeightMax - HeightMin) - HeightMin;

        //        }
        //    }

        //}



    }
}
