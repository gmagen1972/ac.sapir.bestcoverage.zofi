﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class FrmSelFirearm : Form
    {
        DataSet2TableAdapters.FirearmsTableAdapter Ds = new DataSet2TableAdapters.FirearmsTableAdapter();
        
        public FrmSelFirearm()
        {
            InitializeComponent();
            
        }

        private void FrmSelFirearm_Load(object sender, EventArgs e)
        {
            this.CmbNames.Items.Clear();
            
            for (int x = 0; x < Ds.GetCount() ; x++)
            {
                this.CmbNames.Items.Add(Ds.GetData()[x].FName);
            }
        }

        private void CmbNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClsGlobal.SelFireArm = CmbNames.Text;
            this.Hide();
           
        }
    }
}
