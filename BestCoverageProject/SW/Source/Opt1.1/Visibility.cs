using System;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

namespace Opt1._1
{
	/// <summary>
	/// Summary description for Visibility.
	/// </summary>
	public class Visibility
	{
		public int [,]visibilityMatrix; 
		public AreaMap theMap;
		ContainerControl m_sender = null;
		int prog = 0;
		Delegate m_senderDelegate = null;
       		
		public Visibility(ContainerControl sender, Delegate senderDelegate, int _prog, AreaMap map)
		{
			m_sender = sender;
			m_senderDelegate = senderDelegate;
			prog = _prog;
			theMap = map;
			visibilityMatrix = new int[(theMap.Max_Y+1)*(theMap.Max_X+1),(theMap.Max_Y+1)*(theMap.Max_X+1)];
			//generateMatrix();
		}

		public void generateMatrix()
		{/*
			m_sender = sender;
			m_senderDelegate = senderDelegate;
			for(int i = 0; i <= theMap.Max_Y; i++)
			{
				Thread.Sleep(50);
				prog = (100*i)/theMap.Max_Y;
				m_sender.BeginInvoke( m_senderDelegate, new object[] { prog, false } );
				for(int j = 0; j <= theMap.Max_X; j++)
				{					
					calculatePositionVisibility(i , j);
				}
				m_sender.BeginInvoke( m_senderDelegate, new object[] { prog, true } );
			}*/
		}
	
	
		public void calculatePositionVisibility(int y, int x)
		{
			for(int i = 0; i <= theMap.Max_Y;i++)
			{
				generateSideCut(y,x,i,0);
				generateSideCut(y,x,i,theMap.Max_X);
			}

			for(int i = 0; i <= theMap.Max_X;i++)
			{
				generateSideCut(y,x,0,i);
				generateSideCut(y,x,theMap.Max_Y,i);
			}
		
		
		}		
			
		public void generateSideCut(int Y_origin , int X_origin, int Y_target,int X_target)
		{
			int [,]sideCut;
			if (X_origin == X_target && Y_origin == Y_target)
			{
				visibilityMatrix[(theMap.Max_X+1)*Y_origin + X_origin,(theMap.Max_X+1)*Y_origin + X_origin] = 1;
				return;
			}
		
			if (X_origin == X_target && Y_origin > Y_target)//upper line
			{
				sideCut=new int[Y_origin - Y_target +1,2];
				for (int i = 0; i <= Y_origin - Y_target; i++)
				{
					sideCut[i,0] = X_origin;
					sideCut[i,1] = Y_origin - i;
				}
				calculateVisibility (sideCut , Y_origin - Y_target);
				return;
			}
		
			if (X_origin == X_target && Y_origin < Y_target)//lower line
			{
				sideCut=new int[Y_target - Y_origin +1,2];
				for (int i = 0; i <= Y_target - Y_origin; i++)
				{
					sideCut[i,0] = X_origin;
					sideCut[i,1] = Y_origin + i;
				}
				calculateVisibility (sideCut , Y_target - Y_origin);
				return;
			}
		
			//here no division by Zero accures
			float a = (float)(Y_target-Y_origin)/(float)(X_target - X_origin); //calculate a
			float b = (float)Y_origin - a * (float)X_origin;
		
			if (X_origin < X_target && Math.Abs(a) < 1)//Right Triangle
			{
				sideCut=new int[X_target - X_origin + 1,2];
				for (int i = 0; i <= X_target - X_origin; i++)
				{
					sideCut[i,0] = X_origin + i;
					sideCut[i,1] = (int)Math.Round(a*(float)(X_origin+i)+b);
				}
				calculateVisibility (sideCut , X_target - X_origin);
				return;
			}
		
			if (X_origin > X_target && Math.Abs(a) < 1)//Left Triangle
			{
				sideCut=new int[X_origin - X_target + 1,2];
				for (int i = 0; i <= X_origin - X_target; i++)
				{
					sideCut[i,0] = X_origin - i;
					sideCut[i,1] = (int)Math.Round(a*(float)(X_origin-i)+b);
				}
				calculateVisibility (sideCut , X_origin - X_target);
				return;
			}
		
			if (Y_origin < Y_target && Math.Abs(a) >= 1)//lower Triangle
			{
				sideCut=new int[Y_target - Y_origin + 1,2];
				for (int i = 0; i <= Y_target - Y_origin; i++)
				{
					sideCut[i,0] = (int)Math.Round(( (float)(Y_origin+i) - b) / a) ;
					sideCut[i,1] = Y_origin + i;
				}
				calculateVisibility (sideCut , Y_target - Y_origin);
				return;
			}
		
			if (Y_origin > Y_target && Math.Abs(a) >= 1)//upper Triangle
			{
				sideCut=new int[Y_origin - Y_target + 1,2];
				for (int i = 0; i <= Y_origin - Y_target; i++)
				{
					sideCut[i,0] = (int)Math.Round(((float)(Y_origin-i) - b) / a) ;
					sideCut[i,1] = Y_origin - i;
				}
				calculateVisibility (sideCut , Y_origin - Y_target);
				return;
			}
		}
	
		public void calculateVisibility(int [,]sCut , int length)
		{//calculates the visible cells along the sidCut
			int lastVisible = 1;
			visibilityMatrix[sCut[0,1]*(theMap.Max_X+1) + sCut[0,0],sCut[0,1]*(theMap.Max_X+1) + sCut[0,0]] = 1;
			visibilityMatrix[sCut[0,1]*(theMap.Max_X+1) + sCut[0,0],sCut[1,1]*(theMap.Max_X+1) + sCut[1,0]] = 1;		
			for(int i = 2; i <= length; i++)
			{
				if( (theMap.cellSize * lastVisible)  * 
					(theMap.theMap[sCut[i,1],sCut[i,0]]-theMap.theMap[sCut[0,1],sCut[0,0]]-10) - 
					(theMap.theMap[sCut[lastVisible,1],sCut[lastVisible,0]]-theMap.theMap[sCut[0,1],sCut[0,0]]-10) * 
					(theMap.cellSize * i) >= 0)
				{
					visibilityMatrix[sCut[0,1]*(theMap.Max_X+1) + sCut[0,0],sCut[i,1]*(theMap.Max_X+1) + sCut[i,0]] = 1;
					lastVisible = i;
				}
			}	
		}
		public bool checkVisibility(int originY,int originX,int sourceY,int sourceX)
		{
			int oIndex = originY*(theMap.Max_X+1) + originX;
			int sIndex = sourceY*(theMap.Max_Y+1) + sourceX;
			if (this.visibilityMatrix[oIndex,sIndex]==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}				   																						   
	}


}
