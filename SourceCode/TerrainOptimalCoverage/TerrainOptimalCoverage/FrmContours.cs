﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class FrmContours : Form
    {
        public FrmContours()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            textBox3.BackColor = colorDialog1.Color;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string PalFile = ClsGlobal.GPal.GetPalFileName();
            if (!ChkCurPal.Checked) ClsGlobal.GPal = new ClsShader(PalFile, ClsShader.ShaderMode.NoSwipe);
            ClsGlobal.GContoursWidth=Convert.ToByte( textBox2.Text);
            ClsGlobal.GContoursDivideEachLayerEvery=Convert.ToByte( textBox1.Text);
            ClsGlobal.GContoursR = colorDialog1.Color.R;
            ClsGlobal.GContoursG = colorDialog1.Color.G;
            ClsGlobal.GContoursB = colorDialog1.Color.B;
            ClsGlobal.GPal.ThinLineEachLayersEvery(ClsGlobal.GContoursDivideEachLayerEvery, ClsGlobal.GContoursWidth, ClsGlobal.GContoursR, ClsGlobal.GContoursG, ClsGlobal.GContoursB);
            this.Hide();
        }
    }
}
