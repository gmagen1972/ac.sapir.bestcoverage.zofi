using System;

namespace Opt1._1
{
	/// <summary>
	/// Summary description for AreaMap.
	/// </summary>

	public class AreaMap
	{
		public int Max_Y,Max_X;
		public int cellSize;
		public float [,]theMap;
	
		public AreaMap(float[,]FloatArray, int Max_Y_Coordinate, int Max_X_Coordinate, int cell_size)
		{
			Max_Y = Max_Y_Coordinate;
			Max_X = Max_X_Coordinate;
			cellSize = cell_size;
			theMap = FloatArray;
		}	
	
	
		public void displayMap()
		{
		}	
	}
		

		
}
