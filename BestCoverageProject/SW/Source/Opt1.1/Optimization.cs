using System;

namespace Opt1._1
{
	/// <summary>
	/// Summary description for Optimization.
	/// </summary>
	public class Optimization
	{
		public int alg;
		public int[,] pList;
		public int[,]top5;
		public int[,]EnemyArray;
		public int[,]aps;
		public int pointNum;
		public Optimization(int algorithm,Visibility v/*,int[,]enemy,int[,]astPoints*/)
		{
			alg=algorithm;
			//EnemyArray=enemy;
			//aps=astPoints;
			top5=new int[5,2];
			pointNum=(v.theMap.Max_X+1)*(v.theMap.Max_Y+1);
			pList = new int[pointNum,3];
			for (int i=0;i<5;i++)
			{
				v=vSum(v);
				pList=sort(pList);
				int y=pList[0,1];
				int x=pList[0,2];
				v=clearPoint(x,y,v);
				top5[i,0]=x;
				top5[i,1]=y;                
			}
		}
		public Visibility vSum(Visibility v)
		{
			for (int i=0;i<pointNum;i++)
			{
				int sum=0;
				for (int j=0;j<pointNum;j++)
				{
					sum=sum+v.visibilityMatrix[i,j];
				}
				pList[i,0]=sum;
				pList[i,1]=i/(v.theMap.Max_X+1);
				pList[i,2]=i%(v.theMap.Max_X+1);
			}
			return v;
		}

		public int[,] sort(int[,]pList)
		{
			for (int i=pointNum-1;i>=0;i--)
				for (int j=0;j<i;j++)
				{
					if (pList[j,0]<pList[j+1,0])
					{
						int t=pList[j,0];
						pList[j,0]=pList[j+1,0];
						pList[j+1,0]=t;
						t=pList[j,1];
						pList[j,1]=pList[j+1,1];
						pList[j+1,1]=t;
						t=pList[j,2];
						pList[j,2]=pList[j+1,2];
						pList[j+1,2]=t;
					}
				}
			return pList;
		}
		public Visibility clearPoint(int x,int y,Visibility v)
		{
			int index=x*(v.theMap.Max_X+1)+y;
			for (int i=0;i<pointNum;i++)
			{
				if (v.visibilityMatrix[index,i]==1)
				{
					v.visibilityMatrix[index,i]=0;
					for (int j=0;j<pointNum;j++)
					{
						v.visibilityMatrix[j,i]=0;
					}
				}
			}

			return v;
		}



	}
}
