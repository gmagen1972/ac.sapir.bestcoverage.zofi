
public class Visibility
{
	public int visibilityMatrix[][]; 
	private AreaMap theMap;
		
	public Visibility(AreaMap map)
	{
		theMap = map;
		visibilityMatrix = new int[(theMap.Max_Y+1)*(theMap.Max_X+1)][(theMap.Max_Y+1)*(theMap.Max_X+1)];
		generateMatrix();
	}
	
	private void generateMatrix()
	{
		for(int i = 0; i <= theMap.Max_Y; i++)
			for(int j = 0; j <= theMap.Max_X; j++)
				calculatePositionVisibility(i , j);
	}
	
	
	private void calculatePositionVisibility(int y, int x)
	{
		for(int i = 0; i <= theMap.Max_Y;i++)
		{
			generateSideCut(y,x,i,0);
			generateSideCut(y,x,i,theMap.Max_X);
		}

		for(int i = 0; i <= theMap.Max_X;i++)
		{
			generateSideCut(y,x,0,i);
			generateSideCut(y,x,theMap.Max_Y,i);
		}
		
		
	}		
			
	private void generateSideCut(int Y_origin , int X_origin, int Y_target,int X_target)
	{
		int sideCut[][];
		if (X_origin == X_target && Y_origin == Y_target)
		{
			visibilityMatrix[(theMap.Max_X+1)*Y_origin + X_origin][(theMap.Max_X+1)*Y_origin + X_origin] = 1;
			return;
		}
		
		if (X_origin == X_target && Y_origin > Y_target)//upper line
		{
			sideCut = new int[Y_origin - Y_target +1][2];
			for (int i = 0; i <= Y_origin - Y_target; i++)
			{
				sideCut[i][0] = X_origin;
				sideCut[i][1] = Y_origin - i;
			}
			calculateVisibility (sideCut , Y_origin - Y_target);
			return;
		}
		
		if (X_origin == X_target && Y_origin < Y_target)//lower line
		{
			sideCut = new int[Y_target - Y_origin +1][2];
			for (int i = 0; i <= Y_target - Y_origin; i++)
			{
				sideCut[i][0] = X_origin;
				sideCut[i][1] = Y_origin + i;
			}
			calculateVisibility (sideCut , Y_target - Y_origin);
			return;
		}
		
		//here no division by Zero accures
		float a = (float)(Y_target-Y_origin)/(float)(X_target - X_origin); //calculate a
		float b = (float)Y_origin - a * (float)X_origin;
		
		if (X_origin < X_target && Math.abs(a) < 1)//Right Triangle
		{
			sideCut = new int[X_target - X_origin + 1][2];
			for (int i = 0; i <= X_target - X_origin; i++)
			{
				sideCut[i][0] = X_origin + i;
				sideCut[i][1] = Math.round(a*(float)(X_origin+i)+b);
			}
			calculateVisibility (sideCut , X_target - X_origin);
			return;
		}
		
		if (X_origin > X_target && Math.abs(a) < 1)//Left Triangle
		{
			sideCut = new int[X_origin - X_target + 1][2];
			for (int i = 0; i <= X_origin - X_target; i++)
			{
				sideCut[i][0] = X_origin - i;
				sideCut[i][1] = Math.round(a*(float)(X_origin-i)+b);
			}
			calculateVisibility (sideCut , X_origin - X_target);
			return;
		}
		
		if (Y_origin < Y_target && Math.abs(a) >= 1)//lower Triangle
		{
			sideCut = new int[Y_target - Y_origin + 1][2];
			for (int i = 0; i <= Y_target - Y_origin; i++)
			{
				sideCut[i][0] = Math.round(( (float)(Y_origin+i) - b) / a) ;
				sideCut[i][1] = Y_origin + i;
			}
			calculateVisibility (sideCut , Y_target - Y_origin);
			return;
		}
		
		if (Y_origin > Y_target && Math.abs(a) >= 1)//upper Triangle
		{
			sideCut = new int[Y_origin - Y_target + 1][2];
			for (int i = 0; i <= Y_origin - Y_target; i++)
			{
				sideCut[i][0] = Math.round(((float)(Y_origin-i) - b) / a) ;
				sideCut[i][1] = Y_origin - i;
			}
			calculateVisibility (sideCut , Y_origin - Y_target);
			return;
		}
	}
	
	private void calculateVisibility(int sCut[][] , int length)
	{//calculates the visible cells along the sidCut
		int lastVisible = 1;
		visibilityMatrix[sCut[0][1]*(theMap.Max_X+1) + sCut[0][0]][sCut[0][1]*(theMap.Max_X+1) + sCut[0][0]] = 1;
		visibilityMatrix[sCut[0][1]*(theMap.Max_X+1) + sCut[0][0]][sCut[1][1]*(theMap.Max_X+1) + sCut[1][0]] = 1;
		
		for(int i = 2; i <= length; i++)
		{
			if( (theMap.cellSize * lastVisible)  * 
				(theMap.theMap[sCut[i][1]][sCut[i][0]]-theMap.theMap[sCut[0][1]][sCut[0][0]]-1) - 
				(theMap.theMap[sCut[lastVisible][1]][sCut[lastVisible][0]]-theMap.theMap[sCut[0][1]][sCut[0][0]]-1) * 
				(theMap.cellSize * i) >= 0)
			{
				visibilityMatrix[sCut[0][1]*(theMap.Max_X+1) + sCut[0][0]][sCut[i][1]*(theMap.Max_X+1) + sCut[i][0]] = 1;
				lastVisible = i;
			}
		}			   
		
	}

	
	public void DisplayVisibilityOfPoint(int pointY, int pointX)
	{
		int index = pointY*(theMap.Max_X+1) + pointX;
		
		System.out.println();
		
		for(int i=theMap.Max_Y ; i>=0 ; i--)
		{
			
			for(int j=0; j<=theMap.Max_X ;j++)
			{
				System.out.print(visibilityMatrix[index][i*(theMap.Max_X+1) + j]);
			}
			System.out.println("  "+i);
		}
	}
	

																						   																						   
}
