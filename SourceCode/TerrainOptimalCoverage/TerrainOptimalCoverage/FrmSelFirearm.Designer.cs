﻿namespace TerrainOptimalCoverage
{
    partial class FrmSelFirearm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbNames = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // CmbNames
            // 
            this.CmbNames.FormattingEnabled = true;
            this.CmbNames.Location = new System.Drawing.Point(12, 12);
            this.CmbNames.Name = "CmbNames";
            this.CmbNames.Size = new System.Drawing.Size(163, 21);
            this.CmbNames.TabIndex = 0;
            this.CmbNames.SelectedIndexChanged += new System.EventHandler(this.CmbNames_SelectedIndexChanged);
            // 
            // FrmSelFirearm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(185, 48);
            this.Controls.Add(this.CmbNames);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmSelFirearm";
            this.Text = "Select Firearm";
            this.Load += new System.EventHandler(this.FrmSelFirearm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox CmbNames;
    }
}