﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerrainOptimalCoverage
{
    class ClsAscMap
    {
        public float HeightMin, HeightMax;
        public float CellSize;
        public int TerrainWidth;
        public int TerrainHeight;
        public float[,] heightData;

        public struct AscFileStruct
        {
            public long NRows;
            public long NCols;
            public string AscFileName;
            public long AscFileSize;
            public float CellSize;
        }

        public AscFileStruct GetAscFileInfo(string AscFile)
        {
            AscFileStruct _AFS;
            System.IO.StreamReader File=new System.IO.StreamReader(AscFile);
            string _T=File.ReadLine(); 
            _T= _T.Replace("ncols", "");
            _AFS.NCols = Convert.ToInt32(_T);
            _T = File.ReadLine(); 
            _T = _T.Replace("nrows", "");
            _T = _T.Replace(" ", "");
            _AFS.NRows = Convert.ToUInt32 (_T);
            _T = File.ReadLine();
            _T = File.ReadLine();
            _T = File.ReadLine(); 
            _T = _T.Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            _AFS.CellSize = Convert.ToSingle(_T);
            _AFS.AscFileName = AscFile;
            System.IO.FileInfo FI = new System.IO.FileInfo(AscFile);
            _AFS.AscFileSize = FI.Length;
            File.Close();
            return _AFS;
        }

        //public void SampleHeightDataAscFile(string file,int ScaleToX,int ScaleToY)
        //{
        //    AscFileStruct _AFS;
        //    System.IO.StreamReader File = new System.IO.StreamReader(AscFile);
        //    string _T = File.ReadLine();
        //    _T = _T.Replace("ncols", "");
        //    _AFS.NCols = Convert.ToInt32(_T);
        //    _T = File.ReadLine();
        //    _T = _T.Replace("nrows", "");
        //    _T = _T.Replace(" ", "");
        //    _AFS.NRows = Convert.ToUInt32(_T);
        //    _T = File.ReadLine();
        //    _T = File.ReadLine();
        //    _T = File.ReadLine();
        //    _T = _T.Replace("cellsize", "");
        //    _T = _T.Replace(" ", "");
        //    _AFS.CellSize = Convert.ToSingle(_T);
        //    ////////////////////////////////////////
        //    int ResX = (int)_AFS.NCols / ScaleToX;
        //    int ResY = (int)_AFS.NRows / ScaleToY;
        //    ////////////////////////////////////////
        //    while (!File.EndOfStream)
        //    {
        //        string _row = File.ReadLine();
        //    }
        //    File.Close();
        //}
        
        public void LoadHeightDataAscFile(string file)
        {
            int x = 0, y = 0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight = Convert.ToInt16(_T);
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle(_T);

            heightData = new float[TerrainWidth, TerrainHeight];

            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    __T = Convert.ToSingle(_x[x]);
                    if (__T != -9999F) heightData[x, y] = __T;
                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = heightData[x, y];
                        HeightMin = heightData[x, y];
                    }
                    HeightMin = Math.Min(heightData[x, y], HeightMin);
                    HeightMax = Math.Max(heightData[x, y], HeightMax);
                }
            }
        }

        public void SaveRawFile(string file)
        {
            int x = 0, y = 0;
            System.IO.StreamWriter F = new System.IO.StreamWriter(file, false);

            for (y = 0; y < TerrainHeight; y++)
            {
                for (x = 0; x < TerrainWidth; x++)
                {
                    HeightMin = Math.Min(heightData[x, y], HeightMin);
                    HeightMax = Math.Max(heightData[x, y], HeightMax);
                    F.WriteLine(heightData[x, y].ToString());
                }
            }
            F.WriteLine("Min\t" + HeightMin);
            F.WriteLine("Max\t" + HeightMax);

            F.Close();
        }


    }
}
