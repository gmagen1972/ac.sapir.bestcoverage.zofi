﻿namespace TerrainOptimalCoverage
{
    partial class Frm4Map
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.PicDown = new System.Windows.Forms.PictureBox();
            this.PicRight = new System.Windows.Forms.PictureBox();
            this.PicLeft = new System.Windows.Forms.PictureBox();
            this.PicUp = new System.Windows.Forms.PictureBox();
            this.PicTerrain = new System.Windows.Forms.PictureBox();
            this.CmbPallete = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTerrain)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(892, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(150, 16);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(74, 17);
            this.toolStripStatusLabel1.Text = "Please wait...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::TerrainOptimalCoverage.Properties.Resources.TerrainIcon;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(381, 362);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 60);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Click to\r\nanalyze Terrain");
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PicBox1
            // 
            this.PicBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicBox1.Location = new System.Drawing.Point(12, 10);
            this.PicBox1.Name = "PicBox1";
            this.PicBox1.Size = new System.Drawing.Size(462, 402);
            this.PicBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PicBox1.TabIndex = 3;
            this.PicBox1.TabStop = false;
            this.PicBox1.Tag = "";
            this.PicBox1.Click += new System.EventHandler(this.PicBox1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::TerrainOptimalCoverage.Properties.Resources.ColorScale;
            this.pictureBox2.Location = new System.Drawing.Point(277, 222);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 190);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // PicDown
            // 
            this.PicDown.BackgroundImage = global::TerrainOptimalCoverage.Properties.Resources.downkey;
            this.PicDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicDown.Location = new System.Drawing.Point(399, 321);
            this.PicDown.Name = "PicDown";
            this.PicDown.Size = new System.Drawing.Size(35, 35);
            this.PicDown.TabIndex = 8;
            this.PicDown.TabStop = false;
            this.PicDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicDown_MouseDown);
            // 
            // PicRight
            // 
            this.PicRight.BackgroundImage = global::TerrainOptimalCoverage.Properties.Resources.Rightkey;
            this.PicRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicRight.Location = new System.Drawing.Point(438, 321);
            this.PicRight.Name = "PicRight";
            this.PicRight.Size = new System.Drawing.Size(35, 35);
            this.PicRight.TabIndex = 7;
            this.PicRight.TabStop = false;
            this.PicRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicRight_MouseDown);
            // 
            // PicLeft
            // 
            this.PicLeft.BackgroundImage = global::TerrainOptimalCoverage.Properties.Resources.leftkey;
            this.PicLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicLeft.Location = new System.Drawing.Point(359, 321);
            this.PicLeft.Name = "PicLeft";
            this.PicLeft.Size = new System.Drawing.Size(35, 35);
            this.PicLeft.TabIndex = 6;
            this.PicLeft.TabStop = false;
            this.PicLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicLeft_MouseDown);
            // 
            // PicUp
            // 
            this.PicUp.BackgroundImage = global::TerrainOptimalCoverage.Properties.Resources.upkey;
            this.PicUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicUp.Location = new System.Drawing.Point(399, 281);
            this.PicUp.Name = "PicUp";
            this.PicUp.Size = new System.Drawing.Size(35, 35);
            this.PicUp.TabIndex = 5;
            this.PicUp.TabStop = false;
            this.PicUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicUp_MouseDown);
            // 
            // PicTerrain
            // 
            this.PicTerrain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicTerrain.Image = global::TerrainOptimalCoverage.Properties.Resources.MapBackground400x400;
            this.PicTerrain.Location = new System.Drawing.Point(480, 10);
            this.PicTerrain.Name = "PicTerrain";
            this.PicTerrain.Size = new System.Drawing.Size(400, 400);
            this.PicTerrain.TabIndex = 4;
            this.PicTerrain.TabStop = false;
            // 
            // CmbPallete
            // 
            this.CmbPallete.FormattingEnabled = true;
            this.CmbPallete.Items.AddRange(new object[] {
            "Terrain",
            "Greyscale and Red"});
            this.CmbPallete.Location = new System.Drawing.Point(359, 251);
            this.CmbPallete.Name = "CmbPallete";
            this.CmbPallete.Size = new System.Drawing.Size(113, 21);
            this.CmbPallete.TabIndex = 13;
            this.CmbPallete.Text = "Terrain";
            this.CmbPallete.SelectedValueChanged += new System.EventHandler(this.CmbPallete_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(360, 235);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Select Pallete:";
            // 
            // Frm4Map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmbPallete);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PicDown);
            this.Controls.Add(this.PicRight);
            this.Controls.Add(this.PicLeft);
            this.Controls.Add(this.PicUp);
            this.Controls.Add(this.PicTerrain);
            this.Controls.Add(this.PicBox1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Frm4Map";
            this.Text = "Map";
            this.Load += new System.EventHandler(this.FrmAscMap_Load);
            this.Shown += new System.EventHandler(this.FrmAscMap_Shown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTerrain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox PicBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox PicTerrain;
        private System.Windows.Forms.PictureBox PicUp;
        private System.Windows.Forms.PictureBox PicLeft;
        private System.Windows.Forms.PictureBox PicRight;
        private System.Windows.Forms.PictureBox PicDown;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox CmbPallete;
        private System.Windows.Forms.Label label1;
    }
}