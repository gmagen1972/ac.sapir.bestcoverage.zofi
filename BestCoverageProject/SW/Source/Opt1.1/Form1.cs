using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing.Printing;


namespace Opt1._1
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.NumericUpDown numericUpDown4;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button14;
		public System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button17;
		public System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.GroupBox groupBox10;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.GroupBox groupBox11;
		private System.Windows.Forms.GroupBox groupBox12;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Label label11;
		Bitmap bmpBack = null;
		private MarkControl mark1,mark2,mark3,mark4,mark5;
		private bool isSelected = false;
		private int _X, _Y;
		public layer applicationLayer;
		private int dx;
		private int dy;
		private bool vs=false;
		private System.Windows.Forms.GroupBox groupBox13;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label20;
		private bool ast=false;
		private Point temp;
		private bool eol=false;
		private System.Windows.Forms.Button button1;
		private String FileName;
		private String MapFileName;
		private String MapX,MapY;
		private System.Windows.Forms.GroupBox groupBox14;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.GroupBox groupBox15;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button22;
		private int state = 0;
		private int view = 0;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//bmpBack = new Bitmap(pictureBox1.Width,pictureBox1.Height);
			bmpBack = new Bitmap(pictureBox1.Image);
			//Graphics.FromImage(bmpBack).Clear(Color.Transparent);
			pictureBox1.Image = (Bitmap)bmpBack.Clone();


			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button4 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.button16 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.groupBox11 = new System.Windows.Forms.GroupBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.button17 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.groupBox12 = new System.Windows.Forms.GroupBox();
			this.label20 = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.label11 = new System.Windows.Forms.Label();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.groupBox13 = new System.Windows.Forms.GroupBox();
			this.label19 = new System.Windows.Forms.Label();
			this.button21 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox14 = new System.Windows.Forms.GroupBox();
			this.button22 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.groupBox15 = new System.Windows.Forms.GroupBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.groupBox7.SuspendLayout();
			this.groupBox8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
			this.groupBox6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			this.groupBox9.SuspendLayout();
			this.groupBox11.SuspendLayout();
			this.groupBox12.SuspendLayout();
			this.groupBox10.SuspendLayout();
			this.groupBox13.SuspendLayout();
			this.panel1.SuspendLayout();
			this.groupBox14.SuspendLayout();
			this.groupBox15.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button4);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.button6);
			this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox1.Location = new System.Drawing.Point(8, 32);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(120, 128);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Border";
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button4.Enabled = false;
			this.button4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button4.Location = new System.Drawing.Point(16, 64);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(96, 24);
			this.button4.TabIndex = 2;
			this.button4.Text = "Set";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button2.Enabled = false;
			this.button2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button2.Location = new System.Drawing.Point(16, 32);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(96, 24);
			this.button2.TabIndex = 1;
			this.button2.Text = "Create";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button6.Enabled = false;
			this.button6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button6.Location = new System.Drawing.Point(16, 96);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(96, 24);
			this.button6.TabIndex = 4;
			this.button6.Text = "Clean";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.button3);
			this.groupBox2.Controls.Add(this.button5);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox2.Location = new System.Drawing.Point(136, 32);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(136, 128);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Target Points";
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button3.Enabled = false;
			this.button3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button3.Location = new System.Drawing.Point(8, 32);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(120, 24);
			this.button3.TabIndex = 0;
			this.button3.Text = "Create";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button5.Enabled = false;
			this.button5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button5.Location = new System.Drawing.Point(8, 96);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(120, 24);
			this.button5.TabIndex = 3;
			this.button5.Text = "Clean";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button1.Enabled = false;
			this.button1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button1.Location = new System.Drawing.Point(8, 64);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(120, 24);
			this.button1.TabIndex = 5;
			this.button1.Text = "Set";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.groupBox1);
			this.groupBox3.Controls.Add(this.groupBox2);
			this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox3.Location = new System.Drawing.Point(8, 152);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(280, 168);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Visual Input";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.groupBox5);
			this.groupBox4.Controls.Add(this.groupBox7);
			this.groupBox4.Controls.Add(this.groupBox8);
			this.groupBox4.Controls.Add(this.groupBox6);
			this.groupBox4.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox4.Location = new System.Drawing.Point(8, 328);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(280, 320);
			this.groupBox4.TabIndex = 3;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Numerical Input";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.numericUpDown1);
			this.groupBox5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox5.Location = new System.Drawing.Point(8, 32);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(264, 56);
			this.groupBox5.TabIndex = 2;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Available Observers";
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.numericUpDown1.Location = new System.Drawing.Point(8, 24);
			this.numericUpDown1.Maximum = new System.Decimal(new int[] {
																		   8,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Minimum = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(48, 22);
			this.numericUpDown1.TabIndex = 1;
			this.numericUpDown1.Value = new System.Decimal(new int[] {
																		 5,
																		 0,
																		 0,
																		 0});
			this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.button7);
			this.groupBox7.Controls.Add(this.button8);
			this.groupBox7.Controls.Add(this.textBox6);
			this.groupBox7.Controls.Add(this.textBox7);
			this.groupBox7.Controls.Add(this.textBox8);
			this.groupBox7.Controls.Add(this.label7);
			this.groupBox7.Controls.Add(this.label9);
			this.groupBox7.Controls.Add(this.label10);
			this.groupBox7.Controls.Add(this.textBox5);
			this.groupBox7.Controls.Add(this.label4);
			this.groupBox7.Controls.Add(this.textBox4);
			this.groupBox7.Controls.Add(this.label3);
			this.groupBox7.Controls.Add(this.textBox3);
			this.groupBox7.Controls.Add(this.label2);
			this.groupBox7.Controls.Add(this.textBox2);
			this.groupBox7.Controls.Add(this.label1);
			this.groupBox7.Controls.Add(this.textBox1);
			this.groupBox7.Controls.Add(this.label8);
			this.groupBox7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox7.Location = new System.Drawing.Point(8, 96);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(264, 152);
			this.groupBox7.TabIndex = 3;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Set Observer Radius";
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button7.Enabled = false;
			this.button7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button7.Location = new System.Drawing.Point(136, 120);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(56, 24);
			this.button7.TabIndex = 28;
			this.button7.Text = "Clean";
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button8.Enabled = false;
			this.button8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button8.Location = new System.Drawing.Point(200, 120);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(56, 24);
			this.button8.TabIndex = 27;
			this.button8.Text = "Set";
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// textBox6
			// 
			this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox6.Location = new System.Drawing.Point(160, 24);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(96, 20);
			this.textBox6.TabIndex = 26;
			this.textBox6.Text = "";
			this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBox6.Visible = false;
			// 
			// textBox7
			// 
			this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox7.Location = new System.Drawing.Point(160, 48);
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(96, 20);
			this.textBox7.TabIndex = 24;
			this.textBox7.Text = "";
			this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBox7.Visible = false;
			// 
			// textBox8
			// 
			this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox8.Location = new System.Drawing.Point(160, 72);
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new System.Drawing.Size(96, 20);
			this.textBox8.TabIndex = 22;
			this.textBox8.Text = "";
			this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBox8.Visible = false;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label7.Location = new System.Drawing.Point(136, 72);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(24, 23);
			this.label7.TabIndex = 21;
			this.label7.Text = "#8";
			this.label7.Visible = false;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label9.Location = new System.Drawing.Point(136, 48);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(24, 23);
			this.label9.TabIndex = 19;
			this.label9.Text = "#7";
			this.label9.Visible = false;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label10.Location = new System.Drawing.Point(136, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(24, 23);
			this.label10.TabIndex = 17;
			this.label10.Text = "#6";
			this.label10.Visible = false;
			// 
			// textBox5
			// 
			this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox5.Location = new System.Drawing.Point(32, 120);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(96, 20);
			this.textBox5.TabIndex = 16;
			this.textBox5.Text = "";
			this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label4.Location = new System.Drawing.Point(8, 120);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(24, 23);
			this.label4.TabIndex = 15;
			this.label4.Text = "#5";
			// 
			// textBox4
			// 
			this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox4.Location = new System.Drawing.Point(32, 96);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(96, 20);
			this.textBox4.TabIndex = 14;
			this.textBox4.Text = "";
			this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label3.Location = new System.Drawing.Point(8, 96);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(24, 23);
			this.label3.TabIndex = 13;
			this.label3.Text = "#4";
			// 
			// textBox3
			// 
			this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox3.Location = new System.Drawing.Point(32, 72);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(96, 20);
			this.textBox3.TabIndex = 12;
			this.textBox3.Text = "";
			this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label2.Location = new System.Drawing.Point(8, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 23);
			this.label2.TabIndex = 11;
			this.label2.Text = "#3";
			// 
			// textBox2
			// 
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox2.Location = new System.Drawing.Point(32, 48);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(96, 20);
			this.textBox2.TabIndex = 10;
			this.textBox2.Text = "";
			this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label1.Location = new System.Drawing.Point(8, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(24, 23);
			this.label1.TabIndex = 9;
			this.label1.Text = "#2";
			// 
			// textBox1
			// 
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.textBox1.Location = new System.Drawing.Point(32, 24);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(96, 20);
			this.textBox1.TabIndex = 8;
			this.textBox1.Text = "";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label8.Location = new System.Drawing.Point(8, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(24, 23);
			this.label8.TabIndex = 7;
			this.label8.Text = "#1";
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.numericUpDown4);
			this.groupBox8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox8.Location = new System.Drawing.Point(8, 256);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(128, 56);
			this.groupBox8.TabIndex = 4;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Target Height";
			// 
			// numericUpDown4
			// 
			this.numericUpDown4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.numericUpDown4.Location = new System.Drawing.Point(40, 24);
			this.numericUpDown4.Name = "numericUpDown4";
			this.numericUpDown4.Size = new System.Drawing.Size(32, 22);
			this.numericUpDown4.TabIndex = 1;
			this.numericUpDown4.Value = new System.Decimal(new int[] {
																		 1,
																		 0,
																		 0,
																		 0});
			this.numericUpDown4.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.numericUpDown2);
			this.groupBox6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox6.Location = new System.Drawing.Point(144, 256);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(128, 56);
			this.groupBox6.TabIndex = 3;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Observer Height";
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.numericUpDown2.Location = new System.Drawing.Point(48, 24);
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(32, 22);
			this.numericUpDown2.TabIndex = 1;
			this.numericUpDown2.Value = new System.Decimal(new int[] {
																		 1,
																		 0,
																		 0,
																		 0});
			this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.button16);
			this.groupBox9.Controls.Add(this.button15);
			this.groupBox9.Controls.Add(this.button13);
			this.groupBox9.Controls.Add(this.button11);
			this.groupBox9.Controls.Add(this.button10);
			this.groupBox9.Controls.Add(this.button9);
			this.groupBox9.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox9.Location = new System.Drawing.Point(184, 8);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(240, 136);
			this.groupBox9.TabIndex = 4;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "View";
			// 
			// button16
			// 
			this.button16.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button16.Enabled = false;
			this.button16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button16.Location = new System.Drawing.Point(120, 72);
			this.button16.Name = "button16";
			this.button16.Size = new System.Drawing.Size(104, 24);
			this.button16.TabIndex = 11;
			this.button16.Text = "Viewshed";
			this.button16.Click += new System.EventHandler(this.button16_Click);
			// 
			// button15
			// 
			this.button15.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button15.Enabled = false;
			this.button15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button15.Location = new System.Drawing.Point(8, 40);
			this.button15.Name = "button15";
			this.button15.Size = new System.Drawing.Size(104, 24);
			this.button15.TabIndex = 10;
			this.button15.Text = "Map";
			this.button15.Click += new System.EventHandler(this.button15_Click);
			// 
			// button13
			// 
			this.button13.BackColor = System.Drawing.SystemColors.Desktop;
			this.button13.Enabled = false;
			this.button13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.button13.Location = new System.Drawing.Point(120, 104);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(104, 24);
			this.button13.TabIndex = 8;
			this.button13.Text = "Clean Stats";
			this.button13.Click += new System.EventHandler(this.button13_Click);
			// 
			// button11
			// 
			this.button11.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button11.Enabled = false;
			this.button11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button11.Location = new System.Drawing.Point(8, 72);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(104, 24);
			this.button11.TabIndex = 6;
			this.button11.Text = "Grayscale I";
			this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button11.Click += new System.EventHandler(this.button11_Click);
			// 
			// button10
			// 
			this.button10.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button10.Enabled = false;
			this.button10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button10.Location = new System.Drawing.Point(120, 40);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(104, 24);
			this.button10.TabIndex = 5;
			this.button10.Text = "3D View";
			this.button10.Click += new System.EventHandler(this.button10_Click);
			// 
			// button9
			// 
			this.button9.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button9.Enabled = false;
			this.button9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button9.Location = new System.Drawing.Point(8, 104);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(104, 24);
			this.button9.TabIndex = 4;
			this.button9.Text = "Grayscale II";
			this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// groupBox11
			// 
			this.groupBox11.Controls.Add(this.label12);
			this.groupBox11.Controls.Add(this.label15);
			this.groupBox11.Controls.Add(this.label14);
			this.groupBox11.Controls.Add(this.label16);
			this.groupBox11.Controls.Add(this.label17);
			this.groupBox11.Controls.Add(this.label18);
			this.groupBox11.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox11.Location = new System.Drawing.Point(432, 8);
			this.groupBox11.Name = "groupBox11";
			this.groupBox11.Size = new System.Drawing.Size(160, 136);
			this.groupBox11.TabIndex = 21;
			this.groupBox11.TabStop = false;
			this.groupBox11.Text = "Location";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label12.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.label12.Location = new System.Drawing.Point(8, 40);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(80, 23);
			this.label12.TabIndex = 15;
			this.label12.Text = "Longitude:";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label15.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.label15.Location = new System.Drawing.Point(8, 72);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(80, 23);
			this.label15.TabIndex = 17;
			this.label15.Text = "Altitude:";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label14.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.label14.Location = new System.Drawing.Point(8, 104);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(80, 23);
			this.label14.TabIndex = 16;
			this.label14.Text = "Height:";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label16
			// 
			this.label16.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label16.Location = new System.Drawing.Point(88, 40);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(64, 23);
			this.label16.TabIndex = 18;
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label17
			// 
			this.label17.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label17.Location = new System.Drawing.Point(88, 72);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(64, 23);
			this.label17.TabIndex = 19;
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label18
			// 
			this.label18.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label18.Location = new System.Drawing.Point(88, 104);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(64, 23);
			this.label18.TabIndex = 20;
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// button17
			// 
			this.button17.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.button17.Enabled = false;
			this.button17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button17.ForeColor = System.Drawing.SystemColors.InfoText;
			this.button17.Location = new System.Drawing.Point(8, 40);
			this.button17.Name = "button17";
			this.button17.Size = new System.Drawing.Size(120, 40);
			this.button17.TabIndex = 12;
			this.button17.Text = "Calculate Visibility";
			this.button17.Click += new System.EventHandler(this.button17_Click);
			// 
			// button14
			// 
			this.button14.BackColor = System.Drawing.SystemColors.Info;
			this.button14.Enabled = false;
			this.button14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button14.ForeColor = System.Drawing.SystemColors.InfoText;
			this.button14.Location = new System.Drawing.Point(8, 88);
			this.button14.Name = "button14";
			this.button14.Size = new System.Drawing.Size(120, 40);
			this.button14.TabIndex = 9;
			this.button14.Text = "Solve";
			this.button14.Click += new System.EventHandler(this.button14_Click);
			// 
			// button12
			// 
			this.button12.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.button12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button12.ForeColor = System.Drawing.SystemColors.Desktop;
			this.button12.Location = new System.Drawing.Point(8, 40);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(72, 24);
			this.button12.TabIndex = 7;
			this.button12.Text = "Load";
			this.button12.Click += new System.EventHandler(this.button12_Click);
			// 
			// groupBox12
			// 
			this.groupBox12.Controls.Add(this.label20);
			this.groupBox12.Controls.Add(this.progressBar1);
			this.groupBox12.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox12.Location = new System.Drawing.Point(136, 32);
			this.groupBox12.Name = "groupBox12";
			this.groupBox12.Size = new System.Drawing.Size(136, 96);
			this.groupBox12.TabIndex = 22;
			this.groupBox12.TabStop = false;
			this.groupBox12.Text = "Progress:";
			// 
			// label20
			// 
			this.label20.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label20.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.label20.Location = new System.Drawing.Point(8, 24);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(120, 32);
			this.label20.TabIndex = 14;
			this.label20.Text = "%";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(8, 64);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(120, 24);
			this.progressBar1.Step = 1;
			this.progressBar1.TabIndex = 13;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(800, 633);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 5;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.label11);
			this.groupBox10.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox10.Location = new System.Drawing.Point(8, 656);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(1008, 48);
			this.groupBox10.TabIndex = 6;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "Message:";
			// 
			// label11
			// 
			this.label11.BackColor = System.Drawing.SystemColors.Info;
			this.label11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label11.Location = new System.Drawing.Point(8, 24);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(992, 16);
			this.label11.TabIndex = 7;
			this.label11.Text = "To load a data file press \"Load\" or \"Catalog\"...";
			// 
			// groupBox13
			// 
			this.groupBox13.Controls.Add(this.label19);
			this.groupBox13.Controls.Add(this.button21);
			this.groupBox13.Controls.Add(this.button20);
			this.groupBox13.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox13.Location = new System.Drawing.Point(888, 8);
			this.groupBox13.Name = "groupBox13";
			this.groupBox13.Size = new System.Drawing.Size(120, 136);
			this.groupBox13.TabIndex = 7;
			this.groupBox13.TabStop = false;
			this.groupBox13.Text = "Image Set";
			// 
			// label19
			// 
			this.label19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.label19.Location = new System.Drawing.Point(8, 56);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(40, 23);
			this.label19.TabIndex = 4;
			this.label19.Text = "Size:";
			// 
			// button21
			// 
			this.button21.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button21.Enabled = false;
			this.button21.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button21.Location = new System.Drawing.Point(64, 72);
			this.button21.Name = "button21";
			this.button21.Size = new System.Drawing.Size(32, 32);
			this.button21.TabIndex = 3;
			this.button21.Text = "-";
			this.button21.Click += new System.EventHandler(this.button21_Click);
			// 
			// button20
			// 
			this.button20.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.button20.Enabled = false;
			this.button20.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button20.Location = new System.Drawing.Point(64, 32);
			this.button20.Name = "button20";
			this.button20.Size = new System.Drawing.Size(32, 32);
			this.button20.TabIndex = 2;
			this.button20.Text = "+";
			this.button20.Click += new System.EventHandler(this.button20_Click);
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Location = new System.Drawing.Point(296, 152);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(568, 496);
			this.panel1.TabIndex = 8;
			// 
			// groupBox14
			// 
			this.groupBox14.Controls.Add(this.button22);
			this.groupBox14.Controls.Add(this.button19);
			this.groupBox14.Controls.Add(this.button18);
			this.groupBox14.Controls.Add(this.button12);
			this.groupBox14.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox14.Location = new System.Drawing.Point(8, 8);
			this.groupBox14.Name = "groupBox14";
			this.groupBox14.Size = new System.Drawing.Size(168, 136);
			this.groupBox14.TabIndex = 9;
			this.groupBox14.TabStop = false;
			this.groupBox14.Text = "File";
			// 
			// button22
			// 
			this.button22.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.button22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button22.ForeColor = System.Drawing.SystemColors.Desktop;
			this.button22.Location = new System.Drawing.Point(88, 40);
			this.button22.Name = "button22";
			this.button22.Size = new System.Drawing.Size(72, 88);
			this.button22.TabIndex = 10;
			this.button22.Text = "Catalog";
			this.button22.Click += new System.EventHandler(this.button22_Click);
			// 
			// button19
			// 
			this.button19.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.button19.Enabled = false;
			this.button19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button19.ForeColor = System.Drawing.SystemColors.Desktop;
			this.button19.Location = new System.Drawing.Point(8, 104);
			this.button19.Name = "button19";
			this.button19.Size = new System.Drawing.Size(72, 24);
			this.button19.TabIndex = 9;
			this.button19.Text = "Print";
			this.button19.Click += new System.EventHandler(this.button19_Click);
			// 
			// button18
			// 
			this.button18.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.button18.Enabled = false;
			this.button18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.button18.ForeColor = System.Drawing.SystemColors.Desktop;
			this.button18.Location = new System.Drawing.Point(8, 72);
			this.button18.Name = "button18";
			this.button18.Size = new System.Drawing.Size(72, 24);
			this.button18.TabIndex = 8;
			this.button18.Text = "Save";
			this.button18.Click += new System.EventHandler(this.button18_Click);
			// 
			// groupBox15
			// 
			this.groupBox15.Controls.Add(this.button17);
			this.groupBox15.Controls.Add(this.button14);
			this.groupBox15.Controls.Add(this.groupBox12);
			this.groupBox15.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.groupBox15.Location = new System.Drawing.Point(600, 8);
			this.groupBox15.Name = "groupBox15";
			this.groupBox15.Size = new System.Drawing.Size(280, 136);
			this.groupBox15.TabIndex = 10;
			this.groupBox15.TabStop = false;
			this.groupBox15.Text = "Calculation";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(872, 152);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(136, 496);
			this.pictureBox2.TabIndex = 22;
			this.pictureBox2.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.AutoScroll = true;
			this.AutoScrollMinSize = new System.Drawing.Size(800, 600);
			this.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.ClientSize = new System.Drawing.Size(1028, 710);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.groupBox15);
			this.Controls.Add(this.groupBox14);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.groupBox13);
			this.Controls.Add(this.groupBox10);
			this.Controls.Add(this.groupBox9);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox11);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Opt1.1";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.groupBox7.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
			this.groupBox6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			this.groupBox9.ResumeLayout(false);
			this.groupBox11.ResumeLayout(false);
			this.groupBox12.ResumeLayout(false);
			this.groupBox10.ResumeLayout(false);
			this.groupBox13.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.groupBox14.ResumeLayout(false);
			this.groupBox15.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
		delegate void ShowProgressDelegate (int progress, bool statusDone);

		/// <summary>
		/// Simple method that updates the text box & progress bar
		/// </summary>
		/// <param name="totalMessages"></param>
		/// <param name="messagesSoFar"></param>
		/// <param name="done"></param>
		private void ShowProgress ( int prog, bool done )
		{
			if (prog<=100)
			{
				progressBar1.Value = prog;
			}
			label20.Text=prog.ToString()+"%";
			if ( done )
			{
				if (state==5)
					state=6;
				else
                    state=4;
				restate();
				label20.Text="100%";
				label11.Text="Visibility Calculated...";
			}
		}

		private void button12_Click(object sender, System.EventArgs e)
		{
			openFileDialog1.Filter="ASC Files|*.asc";
			openFileDialog1.Title="Select a ASC File";
			if(openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				FileName = openFileDialog1.FileName;
				MapFileName = FileName.Substring(0,FileName.Length-3)+"jpg";
				ShowProgressDelegate showProgress = new ShowProgressDelegate(ShowProgress);
				int imsgs = 100;
				applicationLayer = new layer( this, showProgress, imsgs);
				applicationLayer.FileName=FileName;
				applicationLayer.catalog=false;
				applicationLayer.CSV_Parse();				
				this.label11.Text="Succesfuly loaded "+FileName+". Press 2D/3D/Contour to see terrain.";
				Bitmap bmp = applicationLayer.createMap(1);
				bmpBack=bmp;
				this.pictureBox1.Image=bmp;
				state=2;
				restate();
				view=2;
			}
		}

		private void button11_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer!=null)
			{
				//this.pictureBox1.Image=applicationLayer.create8bpp();	
				this.pictureBox1.Image=applicationLayer.createMap(1);
				bmpBack = new Bitmap(pictureBox1.Image);
				Redraw();
				Bitmap bmp = new Bitmap(pictureBox1.Image);
				pictureBox1.Image=applicationLayer.viewSolution(bmp);
				this.applicationLayer.count=0;
				view=2;
			}
		}

		private void button9_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer!=null)
			{
				this.pictureBox1.Image=applicationLayer.createMap(2);
				bmpBack = new Bitmap(pictureBox1.Image);
				Redraw();
				Bitmap bmp = new Bitmap(pictureBox1.Image);
				pictureBox1.Image=applicationLayer.viewSolution(bmp);
				this.applicationLayer.count=0;
				view=3;
			}		
		}

		/*private void progressUpdate()
		{
			while (true)
			{
				if(applicationLayer.v!=null)
				{
					label20.Text=applicationLayer.v.prog.ToString();
				}
				Thread.Sleep(1);
			}
		}*/

		private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
		{
			if (this.numericUpDown1.Value>1)
			{
				this.textBox2.Visible=true;
				this.label1.Visible=true;
			}
			else
			{
				this.textBox2.Visible=false;
				this.label1.Visible=false;
			}
			if (this.numericUpDown1.Value>2)
			{
				this.textBox3.Visible=true;
				this.label2.Visible=true;
			}
			else
			{
				this.textBox3.Visible=false;
				this.label2.Visible=false;
			}
			if (this.numericUpDown1.Value>3)
			{
				this.textBox4.Visible=true;
				this.label3.Visible=true;
			}
			else
			{
				this.textBox4.Visible=false;
				this.label3.Visible=false;
			}
			if (this.numericUpDown1.Value>4)
			{
				this.textBox5.Visible=true;
				this.label4.Visible=true;
			}
			else
			{
				this.textBox5.Visible=false;
				this.label4.Visible=false;
			}
			if (this.numericUpDown1.Value>5)
			{
				this.textBox6.Visible=true;
				this.label10.Visible=true;
			}
			else
			{
				this.textBox6.Visible=false;
				this.label10.Visible=false;
			}
			if (this.numericUpDown1.Value>6)
			{
				this.textBox7.Visible=true;
				this.label9.Visible=true;
			}
			else
			{
				this.textBox7.Visible=false;
				this.label9.Visible=false;
			}
			if (this.numericUpDown1.Value>7)
			{
				this.textBox8.Visible=true;
				this.label7.Visible=true;
			}
			else
			{
				this.textBox8.Visible=false;
				this.label7.Visible=false;
			}
			if (applicationLayer!=null)
			{
				applicationLayer.obNum=(int)numericUpDown1.Value;
			}

		}

		private void button15_Click(object sender, System.EventArgs e)
		{
			if (FileName!=null)
			{
				String File = FileName.Substring(0,FileName.Length-3)+"jpg";
				Bitmap bmp1 = new Bitmap(File);
				Bitmap bmp = new Bitmap(bmp1,pictureBox1.Image.Size);		
				pictureBox1.Image=bmp;
				bmpBack = new Bitmap(pictureBox1.Image);
				Redraw();
				Bitmap bmp2 = new Bitmap(pictureBox1.Image);
				pictureBox1.Image=applicationLayer.viewSolution(bmp2);
				this.applicationLayer.count=0;
				view=1;
			}
		}

		private void button17_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer!=null)
			{
				state=3;
				restate();
				Thread t = new Thread( new ThreadStart(applicationLayer.calculateVisibility));
				t.IsBackground = true; //make them a daemon - prevent thread callback issues
				t.Start();			
			}
		}
		
		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
		}

		private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if ((state==2)||(state==3)||(state==4)||(state==5)||(state==6))
			{
				try
				{
					double x=Form1.MousePosition.X;
					double y=Form1.MousePosition.Y;
					double pbx=pictureBox1.Location.X;
					double pby=pictureBox1.Location.Y;
					double f1x=Form1.ActiveForm.Location.X;
					double f1y=Form1.ActiveForm.Location.Y;
					x=(x-pbx-f1x-302)/applicationLayer.size;
					y=(y-pby-f1y-184)/applicationLayer.size;
					int dxx=(int)x+applicationLayer.xlcorner;
					int dyy=(int)y+applicationLayer.ylcorner;
					dx=(int)x;
					dy=(int)y;
					string xx=dxx.ToString();
					string yy=dyy.ToString();
					this.label16.Text=xx;
					this.label17.Text=yy;
					this.label18.Text=applicationLayer.theMap.theMap[dx,dy].ToString();
				}
				catch
				{
				}
			}
			if (state==1)
			{
				try
				{
					double x=Form1.MousePosition.X;
					double y=Form1.MousePosition.Y;
					double pbx=pictureBox1.Location.X;
					double pby=pictureBox1.Location.Y;
					double f1x=Form1.ActiveForm.Location.X;
					double f1y=Form1.ActiveForm.Location.Y;
					x=1+((x-pbx-f1x-302)/302)*18;
					y=1+((y-pby-f1y-184)/768)*46;
					dx=(int)x;
					dy=(int)y;
					string xx=dx.ToString();
					string yy=dy.ToString();
					this.label16.Text=xx;
					this.label17.Text=yy;
					this.label18.Text="#";
					this.MapX=dx.ToString();
					this.MapY=dy.ToString();
				}
				catch
				{
				}
			}

		}

		private void button16_Click(object sender, System.EventArgs e)
		{
			vs=true;	
		}

		private void pictureBox1_Click(object sender, System.EventArgs e)
		{
			if (state==1)
			{
				MapFileName = "c:/lib/Catalog/Area_"+MapY+"_"+MapX+".jpg";
				FileName = "c:/lib/Catalog/Area_"+MapY+"_"+MapX+".asc";				
				ShowProgressDelegate showProgress = new ShowProgressDelegate(ShowProgress);
				int imsgs = 100;
				applicationLayer = new layer( this, showProgress, imsgs);
				applicationLayer.FileName=FileName;
				applicationLayer.catalog=true;
				applicationLayer.CSV_Parse();
				this.label11.Text="Succesfuly loaded "+FileName+". Press Grayscale/Map/3D to see terrain.";
				Bitmap bmp = applicationLayer.createMap(1);
				bmpBack=bmp;
				this.pictureBox1.Image=bmp;
				state=2;
				restate();
				view=2;
				MessageBox.Show(this.applicationLayer.Load);
			}
			if ((vs==true)&&(applicationLayer.visibilityCalculated==true))
			{
				Bitmap original = new Bitmap (this.pictureBox1.Image);
				Bitmap bmp = applicationLayer.viewShed(dx,dy,original);
				pictureBox1.Image=bmp;
				int percent=((applicationLayer.count*100/((applicationLayer.theMap.Max_X)*(applicationLayer.theMap.Max_Y))));
				String str = percent.ToString()+"% covered";
				this.label11.Text=str;
				vs=false;
			}

			if ((ast==true))
			{
				int x=Form1.MousePosition.X;
				int y=Form1.MousePosition.Y;
				int pbx=pictureBox1.Location.X;
				int pby=pictureBox1.Location.Y;
				int f1x=Form1.ActiveForm.Location.X;
				int f1y=Form1.ActiveForm.Location.Y;
				x=(x-pbx-f1x-302);
				y=(y-pby-f1y-184);
				int size=applicationLayer.size;
				temp = new Point(dx,dy);
				pictureBox1.Image = (Bitmap)bmpBack.Clone();
				Graphics g = null;
				g = Graphics.FromImage(pictureBox1.Image);
				Color c = Color.Blue;
				Pen p = new Pen(c,size/2);				
				Point up1 = new Point (x,y-size); 
				Point up3 = new Point(x,y-3*size);
				Point down1 = new Point(x,y+size);
				Point down3 = new Point(x,y+3*size);
				Point left1 = new Point (x-size,y); 
				Point left3 = new Point(x-3*size,y);
				Point right1 = new Point(x+size,y);
				Point right3 = new Point(x+3*size,y);
				g.DrawLine(p,up1,up3);
				g.DrawLine(p,down1,down3);
				g.DrawLine(p,left1,left3);
				g.DrawLine(p,right1,right3);
				pictureBox1.Refresh();
			}
		}
		private void button14_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer.visibilityCalculated)
			{
				state=5;
				restate();
				Thread t = new Thread( new ThreadStart(applicationLayer.Solve));
				t.IsBackground = true; //make them a daemon - prevent thread callback issues
				t.Start();

				//this.applicationLayer.Solve();
			}

		}
		private void EnableButton ( bool flag )
		{
			button17.Enabled = flag;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (mark1==null)
				{
					mark1 = new MarkControl();
					mark1.Location = new Point(50,200);
					pictureBox1.Controls.Add(mark1);
					mark1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseUp);
					mark1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseDown);
					mark1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseMove);
					mark1.Cursor = System.Windows.Forms.Cursors.SizeAll;
				}
				if (mark2==null)
				{
					mark2 = new MarkControl();
					mark2.Location = new Point(100,50);
					pictureBox1.Controls.Add(mark2);
					mark2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseUp);
					mark2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseDown);
					mark2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseMove);
					mark2.Cursor = System.Windows.Forms.Cursors.SizeAll;
				}
				if (mark3==null)
				{
					mark3 = new MarkControl();
					mark3.Location = new Point(150,50);
					pictureBox1.Controls.Add(mark3);
					mark3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseUp);
					mark3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseDown);
					mark3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseMove);
					mark3.Cursor = System.Windows.Forms.Cursors.SizeAll;
				}
				if (mark4==null)
				{
					mark4 = new MarkControl();
					mark4.Location = new Point(200,50);
					pictureBox1.Controls.Add(mark4);
					mark4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseUp);
					mark4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseDown);
					mark4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseMove);
					mark4.Cursor = System.Windows.Forms.Cursors.SizeAll;
				}
				if (mark5==null)
				{
					mark5 = new MarkControl();
					mark5.Location = new Point(250,100);
					pictureBox1.Controls.Add(mark5);
					mark5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseUp);
					mark5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseDown);
					mark5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mark_MouseMove);
					mark5.Cursor = System.Windows.Forms.Cursors.SizeAll;
				}
				Redraw();
				button6.Enabled=false;
				button2.Enabled=false;
				button4.Enabled=true;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}


		}

		private void Redraw()
		{
			pictureBox1.Image = (Bitmap)bmpBack.Clone();
			Graphics g = null;
			g = Graphics.FromImage(pictureBox1.Image);
			Point[]p=new Point[5];
			if (mark1!=null)
			{
				p[0]=new Point(mark1.Center.X,mark1.Center.Y);			
				p[1]=new Point(mark2.Center.X,mark2.Center.Y);
				p[2]=new Point(mark3.Center.X,mark3.Center.Y);
				p[3]=new Point(mark4.Center.X,mark4.Center.Y);
				p[4]=new Point(mark5.Center.X,mark5.Center.Y);			
			}
			Brush r=new SolidBrush(Color.Red);
			Brush b=new SolidBrush(Color.LightBlue);
			Pen myPen = new Pen(r,10f);
			Pen myPen2 = new Pen(b,10f);
			float[]ar = new float[4];
			ar[0] = 0.0f;
			ar[1] = 0.4f;
			ar[2] = 1f;
			ar[3] = 1f;
			myPen.CompoundArray=ar;
			float[]ar2 = new float[4];
			ar2[0] = 0.0f;
			ar2[1] = 0.0f;
			ar2[2] = 0.6f;
			ar2[3] = 1f;
			myPen2.CompoundArray=ar2;
			g.DrawClosedCurve(myPen,p);
			g.DrawClosedCurve(myPen2,p);			
			SolidBrush bsh = new SolidBrush( Color.FromArgb(40,0,100,255) );
			g.FillClosedCurve(bsh,p,System.Drawing.Drawing2D.FillMode.Alternate);			
			g.Dispose();
			pictureBox1.Refresh();	
		}
		private void Redraw2()
		{
			Bitmap bmp=null;
			Bitmap bmp1=null;
			if (view==1)
			{
				bmp1=new Bitmap(MapFileName);
				bmp = new Bitmap(bmp1,pictureBox1.Image.Size);
			}
			if (view==2)
			{
				bmp1=applicationLayer.createMap(1);
				bmp = new Bitmap(bmp1,pictureBox1.Image.Size);
			}
			if (view==3)
			{
				bmp1=applicationLayer.createMap(2);
				bmp = new Bitmap(bmp1,pictureBox1.Image.Size);
			}
			bmp = this.applicationLayer.viewSolution(bmp);
			pictureBox1.Image = bmp;
			bmpBack=bmp;

			/*if (applicationLayer.Solution!=null)
			{
				pictureBox1.Image = (Bitmap)bmpBack.Clone();
				Graphics g = null;
				g = Graphics.FromImage(pictureBox1.Image);
				for (int i=0;i<10;i++)
				{
					Color c = Color.Blue;
					SolidBrush b = new SolidBrush(c);
					int size = applicationLayer.size;
					Pen p = new Pen(c,size);
					Point center = (Point)applicationLayer.Solution[i];
					center.X=center.X*size;
					center.Y=center.Y*size;
					Point[]triangle=new Point[3];
					triangle[0] = new Point(center.X,center.Y-size);
					triangle[1] = new Point(center.X+size,center.Y+size);
					triangle[2] = new Point(center.X-size,center.Y+size);
					g.FillPolygon(b,triangle);
					g.Dispose();
					pictureBox1.Refresh();
				}
			}*/

		}

		private void Mark_MouseDown(object sender, MouseEventArgs e)
		{
			this.SuspendLayout();
			isSelected = true;
			_X = e.X;
			_Y = e.Y;
		}
		private void Mark_MouseMove(object sender, MouseEventArgs e)
		{
			if((isSelected)&&(button4.Enabled))
			{			
				MarkControl mc1 = (MarkControl)sender;
				Point p = new Point(e.X - _X + mc1.Left, e.Y - _Y + mc1.Top);
				mc1.Location=p;
				Redraw();
			}	
		}
		private void Mark_MouseUp(object sender, MouseEventArgs e)
		{
			isSelected = false; 
			ResumeLayout();
			Redraw();
		}
		private void Delete_Click(object sender, System.EventArgs e)
		{
			Redraw();
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			button4.Enabled=true;
			button6.Enabled=false;
			if (applicationLayer!=null)
			{
				applicationLayer.border=false;
				button2.Enabled=true;
				button4.Enabled=false;
				resize();
			}
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			if(this.applicationLayer!=null)
			{
				button6.Enabled=true;
				button4.Enabled=false;
				double x=Form1.MousePosition.X;
				double y=Form1.MousePosition.Y;
				double pbx=pictureBox1.Location.X;
				double pby=pictureBox1.Location.Y;
				double f1x=Form1.ActiveForm.Location.X;
				double f1y=Form1.ActiveForm.Location.Y;
				x=this.panel1.DisplayRectangle.X;
				y=this.panel1.DisplayRectangle.Y;
				Point[]p = new Point[5];
				p[0].X=(int)(mark1.Center.X-pbx-f1x+x)/applicationLayer.size;
				p[0].Y=(int)(mark1.Center.Y-pby-f1y+y)/applicationLayer.size;
				p[1].X=(int)(mark2.Center.X-pbx-f1x+x)/applicationLayer.size;
				p[1].Y=(int)(mark2.Center.Y-pby-f1y+y)/applicationLayer.size;
				p[2].X=(int)(mark3.Center.X-pbx-f1x+x)/applicationLayer.size;
				p[2].Y=(int)(mark3.Center.Y-pby-f1y+y)/applicationLayer.size;
				p[3].X=(int)(mark4.Center.X-pbx-f1x+x)/applicationLayer.size;
				p[3].Y=(int)(mark4.Center.Y-pby-f1y+y)/applicationLayer.size;
				p[4].X=(int)(mark5.Center.X-pbx-f1x+x)/applicationLayer.size;
				p[4].Y=(int)(mark5.Center.Y-pby-f1y+y)/applicationLayer.size;
				GraphicsPath path = new GraphicsPath();
				path.AddClosedCurve(p);
				applicationLayer.createBorder(path,p);
				Point pp = new Point(-10,-10);
				mark1.Location=pp;
				mark2.Location=pp;
				mark3.Location=pp;
				mark4.Location=pp;
				mark5.Location=pp;
				Redraw();
				pictureBox1.Controls.Clear();
				mark1.Dispose();
				mark2.Dispose();
				mark3.Dispose();
				mark4.Dispose();
				mark5.Dispose();
				mark1=null;
				mark2=null;
				mark3=null;
				mark4=null;
				mark5=null;
				Bitmap bmp = new Bitmap(this.pictureBox1.Image);
				bmp = applicationLayer.viewSolution(bmp);
				pictureBox1.Image=bmp;
				resize();
			}			
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer!=null)
			{
				button1.Enabled=true;
				button5.Enabled=false;
				ast=true;
			}	
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			if (ast==true)
			{
				button5.Enabled=true;
				bmpBack=(Bitmap)pictureBox1.Image;
				applicationLayer.AddAstPoint(temp);
				ast=false;
				applicationLayer.strategic=true;
			}
		
		}
		unsafe private void button10_Click(object sender, System.EventArgs e)
		{
			Bitmap bmp = applicationLayer.create8bpp();			
			Bitmap bmp2 = new Bitmap(bmp,512,512);	
			SaveBMPWithNewColorTable(bmp2,"c:/lib/heightmap.bmp",256,false);			
			Bitmap bmpt = applicationLayer.viewSolution(pictureBox1.Image);
			Bitmap bmpt2 = new Bitmap(bmpt,512,512);
			bmpt2.Save("c:/lib/texture.bmp",ImageFormat.Bmp);
			Process.Start("c:/lib/terrain engine/3D_C.exe");
		
		}
		protected ColorPalette GetColorPalette( uint nColors )
		{
			// Assume monochrome image.
			PixelFormat     bitscolordepth = PixelFormat.Format1bppIndexed;
			ColorPalette    palette;    // The Palette we are stealing
			Bitmap          bitmap;     // The source of the stolen palette

			// Determine number of colors.
			if (nColors > 2)
				bitscolordepth = PixelFormat.Format4bppIndexed;
			if (nColors > 16)
				bitscolordepth = PixelFormat.Format8bppIndexed;

			// Make a new Bitmap object to get its Palette.
			bitmap = new Bitmap( 1, 1, bitscolordepth );

			palette = bitmap.Palette;   // Grab the palette
    
			bitmap.Dispose();           // cleanup the source Bitmap

			return palette;             // Send the palette back
		}
		protected void SaveBMPWithNewColorTable(
			Image       image,
			string      filename,
			uint        nColors,
			bool        fTransparent
			)
		{

			// GIF codec supports 256 colors maximum, monochrome minimum.
			if (nColors > 256)
				nColors = 256;
			if (nColors < 2)
				nColors = 2;

			// Make a new 8-BPP indexed bitmap that is the same size as the source image.
			int   Width = image.Width;
			int   Height = image.Height;

			// Always use PixelFormat8bppIndexed because that is the color
			// table-based interface to the GIF codec.
			Bitmap  bitmap = new Bitmap(Width, 
				Height, 
				PixelFormat.Format8bppIndexed); 

			// Create a color palette big enough to hold the colors you want.
			ColorPalette pal = GetColorPalette(nColors);

			// Initialize a new color table with entries that are determined
			// by some optimal palette-finding algorithm; for demonstration 
			// purposes, use a grayscale.
			for (uint i = 0; i < nColors; i++)
			{
				uint Alpha = 0xFF;                      // Colors are opaque.
				uint Intensity = i*0xFF/(nColors-1);    // Even distribution. 

				// The GIF encoder makes the first entry in the palette
				// that has a ZERO alpha the transparent color in the GIF.
				// Pick the first one arbitrarily, for demonstration purposes.

				if ( i == 0 && fTransparent) // Make this color index...
					Alpha = 0;          // Transparent
    
				// Create a gray scale for demonstration purposes.
				// Otherwise, use your favorite color reduction algorithm
				// and an optimum palette for that algorithm generated here.
				// For example, a color histogram, or a median cut palette.
				pal.Entries[i] = Color.FromArgb( (int)Alpha, 
					(int)Intensity, 
					(int)Intensity, 
					(int)Intensity );
			}

			// Set the palette into the new Bitmap object.
			bitmap.Palette = pal;


			// Use GetPixel below to pull out the color data of Image.
			// Because GetPixel isn't defined on an Image, make a copy 
			// in a Bitmap instead. Make a new Bitmap that is the same size as the
			// image that you want to export. Or, try to
			// interpret the native pixel format of the image by using a LockBits
			// call. Use PixelFormat32BppARGB so you can wrap a Graphics  
			// around it.
			Bitmap BmpCopy = new Bitmap(Width, 
				Height, 
				PixelFormat.Format32bppArgb); 
		{
			Graphics g = Graphics.FromImage(BmpCopy);

			g.PageUnit = GraphicsUnit.Pixel;

			// Transfer the Image to the Bitmap
			g.DrawImage(image, 0, 0, Width, Height);

			// g goes out of scope and is marked for garbage collection.
			// Force it, just to keep things clean.
			g.Dispose();
		}

			// Lock a rectangular portion of the bitmap for writing.
			BitmapData  bitmapData;
			Rectangle   rect = new Rectangle(0, 0, Width, Height);

			bitmapData = bitmap.LockBits(
				rect,
				ImageLockMode.WriteOnly,
				PixelFormat.Format8bppIndexed);

			// Write to the temporary buffer that is provided by LockBits.
			// Copy the pixels from the source image in this loop.
			// Because you want an index, convert RGB to the appropriate
			// palette index here.
			IntPtr pixels = bitmapData.Scan0;

			unsafe 
			{ 
				// Get the pointer to the image bits.
				// This is the unsafe operation.
				byte *  pBits;
				if (bitmapData.Stride > 0)
					pBits = (byte *)pixels.ToPointer();
				else
					// If the Stide is negative, Scan0 points to the last 
					// scanline in the buffer. To normalize the loop, obtain
					// a pointer to the front of the buffer that is located 
					// (Height-1) scanlines previous.
					pBits = (byte *)pixels.ToPointer() + bitmapData.Stride*(Height-1);
				uint stride = (uint)Math.Abs(bitmapData.Stride);

				for ( uint row = 0; row < Height; ++row )
				{
					for ( uint col = 0; col < Width; ++col )
					{
						// Map palette indexes for a gray scale.
						// If you use some other technique to color convert,
						// put your favorite color reduction algorithm here.
						Color     pixel;    // The source pixel.

						// The destination pixel.
						// The pointer to the color index byte of the
						// destination; this real pointer causes this
						// code to be considered unsafe.
						byte *    p8bppPixel = pBits + row*stride + col;

						pixel = BmpCopy.GetPixel((int)col, (int)row);

						// Use luminance/chrominance conversion to get grayscale.
						// Basically, turn the image into black and white TV.
						// Do not calculate Cr or Cb because you 
						// discard the color anyway.
						// Y = Red * 0.299 + Green * 0.587 + Blue * 0.114

						// This expression is best as integer math for performance,
						// however, because GetPixel listed earlier is the slowest 
						// part of this loop, the expression is left as 
						// floating point for clarity.

						double luminance = (pixel.R *0.299) +
							(pixel.G *0.587) +
							(pixel.B *0.114);

						// Gray scale is an intensity map from black to white.
						// Compute the index to the grayscale entry that
						// approximates the luminance, and then round the index.
						// Also, constrain the index choices by the number of
						// colors to do, and then set that pixel's index to the 
						// byte value.
						*p8bppPixel = (byte)(luminance * (nColors-1)/255 +0.5);

					} /* end loop for col */ 
				} /* end loop for row */ 
			} /* end unsafe */ 

			// To commit the changes, unlock the portion of the bitmap.  
			bitmap.UnlockBits(bitmapData);

			bitmap.Save(filename, ImageFormat.Bmp);

			// Bitmap goes out of scope here and is also marked for
			// garbage collection.
			// Pal is referenced by bitmap and goes away.
			// BmpCopy goes out of scope here and is marked for garbage
			// collection. Force it, because it is probably quite large.
			// The same applies to bitmap.
			BmpCopy.Dispose();
			bitmap.Dispose();

		}

		private void button22_Click(object sender, System.EventArgs e)
		{
			Bitmap bmp = new Bitmap("c:/lib/Catalog/Map.jpg");
			pictureBox1.Image=bmp;
			state=1;
			restate();
			this.label11.Text="Choose an area of the map.";
		}
		private void restate()
		{
			if (state==0)
			{
				this.button1.Enabled=false;
				this.button2.Enabled=false;
				this.button3.Enabled=false;
				this.button4.Enabled=false;
				this.button5.Enabled=false;
				this.button6.Enabled=false;
				this.button8.Enabled=false;
				this.button9.Enabled=false;
				this.button10.Enabled=false;
				this.button11.Enabled=false;
				this.button12.Enabled=true;
				this.button13.Enabled=false;
				this.button14.Enabled=false;
				this.button15.Enabled=false;
				this.button16.Enabled=false;
				this.button17.Enabled=false;
				this.button18.Enabled=false;
				this.button19.Enabled=false;
				this.button20.Enabled=false;
				this.button21.Enabled=false;
				this.button22.Enabled=true;
				this.numericUpDown1.Enabled=true;
				this.numericUpDown2.Enabled=true;
				this.numericUpDown4.Enabled=true;

			}
			if (state==1)
			{
				this.button1.Enabled=false;
				this.button2.Enabled=false;
				this.button3.Enabled=false;
				this.button4.Enabled=false;
				this.button5.Enabled=false;
				this.button6.Enabled=false;
				this.button7.Enabled=false;
				this.button8.Enabled=false;
				this.button9.Enabled=false;
				this.button10.Enabled=false;
				this.button11.Enabled=false;
				this.button12.Enabled=true;
				this.button13.Enabled=false;
				this.button14.Enabled=false;
				this.button15.Enabled=false;
				this.button16.Enabled=false;
				this.button17.Enabled=false;
				this.button18.Enabled=false;
				this.button19.Enabled=true;
				this.button20.Enabled=false;
				this.button21.Enabled=false;
				this.button22.Enabled=true;
			}
			if (state==2)
			{
				this.button1.Enabled=false;
				this.button2.Enabled=true;
				this.button3.Enabled=true;
				this.button4.Enabled=false;
				this.button5.Enabled=false;
				this.button6.Enabled=false;
				this.button7.Enabled=false;
				this.button8.Enabled=true;
				this.button9.Enabled=true;
				this.button10.Enabled=true;
				this.button11.Enabled=true;
				this.button12.Enabled=false;
				this.button13.Enabled=true;
				this.button14.Enabled=false;
				this.button15.Enabled=true;
				this.button16.Enabled=true;
				this.button17.Enabled=true;
				this.button18.Enabled=true;
				this.button19.Enabled=true;
				this.button20.Enabled=true;
				this.button21.Enabled=true;
				this.button22.Enabled=false;
			}
			if (state==3)
			{
				this.button1.Enabled=false;
				this.button2.Enabled=false;
				this.button3.Enabled=false;
				this.button4.Enabled=false;
				this.button5.Enabled=false;
				this.button6.Enabled=false;
				this.button7.Enabled=false;
				this.button8.Enabled=true;
				this.button9.Enabled=true;
				this.button10.Enabled=false;
				this.button11.Enabled=true;
				this.button12.Enabled=false;
				this.button13.Enabled=false;
				if (eol==false)
                    this.button14.Enabled=false;
				this.button15.Enabled=true;
				this.button16.Enabled=true;
				this.button17.Enabled=false;
				this.button18.Enabled=false;
				this.button19.Enabled=false;
				this.button20.Enabled=true;
				this.button21.Enabled=true;
				this.button22.Enabled=false;
				this.numericUpDown2.Enabled=false;
				this.numericUpDown4.Enabled=false;
			}
			if (state==4)
			{
				this.button1.Enabled=true;
				this.button2.Enabled=true;
				this.button3.Enabled=true;
				this.button4.Enabled=true;
				this.button5.Enabled=true;
				this.button6.Enabled=true;
				this.button7.Enabled=true;
				this.button8.Enabled=true;
				this.button9.Enabled=true;
				this.button10.Enabled=true;
				this.button11.Enabled=true;
				this.button12.Enabled=true;
				this.button13.Enabled=true;
				this.button14.Enabled=true;
				this.button15.Enabled=true;
				this.button16.Enabled=true;
				this.button17.Enabled=true;
				this.button18.Enabled=true;
				this.button19.Enabled=true;
				this.button20.Enabled=true;
				this.button21.Enabled=true;
				this.button22.Enabled=true;
			}
			if (state==5)
			{
				this.button1.Enabled=false;
				this.button2.Enabled=false;
				this.button3.Enabled=false;
				this.button4.Enabled=false;
				this.button5.Enabled=false;
				this.button6.Enabled=false;
				this.button7.Enabled=false;
				this.button8.Enabled=false;
				this.button9.Enabled=true;
				this.button10.Enabled=false;
				this.button11.Enabled=true;
				this.button12.Enabled=false;
				this.button13.Enabled=false;
				this.button14.Enabled=false;
				this.button15.Enabled=false;
				this.button16.Enabled=false;
				this.button17.Enabled=false;
				this.button18.Enabled=false;
				this.button19.Enabled=false;
				this.button20.Enabled=true;
				this.button21.Enabled=true;
				this.button22.Enabled=false;
				this.numericUpDown1.Enabled=false;
			}
			if (state==6)
			{
				this.button1.Enabled=false;
				this.button2.Enabled=false;
				this.button3.Enabled=false;
				this.button4.Enabled=false;
				this.button5.Enabled=false;
				this.button6.Enabled=false;
				this.button7.Enabled=false;
				this.button8.Enabled=false;
				this.button9.Enabled=true;
				this.button10.Enabled=true;
				this.button11.Enabled=true;
				this.button12.Enabled=false;
				this.button13.Enabled=true;
				this.button14.Enabled=false;
				this.button15.Enabled=true;
				this.button16.Enabled=false;
				this.button17.Enabled=true;
				this.button18.Enabled=true;
				this.button19.Enabled=true;
				this.button20.Enabled=true;
				this.button21.Enabled=true;
				this.button22.Enabled=false;
				this.numericUpDown1.Enabled=false;
				eol=true;
				resize();
				this.label11.Text="Solution calculated";
				if (applicationLayer.observers!=null)
				{
					this.textBox1.Text=applicationLayer.observers[0];
					if (applicationLayer.observers.Length>1)
						this.textBox2.Text=applicationLayer.observers[1];
					if (applicationLayer.observers.Length>2)
						this.textBox3.Text=applicationLayer.observers[2];
					if (applicationLayer.observers.Length>3)
						this.textBox4.Text=applicationLayer.observers[3];
					if (applicationLayer.observers.Length>4)
						this.textBox5.Text=applicationLayer.observers[4];
					if (applicationLayer.observers.Length>5)
						this.textBox6.Text=applicationLayer.observers[5];
					if (applicationLayer.observers.Length>6)
						this.textBox7.Text=applicationLayer.observers[6];
					if (applicationLayer.observers.Length>7)
						this.textBox8.Text=applicationLayer.observers[7];		
					textBox1.Enabled=false;
					textBox2.Enabled=false;
					textBox3.Enabled=false;
					textBox4.Enabled=false;
					textBox5.Enabled=false;
					textBox6.Enabled=false;
					textBox7.Enabled=false;
					textBox8.Enabled=false;
				}



			}
		}

		private void button13_Click(object sender, System.EventArgs e)
		{
			eol=false;
			state=0;
			this.applicationLayer=null;			
			restate();
			this.label11.Text="To load a data file press 'Load' or 'Catalog'...";
		}
		private void resize()
		{
			pictureBox1.Image = applicationLayer.createMap(1);
			Redraw2();
		}


		private void button20_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer.size<10)
			{
				applicationLayer.size=applicationLayer.size+1;
				resize();
				this.label11.Text="Size Ratio - 1:"+applicationLayer.size.ToString();
			}
		}

		private void button21_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer.size>1)
			{
				applicationLayer.size=applicationLayer.size-1;
				resize();
				this.label11.Text="Size Ratio - 1 : "+applicationLayer.size.ToString();
			}		
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			button3.Enabled=true;
			button1.Enabled=false;
			button5.Enabled=false;
			applicationLayer.strategic=false;
			applicationLayer.astPoints=new ArrayList();
			resize();
		}

		private void numericUpDown4_ValueChanged(object sender, System.EventArgs e)
		{
			if ((state!=0)&&(state!=1))
			{
				applicationLayer.targetHeight=(int)numericUpDown4.Value;
			}
			else
			{
				numericUpDown4.Value=1;
			}
		}

		private void numericUpDown2_ValueChanged(object sender, System.EventArgs e)
		{
			if ((state!=0)&&(state!=1))
			{
				applicationLayer.observerHeight=(int)numericUpDown2.Value;
			}
			else
			{
				numericUpDown2.Value=1;
			}
		
		}

		private void button18_Click(object sender, System.EventArgs e)
		{
			saveFileDialog1.Filter="JPG Files|*.jpg";
			saveFileDialog1.Title="Select a CSV File";
			if(saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				FileName = saveFileDialog1.FileName;
				Bitmap bmp = new Bitmap (this.pictureBox1.Image);
				bmp.Save(FileName,System.Drawing.Imaging.ImageFormat.Jpeg);
			}
		}

		private void button19_Click(object sender, System.EventArgs e)
		{
		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			if (applicationLayer!=null)
			{
				applicationLayer.observers=new String[(int)this.numericUpDown1.Value];
			}
			textBox1.Enabled=false;
			textBox2.Enabled=false;
			textBox3.Enabled=false;
			textBox4.Enabled=false;
			textBox5.Enabled=false;
			textBox6.Enabled=false;
			textBox7.Enabled=false;
			textBox8.Enabled=false;
			button8.Enabled=false;
			button7.Enabled=true;
			numericUpDown1.Enabled=false;
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			numericUpDown1.Enabled=true;
			button7.Enabled=false;
			button8.Enabled=true;
			textBox1.Enabled=true;
			textBox2.Enabled=true;
			textBox3.Enabled=true;
			textBox4.Enabled=true;
			textBox5.Enabled=true;
			textBox6.Enabled=true;
			textBox7.Enabled=true;
			textBox8.Enabled=true;

			textBox1.Text="";
			textBox2.Text="";
			textBox3.Text="";
			textBox4.Text="";
			textBox5.Text="";
			textBox6.Text="";
			textBox7.Text="";
			textBox8.Text="";

		}

	}
}

