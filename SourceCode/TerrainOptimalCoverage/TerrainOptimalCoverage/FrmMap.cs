﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
//using System.Windows.Forms.DataVisualization.Charting;
//yoav-My
namespace TerrainOptimalCoverage
{
    public partial class FrmMap : Form
    {
        float HeightMin, HeightMax;
        float ScaleHeightTo = 500;
        private float CellSize;
        private int TerrainWidth;    
        private int TerrainHeight;  
        private float xllcorner;
        private float yllcorner;
        public float[,] heightData; 
        int[,] heightDataReches;
        int x1;
        int y1;
        int x2;
        int y2;
        float degel = 2;
        int x = 0;
        int y = 0;
        int[] XYarrayAscPosition = new int[4];
        int[] ArrayRangeFireTarget = new int[4];
        bool [,] ArrayRangeFireVisibility;  //מטריצת הנראות עבור טווח הירי בין הרדיוס הפנימי לחיצוני
        bool[,] ArrayRangeFireAttack;  //מטריצת שמייצגת את כל הנקודות שבהם ניתן למקם עמדת ירי
        int[] HightPointsBetweenLine;
        int[,] HightPointsBetweenLineForAllFireRange;//בהקבללה ArrayRangeFireAttack מטריצה שמכילה בכל שורה את הקוארדינטות של כל תא במטריצת  
        float[] AscArraySideCat;
        bool FlagMatrix = false;
        bool FlagDrawLine = false;
        bool FlagShowElements = false;
        bool FlagTrafficRoute = false;
        bool FlagDrawRectForSideCut = false;
        bool FlagTarget = false; 
        bool FlagAnalysisPointOnRoute=false;
        bool FlagcreateSideCutWithDistance = false;
        int CounterTrafficRoute = 0;
        const int ConstArrayTrafficRoute = 1000;
        int[] ArrayTrafficRoute = new int[ConstArrayTrafficRoute]; //up to 500 step
        int[] rectDrawAraay = new int[] { -10, -10, -10, -10 };
        int[,] TgtPos;
        int xg, yg;
        int IndexTrafficRouteFireArmTable = -1;
        
        string strPathImg = Application.StartupPath + "\\Maps\\";
        string strFileImg = Application.StartupPath + "\\Maps\\" + "HeightMap.jpg";
        string strPathAsc = Application.StartupPath + "\\Maps\\";
        string strFileAsc = Application.StartupPath + "\\Maps\\" + "HeightMap.asc";
        string strDate = DateTime.Now.ToString();
        ClsGrafix Draw1;
        
        int pictureBox1Width;
        int pictureBox1Height;
        DataSet2TableAdapters.FirearmsTableAdapter Ds = new DataSet2TableAdapters.FirearmsTableAdapter();
        Color colAttackTrafficRoute;
       
        public FrmMap()
        {
            InitializeComponent();
        }

        private void FrmMap_Load(object sender, EventArgs e)
        {
            colAttackTrafficRoute=Color.Blue;
            toolStripSideCat.Enabled = false;
            toolStripShowPointsOnLine.Enabled = false;
            toolStripVisibility.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute1.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute2.Enabled = false;
            toolStripButtonShowElementsOnRaduis.Enabled = false;
            toolStripStatusLabel1.Visible = false;
            FlagcreateSideCutWithDistance = false;
            strDate = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            strPathImg = strPathImg + "HeightMap" + strDate + ".jpg";
            strPathAsc = strPathAsc + "HeightMap" + strDate + ".asc";
            System.IO.File.Move(strFileAsc, strPathAsc);
            System.IO.File.Move(strFileImg, strPathImg);
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            pictureBox1.Top = 25;
            pictureBox1.Left = 0;
            //progressBar1.Left = this.Left;
            //progressBar1.Top = this.Bottom;
            Draw1 = new ClsGrafix(pictureBox1);
             this.Text = "HeightMap" + strDate + ".asc"; //form name
            __LoadHeightDataAscFile(strPathAsc, ScaleHeightTo);
            //toolStripComboZoom.Text = "x1";

            ClsGlobal.LoadFireArmsTable();
           
        }

       

        private void _LoadHeightDataAscFile(string file, float ScaleTo)
        {
            //terrainWidth =  heightMap.Width;
            //terrainHeight =  heightMap.Height;
            int x = 0, y = 0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight = Convert.ToInt16(_T);
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle(_T);
           
            heightData = new float[TerrainWidth, TerrainHeight];

            //LoadAddData

            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    __T = Convert.ToSingle(_x[x]);
                    if (__T != -9999F) heightData[x, y] = __T;
                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = heightData[x, y];
                        HeightMin = heightData[x, y];
                    }
                    HeightMin = Math.Min(heightData[x, y], HeightMin);
                    HeightMax = Math.Max(heightData[x, y], HeightMax);
                }
            }

            //reduce minimum and Scale to 1024
            for (x = 0; x < TerrainHeight; x++)
            {
                string[] _x = T[6 + x].Split();
                for (y = 0; y < TerrainWidth; y++)
                {
                    heightData[x, y] = Convert.ToSingle(_x[y]); //* ScaleHeightTo / (HeightMax - HeightMin) - HeightMin;

                }
            }

        }

        private void LoadHeightDataAscFile(string file, float ScaleTo)
        {
            //terrainWidth =  heightMap.Width;
            //terrainHeight =  heightMap.Height;
            int x = 0, y = 0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight = Convert.ToInt16(_T);
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle(_T);

            heightData = new float[TerrainWidth, TerrainHeight];

            //LoadAddData

            for (y = 0; y < TerrainWidth; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainHeight; x++)
                {
                    __T = Convert.ToSingle(_x[x]);
                    if (__T != -9999F) heightData[x, y] = __T;
                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = heightData[x, y];
                        HeightMin = heightData[x, y];
                    }
                    HeightMin = Math.Min(heightData[x, y], HeightMin);
                    HeightMax = Math.Max(heightData[x, y], HeightMax);
                }
            }

            //reduce minimum and Scale to 1024
            for (x = 0; x < TerrainWidth; x++)
            {
                string[] _x = T[6 + x].Split();
                for (y = 0; y < TerrainHeight; y++)
                {
                    heightData[x, y] = Convert.ToSingle(_x[y]); //* ScaleHeightTo / (HeightMax - HeightMin) - HeightMin;

                }
            }

        }

        private void __LoadHeightDataAscFile(string file, float ScaleTo)
        {
            //terrainWidth =  heightMap.Width;
            //terrainHeight =  heightMap.Height;
            int x = 0, y = 0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight = Convert.ToInt16(_T);

            _T = T[2].Replace("xllcorner", "");
            _T = _T.Replace(" ", "");
            xllcorner = Convert.ToSingle(_T);

            _T = T[3].Replace("yllcorner", "");
            _T = _T.Replace(" ", "");
            yllcorner = Convert.ToSingle(_T);

           
            
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle(_T);
            heightData = new float[TerrainHeight, TerrainWidth];

            //LoadAddData
            
            //find min/max
            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    __T = Convert.ToSingle(_x[x]);
                    if (__T != -9999F) heightData[y, x] = __T;
                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = heightData[y, x];
                        HeightMin = heightData[y, x];
                    }
                    HeightMin = Math.Min(heightData[y, x], HeightMin);
                    HeightMax = Math.Max(heightData[y, x], HeightMax);
                }
            }
            
            //reduce minimum and Scale to 1024
            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                   heightData[y, x] = Convert.ToSingle(_x[x])-HeightMin; //* ScaleHeightTo / (HeightMax - HeightMin) - HeightMin;
                }
            }
            //pictureBox1Height = TerrainHeight * 1;
            //pictureBox1Width = TerrainWidth * 1;
            //pictureBox1.Height = pictureBox1Height;
            //pictureBox1.Width = pictureBox1Width;
            pictureBox1Height = TerrainHeight * pictureBox1.Height / TerrainHeight;
            pictureBox1Width = TerrainWidth * pictureBox1.Width / TerrainWidth;
            this.Width = pictureBox1Width;
            this.Height = pictureBox1Height + 50;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            xg = x - this.Left - pictureBox1.Left - 4;
            yg = y - this.Top - pictureBox1.Top - 92;

           
            
                FlagDrawLine = false;
                if (degel % 2 == 0 && FlagTarget == false && FlagTrafficRoute == false && FlagDrawRectForSideCut)
                {
                    pictureBox1.Image = Bitmap.FromFile(strPathImg);
                    Draw1 = new ClsGrafix(pictureBox1);
                    toolStripSideCat.Enabled = false;
                    toolStripShowPointsOnLine.Enabled = false;
                    toolStripVisibility.Enabled = false;
                    x1 = x - this.Left - pictureBox1.Left - 4;
                    y1 = y - this.Top - pictureBox1.Top - 92;
                    //x1 = Convert.ToInt16(x - this.Left - pictureBox1.Left - 4) / (Convert.ToInt16(pictureBox1Width) / Convert.ToInt16(TerrainWidth));
                    //y1 = Convert.ToInt16(y - this.Top - pictureBox1.Top - 92) / (Convert.ToInt16(pictureBox1Height) / Convert.ToInt16(TerrainHeight));

                    // toolStripLabelX.Text = "X=" + x1;in
                    // toolStripLabelY.Text = "Y=" + y1;
                    CalculatePositionScreenInAscArray(x1, y1, false);

                    rectDrawAraay[0] = x1;// -this.Left - pictureBox1.Left - 4;
                    rectDrawAraay[1] = y1;// -this.Left - pictureBox1.Left - 92;
                    DrawRect();
                    degel++;
                    if (FlagcreateSideCutWithDistance) //For SideCut with Distance
                    {
                        toolStripSideCat.Enabled = true;
                        toolStripShowPointsOnLine.Enabled = true;
                        toolStripVisibility.Enabled = true;
                        x2 = rectDrawAraay[0] + ((Convert.ToInt16(toolStripTextBoxSideCutDistance.Text)) / Convert.ToInt16(CellSize)) * Convert.ToInt16(Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth));
                        y2 = rectDrawAraay[1];
                        //toolStripLabelX.Text = "X=" + x2;
                        //toolStripLabelY.Text = "Y=" + y2;
                        CalculatePositionScreenInAscArray(x2, y2, true);
                        rectDrawAraay[2] = x2;// -this.Left - pictureBox1.Left - 4;
                        rectDrawAraay[3] = y2;// -this.Left - pictureBox1.Left - 92;
                        // pictureBox1.Refresh();
                        DrawRect();
                        DrawLineBetweenTwoPointsOnTerrian();
                        CalculatSideCut();
                        FlagDrawRectForSideCut = false;
                        toolStripButton1_Click(sender, e);
                        FlagcreateSideCutWithDistance = false;
                    }
                }
                else if (FlagTarget == false && FlagTrafficRoute == false && FlagDrawRectForSideCut)
                {
                    pictureBox1.Image = Bitmap.FromFile(strPathImg);
                    Draw1 = new ClsGrafix(pictureBox1);
                    toolStripSideCat.Enabled = true;
                    toolStripShowPointsOnLine.Enabled = true;
                    toolStripVisibility.Enabled = true;
                    x2 = x - this.Left - pictureBox1.Left - 4;
                    y2 = y - this.Top - pictureBox1.Top - 92;
                    //toolStripLabelX.Text = "X=" + x2;
                    //toolStripLabelY.Text = "Y=" + y2;
                    CalculatePositionScreenInAscArray(x2, y2, true);
                    rectDrawAraay[2] = x2;// -this.Left - pictureBox1.Left - 4;
                    rectDrawAraay[3] = y2;// -this.Left - pictureBox1.Left - 92;
                    // pictureBox1.Refresh();
                    DrawRect();
                    DrawLineBetweenTwoPointsOnTerrian();
                    CalculatSideCut();
                    FlagDrawRectForSideCut = false;

                }
                else if (FlagTarget == true && FlagTrafficRoute == false)
                {
                    DrawTarget();
                    toolStripButtonShowElementsOnRaduis.Enabled = true;
                    FlagTarget = false;
                    //DrawTgts();
                }
                else if (FlagTrafficRoute == true)
                {
                    //  j = Convert.ToDouble(x - this.Left - pictureBox1.Left - 4) / (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth));
                    //  i = Convert.ToDouble(y - this.Top - pictureBox1.Top - 92) / (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight));
                    //pictureBox1.Image = Bitmap.FromFile(strPathImg);
                    Draw1.Rectangle(x - this.Left - pictureBox1.Left - 4, y - this.Top - pictureBox1.Top - 92, 2, 2, Color.Black, 2);
                    ArrayTrafficRoute[CounterTrafficRoute] = x - this.Left - pictureBox1.Left - 4;
                    ArrayTrafficRoute[CounterTrafficRoute + 1] = y - this.Top - pictureBox1.Top - 92;

                    CounterTrafficRoute += 2;
                    if (CounterTrafficRoute == ConstArrayTrafficRoute) //אם בצענו יותר מ-500 צעדים
                        selectTrafficRouteEndToolStripMenuItem_Click_1(sender, e);
                    if (CounterTrafficRoute > 2)
                    { 
                        Draw1.Line(ArrayTrafficRoute[CounterTrafficRoute - 4], ArrayTrafficRoute[CounterTrafficRoute - 3], ArrayTrafficRoute[CounterTrafficRoute - 2], ArrayTrafficRoute[CounterTrafficRoute - 1], Color.Blue, 2);
                        InsertNewPointBetweenLineBaseOnDistanceOfLineChecked(ArrayTrafficRoute[CounterTrafficRoute - 4], ArrayTrafficRoute[CounterTrafficRoute - 3], ArrayTrafficRoute[CounterTrafficRoute - 2], ArrayTrafficRoute[CounterTrafficRoute - 1]);
                    }
                    
                }
                else if (FlagAnalysisPointOnRoute)
                {

                    Draw1.Rectangle(xg, yg, 2, 2, Color.GreenYellow);
                    for (int index = 0; index < ClsGlobal.FireArmTable.Length / 6; index++)
                    {
                        IndexTrafficRouteFireArmTable = index;
                        if (ClsGlobal.FireArmTable[index, 5] == "True" || ClsGlobal.FireArmTable[index, 5] == "true")
                        {
                            x1 = xg;
                            y1 = yg;
                            SetTargetForTrafficRoute(Convert.ToInt16(ClsGlobal.FireArmTable[index, 1]), Convert.ToInt16(ClsGlobal.FireArmTable[index, 2]));
                        }
                    }
                }

                //if (FlagTrafficRoute == false)
                //{
                //    if (CounterTrafficRoute > 2)
                //    {
                //        for (int i = pictureBox1.Controls.Count - 1; i >= 0; i--)
                //        {
                //            Label control = pictureBox1.Controls[i] as Label;
                //            if (control == null)
                //                continue;

                //            control.Dispose();
                //        }
                //        CounterTrafficRoute = 0;
                //    }
                //}
            
        }
        private void InsertNewPointBetweenLineBaseOnDistanceOfLineChecked(int x1Point, int y1Point,int x2Point,int y2Point)
        {
            //int x1 = 30, y1 = 20, x2 = 70, y2 = 100;
            //Draw1 = new ClsGrafix(this.pictureBox1);
            ////Draw1.Rectangle(5, 5, 400,300);
            int dx = x2Point - x1Point;
            int dy = y2Point - y1Point;
            int s=10;
            s = CalculateNumberOfPointBetweenOneLine(x1Point, y1Point, x2Point, y2Point);
            if (s == 0) s = 1; ;
            float rx = dx / s;
            float ry = dy / s;
            CounterTrafficRoute = CounterTrafficRoute - 4;
            int _x2Temp= ArrayTrafficRoute[CounterTrafficRoute+2 ];
            int _y2Temp= ArrayTrafficRoute[CounterTrafficRoute +3];
            
            for (int c = 0; c < s; c++)
            {   
                int _x = Convert.ToInt16(x1Point + rx * c);
                ArrayTrafficRoute[CounterTrafficRoute ]=_x;
                int _y = Convert.ToInt16(y1Point + ry * c);
                ArrayTrafficRoute[CounterTrafficRoute +1]=_y;
                CounterTrafficRoute += 2;
                Draw1.Rectangle(_x, _y, 2, 2, Color.CadetBlue);
                //if (CounterTrafficRoute >= ConstArrayTrafficRoute) //אם בצענו יותר מ-500 צעדים
                //{
                //    CounterTrafficRoute = CounterTrafficRoute - 4;
                //    break;
                //}
            }

            ArrayTrafficRoute[CounterTrafficRoute ] = _x2Temp;
            ArrayTrafficRoute[CounterTrafficRoute+1] = _y2Temp;
            Draw1.Rectangle(_x2Temp, _y2Temp, 2, 2, Color.Red);
            CounterTrafficRoute += 2;
        }

        private int CalculateNumberOfPointBetweenOneLine(int x1Point, int y1Point, int x2Point, int y2Point)
        {
            double num = Math.Sqrt(Math.Pow(Convert.ToDouble(x1Point) - Convert.ToDouble(x2Point),2) + Math.Pow(Convert.ToDouble(y1Point) - Convert.ToDouble(y2Point),2));
            num = num / ( pictureBox1Height / TerrainHeight) ;
            num = num * CellSize;
            num = num / Convert.ToDouble(txtBoxSetDisTrafficRoute.Text);
            return Convert.ToInt16(num);
        }

        
        private void CalculatePositionScreenInAscArray(int x_pos, int y_pos, bool ClickNum)
        {
            if(ClickNum == false)
            {
                XYarrayAscPosition[0] = Convert.ToInt16(Convert.ToDouble(x_pos) / ((Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth))));
                XYarrayAscPosition[1] = Convert.ToInt16(Convert.ToDouble(y_pos) / ((Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight))));
            }
            else
            {
                XYarrayAscPosition[2] = Convert.ToInt16(Convert.ToDouble(x_pos) / ((Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth))));
                XYarrayAscPosition[3] = Convert.ToInt16(Convert.ToDouble(y_pos) / ((Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight))));
            }
        }

        private void DrawRect()
        {
            ////  Bitmap bmp = new Bitmap(pictureBox1.Image);
            // Graphics GR;
            // Pen PT;
            // PT = new Pen(Color.Black, 2);
            // GR = pictureBox1.CreateGraphics();
            ////GR = Graphics.FromImage(bmp);

            if (degel % 2 == 0)
            {
                rectDrawAraay[0] =  x1;// -this.Left - pictureBox1.Left - 4;
                rectDrawAraay[1] =  y1;//- this.Top - pictureBox1.Top - 92;
            }
            else
            {
                rectDrawAraay[2] = x2;//- this.Left - pictureBox1.Left - 4;
                rectDrawAraay[3] = y2;//- this.Top - pictureBox1.Top - 92;
            }

            //GR.DrawRectangle(PT, rectDrawAraay[0] - 10, rectDrawAraay[1] - 10, 20, 20);
            //GR.DrawRectangle(PT, rectDrawAraay[2] - 10, rectDrawAraay[3] - 10, 20, 20);
            Draw1.Rectangle(rectDrawAraay[0] - 10, rectDrawAraay[1] - 10, 20, 20, Color.Black);
            Draw1.Rectangle(rectDrawAraay[2] - 10, rectDrawAraay[3] - 10, 20, 20, Color.Black);
            //pictureBox1.Image = bmp;
            //pictureBox1.Image.Save(strFileImg, ImageFormat.Jpeg);
        }

        private void DrawLineBetweenTwoPointsOnTerrian()
        {
            // Pen selPen = new Pen(Color.Red);
            // Graphics graphic = this.pictureBox1.CreateGraphics();
            //// Bitmap bmp = new Bitmap(pictureBox1.Image);
            ////graphic = Graphics.FromImage(bmp); 
            // Point coord1 = new Point(rectDrawAraay[0], rectDrawAraay[1]);
            //  Point coord2 = new Point(rectDrawAraay[2], rectDrawAraay[3]);
            // selPen.Width = 2;
            //  graphic.DrawLine(selPen, coord1, coord2);
            Draw1.Line(rectDrawAraay[0], rectDrawAraay[1], rectDrawAraay[2], rectDrawAraay[3], Color.DeepSkyBlue, 2);
            degel = 2;
            FlagDrawLine = true;
            toolStripSideCat.Enabled = true;
            ////pictureBox1.Image = bmp;
        }

        //*******************************SideCat*******************************************************//
        private void toolStripSideCut_Click(object sender, EventArgs e)
        {
            CalculatSideCut();
            CallGrafChart();
            toolStripSideCat.Enabled = false;
            FlagMatrix = false;
        }

        private void CalculatSideCut()
        {
            double _x1, _y1, _x2, _y2; //   בעולם ההוקטור שאנו מכירים X,Y מייצג את האינדקסים במטריצת 
            double _y, _x, a, b;
            int z;
            try
            {
                _x1 = XYarrayAscPosition[0];
                _x2 = XYarrayAscPosition[2];
                //לוקטר כפי שאנחנו רגילים מהמציאות למציאת נקודות על הקו הישר y הפיכת ה-
                _y1 = TerrainHeight - XYarrayAscPosition[1] - 1;
                _y2 = TerrainHeight - XYarrayAscPosition[3] - 1;
                // toolStripLabel1.Text = "_x1=" + _x1 + " " + "_x2=" + _x2 + " " + "_y1=" + _y1 + " " + "_y2=" + _y2 + " ";



                if (_x1 == _x2 && _y1 == _y2)
                {
                    HightPointsBetweenLine = new int[4];
                    HightPointsBetweenLine[0] = Convert.ToInt16(_x1);    //for x
                    HightPointsBetweenLine[1] = Convert.ToInt16(_y1);    //for y;
                    HightPointsBetweenLine[2] = Convert.ToInt16(_x2);    //for x
                    HightPointsBetweenLine[3] = Convert.ToInt16(_y2);    //for y;
                }

                if (_x1 == _x2 && _y1 < _y2)
                {
                    HightPointsBetweenLine = new int[Convert.ToInt16((_y2 - _y1)) * 2 + 2];
                    z = 0;
                    for (double i = _y1; i <= _y2; i++)
                    {
                        //_y = a + b * i;
                        HightPointsBetweenLine[z] = Convert.ToInt16(_x1);    //for x
                        z++;
                        HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for y;
                        z++;
                    }
                }

                if (_x1 == _x2 && _y1 > _y2)
                {
                    HightPointsBetweenLine = new int[Convert.ToInt16((_y1 - _y2)) * 2 + 2];
                    z = Convert.ToInt16((_y1 - _y2)) * 2 + 1;
                    for (double i = _y2; i <= _y1; i++)
                    {
                        //_y = a + b * i;
                        HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for y;
                        z--;
                        HightPointsBetweenLine[z] = Convert.ToInt16(_x2);    //for x
                        z--;
                        ;
                    }

                }

                ///////////////////////////

                if (Convert.ToInt16((_x1 - _x2)) != 0)  //בדיקה שאין חלוקה באפס
                {
                    if (_x1 < _x2 && Math.Abs((_y1 - _y2) / (_x1 - _x2)) < 1) // b<1
                    {
                        HightPointsBetweenLine = new int[Math.Abs((XYarrayAscPosition[0] - XYarrayAscPosition[2]) * 2) + 2];
                        b = (_y1 - _y2) / (_x1 - _x2);
                        a = _y1 - b * _x1;
                        z = 0;
                        for (double i = _x1; i <= _x2; i++)
                        {
                            _y = a + b * i;
                            HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for x
                            z++;
                            HightPointsBetweenLine[z] = Convert.ToInt16(_y);    //for y;
                            z++;
                        }
                    }
                    if (_x1 > _x2 && Math.Abs((_y1 - _y2) / (_x1 - _x2)) < 1) //b<1
                    {
                        HightPointsBetweenLine = new int[Math.Abs((XYarrayAscPosition[0] - XYarrayAscPosition[2]) * 2) + 2];
                        b = (_y1 - _y2) / (_x1 - _x2);
                        a = _y1 - b * _x1;
                        z = Math.Abs(XYarrayAscPosition[0] - XYarrayAscPosition[2]) * 2 + 1;
                        for (double i = _x2; i <= _x1; i++)
                        {
                            _y = a + b * i;
                            HightPointsBetweenLine[z] = Convert.ToInt16(_y);    //for y;
                            z--;
                            HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for x
                            z--;
                        }
                    }

                    if (_y1 < _y2 && Math.Abs((_y1 - _y2) / (_x1 - _x2)) > 1)
                    {
                        HightPointsBetweenLine = new int[Convert.ToInt16((_y2 - _y1)) * 2 + 2];
                        b = (_y1 - _y2) / (_x1 - _x2);
                        a = _y1 - b * _x1;
                        z = 0;
                        for (double i = _y1; i <= _y2; i++)
                        {
                            _x = (i - a) / b;
                            HightPointsBetweenLine[z] = Convert.ToInt16(_x);    //for x
                            z++;
                            HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for y;
                            z++;
                        }
                    }


                    if (_y1 > _y2 && Math.Abs((_y1 - _y2) / (_x1 - _x2)) > 1)
                    {
                        HightPointsBetweenLine = new int[Convert.ToInt16((_y1 - _y2)) * 2 + 2];
                        b = (_y1 - _y2) / (_x1 - _x2);
                        a = _y1 - b * _x1;
                        z = Convert.ToInt16((_y1 - _y2)) * 2 + 1;
                        for (double i = _y2; i <= _y1; i++)
                        {
                            _x = (i - a) / b;
                            HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for y;
                            z--;
                            HightPointsBetweenLine[z] = Convert.ToInt16(_x);    //for x
                            z--;
                            ;
                        }
                    }

                }
                //בדיקה האם יצאנו מחוץ לגבולות המסך
                for (int i = 0; i < HightPointsBetweenLine.Length; i = i + 2)
                {
                    if (HightPointsBetweenLine[i] < 0)
                        HightPointsBetweenLine[i] = 0;
                    if (HightPointsBetweenLine[i] > TerrainWidth - 1)
                        HightPointsBetweenLine[i] = TerrainWidth - 1;

                    if (HightPointsBetweenLine[i + 1] < 0)
                        HightPointsBetweenLine[i + 1] = 0;
                    if (HightPointsBetweenLine[i + 1] > TerrainHeight - 1)
                        HightPointsBetweenLine[i + 1] = TerrainHeight - 1;

                }

                ///  האמיתיים כפי שהם מוצגים במערך האמיתי y-נשחזר רק את ה
                for (int i = 1; i <= HightPointsBetweenLine.Length; i = i + 2)
                {
                    HightPointsBetweenLine[i] = TerrainHeight - HightPointsBetweenLine[i] - 1;
                }

                ///   
                AscArraySideCat = new float[HightPointsBetweenLine.Length / 2];
                //int w = Math.Abs((XYarrayAscPosition[0] - XYarrayAscPosition[2])) ;
                int LocalX;
                int LocalY;
                int index = 0;
                for (int i = 0; index < HightPointsBetweenLine.Length / 2; i = i + 2)
                {
                    LocalX = HightPointsBetweenLine[i];
                    LocalY = HightPointsBetweenLine[i + 1];
                    if (LocalY == TerrainHeight)
                        LocalY = TerrainHeight - 1;
                    if (LocalX == TerrainWidth)
                        LocalX = TerrainWidth - 1;

                    AscArraySideCat[index] = heightData[LocalY, LocalX]; // The LOCALY is actually a matrix row heights, the LOCALX is actually a column matrix of heights
                    index++;
                }
            }
            catch { }
        }

        private void CallGrafChart()
        {


            //FrmChartGraf myGrafChart = new FrmChartGraf(AscArraySideCat);
            //myGrafChart.Show();

            FrmChartGraf myGrafChart = new FrmChartGraf(AscArraySideCat);
            myGrafChart.MdiParent = MdiParent;
            myGrafChart.Show();
            //chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            //chart1.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height);
            //chart1.Top = 25;
            //chart1.Left = 0;
            //int index = 0;
            //// chart1.PreferredSize.Width  
            //for (int i = 0; i < AscArraySideCat.Length; i++)
            //{

            //    System.Windows.Forms.DataVisualization.Charting.Series series = chart1.Series.Add(i.ToString());

            //    series.Points.Add(AscArraySideCat[i]);
            //    series.BorderColor = Color.Black;
            //    series.ToolTip = AscArraySideCat[i].ToString();
            //    index++;
            //}
            //chart1.Visible = true;

            //System.Windows.Forms.MessageBox.Show("test");
        }




        private void toolStripButtonShowMapTransparent_Click(object sender, EventArgs e)
        {

            this.Size = new System.Drawing.Size(pictureBox1.Size.Width, pictureBox1.Size.Height + 50);

            toolStripSideCat.Enabled = false;
            //toolStripButtonShowMap.Enabled = false;
            rectDrawAraay[0] = -10;
            rectDrawAraay[1] = -10;
            rectDrawAraay[2] = -10;
            rectDrawAraay[3] = -10;

        }

        private void toolStripButtonShowMatrix_Click(object sender, EventArgs e)
        {
            if (FlagMatrix == false)
            {
                FlagMatrix = true;
                //pictureBox1.Refresh();
                DrawMatrix();
                DrawRect();
                if (FlagDrawLine == true)
                    DrawLineBetweenTwoPointsOnTerrian();
            }
            else
            {
                pictureBox1.Image = Bitmap.FromFile(strPathImg);
                FlagMatrix = false;
                //pictureBox1.Refresh();
                DrawRect();
                if (FlagDrawLine == true)
                    DrawLineBetweenTwoPointsOnTerrian();
            }

        }

        private void DrawMatrix()
        {
            double SumCol = 0;
            double SumRow = 0;
            Pen selPen = new Pen(Color.Salmon);
            Graphics graphic = this.pictureBox1.CreateGraphics();
            ////Bitmap bmp = new Bitmap(pictureBox1.Image);
            ////graphic = Graphics.FromImage(bmp); 
            //selPen.Width = 1;
            //Point coord1;
            //Point coord2;
            for (int i = 0; i < TerrainWidth; i++)
            {
                SumCol = SumCol + Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth);
                //SumRow = SumRow + pictureBox1Height / TerrainHeight;
                ////coord1 = new Point(SumCol, 0);
                //// coord2 = new Point(SumCol, pictureBox1Height);
                Draw1.Line(Convert.ToInt32(SumCol), 0, Convert.ToInt32(SumCol), pictureBox1Height, Color.Red, 1);
                // graphic.DrawLine(selPen, coord1, coord2);

                //coord1 = new Point(0, SumRow);
                //coord2 = new Point(pictureBox1Width, SumRow);
                //.DrawLine(selPen, coord1, coord2);
                //Draw1.Line(0, SumRow, pictureBox1Width, SumRow, Color.Red, 1);

            }

            for (int i = 0; i < TerrainHeight; i++)
            {
                SumRow = SumRow + Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight);
                //coord1 = new Point(0, SumRow);
                //coord2 = new Point(pictureBox1Width, SumRow);
                //.DrawLine(selPen, coord1, coord2);
                Draw1.Line(0, Convert.ToInt32(SumRow), pictureBox1Width, Convert.ToInt32(SumRow), Color.Red, 1);
            }
        }

        //private void FrmMap_LocationChanged(object sender, EventArgs e)
        // {
        //if (Flag)
        //{
        //    pictureBox1.Refresh();
        //   // DrawRect();
        //    DrawMatrix();

        //}
        // }



        private void toolStripVisibility_Click(object sender, EventArgs e)
        {  
            DrawRect();
            DrawLineBetweenTwoPointsOnTerrian();
            CalculateVisibilityBetweenToPoints();
        }

        private void CalculateVisibilityBetweenToPoints_old()
        {
            bool[] VisibilityMatrix = new bool[AscArraySideCat.Length];       // visible or  not
            int[] DistanceBetweenPoints = new int[AscArraySideCat.Length];   //Distance between tow Points
            int lastVisiblePoint = 1;
            float Cal;
            VisibilityMatrix[0] = true;
            VisibilityMatrix[1] = true;
            DistanceBetweenPoints[0] = Convert.ToInt16(CellSize);
            for (int i = 1; i < AscArraySideCat.Length; i++)
            {
                DistanceBetweenPoints[i] = DistanceBetweenPoints[i - 1] + Convert.ToInt16(CellSize);
            }

            for (int i = 2; i < AscArraySideCat.Length; i++)
            {
                //if ( ((DistanceBetweenPoints[lastVisiblePoint] - DistanceBetweenPoints[0]) * (AscArraySideCat[i] - AscArraySideCat[0])) - ((DistanceBetweenPoints[i] - DistanceBetweenPoints[0]) * (AscArraySideCat[lastVisiblePoint] - AscArraySideCat[0])) > 0)
                Cal = ((DistanceBetweenPoints[lastVisiblePoint] - DistanceBetweenPoints[0]) * (AscArraySideCat[i] - AscArraySideCat[0])) - ((DistanceBetweenPoints[i] - DistanceBetweenPoints[0]) * (AscArraySideCat[lastVisiblePoint] - AscArraySideCat[0]));
                if (Cal > 0)
                {
                    VisibilityMatrix[i] = true;
                    lastVisiblePoint = i;

                }
                else
                {
                    VisibilityMatrix[i] = false;
                }
            }

            int __x;
            int __y;
            int j = 0;

            Pen PT_Trans, PT_Black;

            Color col = Color.FromArgb(90, 183, 54, 17);
            SolidBrush b = new SolidBrush(col);

            PT_Trans = new Pen(Color.FromArgb(100, Color.Red), 2);//show tranparent
            PT_Black = new Pen(Color.Black, 2);

            CalculatSideCut();

            ////Bitmap bmp = new Bitmap(pictureBox1.Image);
            Graphics GR;
            Pen PT;
            PT = new Pen(Color.Red, 2);
            GR = pictureBox1.CreateGraphics();
            ////GR = Graphics.FromImage(bmp);

            for (int h = 0; h < HightPointsBetweenLine.Length; h = h + 2)
            {
                __x = Convert.ToInt16(HightPointsBetweenLine[h] * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth)));
                __y = Convert.ToInt16(HightPointsBetweenLine[h + 1] * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight)));

                //if (VisibilityMatrix[j] == true)
                //{
                //GR.FillRectangle(b, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight);
                GR.DrawRectangle(PT, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight);
                //}
                //GR.DrawRectangle(PT_Green, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height/TerrainHeight);
                //else 
                //  GR.DrawRectangle(PT_Black, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight);

                j++;
            }
            ////pictureBox1.Image = bmp;

            //Bitmap bmp = new Bitmap(pictureBox1.Image);
            //Graphics GR;
            //Pen PT;
            //PT = new Pen(Color.Black, 2);
            //GR = pictureBox1.CreateGraphics();
            //GR = Graphics.FromImage(bmp);

            //GR.DrawRectangle(PT, rectDrawAraay[0] - 10, rectDrawAraay[1] - 10, 20, 20);
            //GR.DrawRectangle(PT, rectDrawAraay[2] - 10, rectDrawAraay[3] - 10, 20, 20);
            //pictureBox1.Image = bmp;

        }
        private void CalculateVisibilityBetweenToPoints()
        {
            bool[] VisibilityMatrix = new bool[AscArraySideCat.Length];       // visible or  not
            int[] DistanceBetweenPoints = new int[AscArraySideCat.Length];   //Distance between tow Points
            int lastVisiblePoint = 1;
            float Cal;
            VisibilityMatrix[0] = true;
            VisibilityMatrix[1] = true;
            DistanceBetweenPoints[0] = Convert.ToInt16(CellSize);
            for (int i = 1; i < AscArraySideCat.Length; i++)
            {
                DistanceBetweenPoints[i] = DistanceBetweenPoints[i - 1] + Convert.ToInt16(CellSize);
            }

            for (int i = 2; i < AscArraySideCat.Length; i++)
            {
                //if ( ((DistanceBetweenPoints[lastVisiblePoint] - DistanceBetweenPoints[0]) * (AscArraySideCat[i] - AscArraySideCat[0])) - ((DistanceBetweenPoints[i] - DistanceBetweenPoints[0]) * (AscArraySideCat[lastVisiblePoint] - AscArraySideCat[0])) > 0)
                Cal = ((DistanceBetweenPoints[lastVisiblePoint] - DistanceBetweenPoints[0]) * (AscArraySideCat[i] - AscArraySideCat[0])) - ((DistanceBetweenPoints[i] - DistanceBetweenPoints[0]) * (AscArraySideCat[lastVisiblePoint] - AscArraySideCat[0]));
                if (Cal > 0)
                {
                    VisibilityMatrix[i] = true;
                    lastVisiblePoint = i;
                }
                else
                {
                    VisibilityMatrix[i] = false;
                }
            }


            int __x;
            int __y;
            int j = 0;
            Graphics GR;
            Pen PT_Trans, PT_Black;

            Color col = Color.FromArgb(90, 183, 54, 17);
            SolidBrush b = new SolidBrush(col);


            PT_Trans = new Pen(Color.FromArgb(100, Color.Green), 2);//show tranparent
            PT_Black = new Pen(Color.Black, 2);
            GR = pictureBox1.CreateGraphics();
            CalculatSideCut();
            for (int h = 0; h < HightPointsBetweenLine.Length; h = h + 2)
            {
                __x = Convert.ToInt16(HightPointsBetweenLine[h] * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth)));
                __y = Convert.ToInt16(HightPointsBetweenLine[h + 1] * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight)));
                if (VisibilityMatrix[j] == true)
                    Draw1.FillRectangle(b, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, col);  
                j++;
            }
        }

        private void CalculateVisibilityBetweenToPointsForFireRange(int Rowindex,int R_int)
        {
            bool[] VisibilityMatrix = new bool[AscArraySideCat.Length];       // visible or  not
            int[] DistanceBetweenPoints = new int[AscArraySideCat.Length];   //Distance between tow Points
            int lastVisiblePoint = 1;
            float Cal;
            VisibilityMatrix[0] = true;
            VisibilityMatrix[1] = true;
            ArrayRangeFireVisibility[Rowindex, 0] = true;
            ArrayRangeFireVisibility[Rowindex, 1] = true;
            DistanceBetweenPoints[0] = Convert.ToInt16(CellSize);
            for (int i = 1; i < AscArraySideCat.Length; i++)
            {
               DistanceBetweenPoints[i] = DistanceBetweenPoints[i - 1] + Convert.ToInt16(CellSize);
            }

            for (int i = 2; i < AscArraySideCat.Length; i++)
            {
                //if ( ((DistanceBetweenPoints[lastVisiblePoint] - DistanceBetweenPoints[0]) * (AscArraySideCat[i] - AscArraySideCat[0])) - ((DistanceBetweenPoints[i] - DistanceBetweenPoints[0]) * (AscArraySideCat[lastVisiblePoint] - AscArraySideCat[0])) > 0)
                Cal = ((DistanceBetweenPoints[lastVisiblePoint] - DistanceBetweenPoints[0]) * (AscArraySideCat[i] - AscArraySideCat[0])) - ((DistanceBetweenPoints[i] - DistanceBetweenPoints[0]) * (AscArraySideCat[lastVisiblePoint] - AscArraySideCat[0]));
                if (Cal > 0)
                {
                    VisibilityMatrix[i] = true;
                    lastVisiblePoint = i;
                    ArrayRangeFireVisibility[Rowindex, i] = true;

                }
                else
                {
                    VisibilityMatrix[i] = false;
                    ArrayRangeFireVisibility[Rowindex, i] = false;
                }
            }
            int __x;
            int __y;
            int j = 0;
            Graphics GR;
            Pen PT_Trans, PT_Black;

            Color col = Color.FromArgb(90, 183, 54, 17);
            SolidBrush b = new SolidBrush(col);

            Color ColAttack = Color.Yellow;
            SolidBrush bAttack = new SolidBrush(ColAttack);

            Color ColAttackLine = Color.DarkRed;
            SolidBrush bAttackLine = new SolidBrush(ColAttackLine);

            PT_Trans = new Pen(Color.FromArgb(100, Color.Red), 2);//show transparent
            PT_Black = new Pen(Color.Black, 2);
            GR = pictureBox1.CreateGraphics();
            CalculatSideCut();
//////////////////////////////////////////////////////////////////////////////////////////////////
            // ציור הנראות עבור כל קו רדיוס במעגל בצבע אדום שקוף
            for (int h = 0; h < HightPointsBetweenLine.Length; h = h + 2)
            {
                __x = Convert.ToInt16(HightPointsBetweenLine[h] * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth)));
                __y = Convert.ToInt16(HightPointsBetweenLine[h + 1] * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight)));
                if (VisibilityMatrix[j] == true)
                {
                    //if (FlagShowElements || FlagTrafficRoute)
                    //{
                    //    if (IndexTrafficRouteFireArmTable > -1)
                    //    {
                    //             col = Color.FromName(ClsGlobal.FireArmTable[IndexTrafficRouteFireArmTable, 3]);
                    //             b = new SolidBrush(col);
                    //    }
                    //    Draw1.FillRectangle(b, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, col);
                    //}

                      if (FlagShowElements)
                        Draw1.FillRectangle(b, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, col);
                }
                HightPointsBetweenLineForAllFireRange[Rowindex, h] = __x;
                HightPointsBetweenLineForAllFireRange[Rowindex, h + 1] = __y;
                j++;
            }
            //ציור קווי ההתקפה בצהוב בטווח הירי כלומר בין הרדיוס הפנימי לרדיוס החיצוני
            j = R_int;
            for (int h = R_int * 2; h < HightPointsBetweenLine.Length; h = h + 2)
            {
                __x = Convert.ToInt16(HightPointsBetweenLine[h] * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth)));
                __y = Convert.ToInt16(HightPointsBetweenLine[h + 1] * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight)));
                //////////////////////גם נחל וגם רכס עפ בקשתו של צופי מה שמסומן בהערה בשורה הבאה זה מה שהיה לפני
                //if (ArrayRangeFireVisibility[Rowindex, j] == true && heightDataReches[HightPointsBetweenLine[h + 1], HightPointsBetweenLine[h]] == 1)
                if (ArrayRangeFireVisibility[Rowindex, j] == true && (heightDataReches[HightPointsBetweenLine[h + 1], HightPointsBetweenLine[h]] == 1 || heightDataReches[HightPointsBetweenLine[h + 1], HightPointsBetweenLine[h]] == 2))
                {
                    if (FlagShowElements)
                     Draw1.FillRectangle(bAttack, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, ColAttack);
                   
                   // if (IndexTrafficRouteFireArmTable > -1 && FlagTrafficRoute)
                   // {
                      //  col = Color.FromName(ClsGlobal.FireArmTable[IndexTrafficRouteFireArmTable, 3]);
                       // b = new SolidBrush(col);
                      //  Draw1.FillRectangle(b, __x, __y, pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, col);
                   // }
                    ArrayRangeFireAttack[Rowindex, j] = true;
                }
                j++;
            }
            //ציור קווי הירי על גבי הנקודות הצהובות שהם קווי ההתקפה 
            j = 0;
            for (int h = 0; j < ArrayRangeFireVisibility.Length / 360; j++)
            {
                if (ArrayRangeFireAttack[Rowindex, j])
                {
                   // if (FlagShowElements == false && CounterTrafficRoute < 2)
                      //  Draw1.FillRectangle(bAttackLine, HightPointsBetweenLineForAllFireRange[Rowindex, h], HightPointsBetweenLineForAllFireRange[Rowindex, h + 1], pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, ColAttackLine);

                    if (IndexTrafficRouteFireArmTable > -1 && FlagTrafficRoute)
                    {   ///מיקום מטרות ירי ע"ג נקודות לפגיעה בציר התנועה
                        colAttackTrafficRoute = Color.FromName(ClsGlobal.FireArmTable[IndexTrafficRouteFireArmTable, 3]);
                        b = new SolidBrush(colAttackTrafficRoute);
                        //Draw1.FillRectangle(b, HightPointsBetweenLineForAllFireRange[Rowindex, h], HightPointsBetweenLineForAllFireRange[Rowindex, h + 1], pictureBox1Width / TerrainWidth, pictureBox1Height / TerrainHeight, colAttackTrafficRoute);
                    }
                   
                }
                h = h + 2;
            }
          //  if (IndexTrafficRouteFireArmTable > -1 && FlagTrafficRoute && CounterTrafficRoute > 2)
          //          DrawTgts(); 
        }

        private void toolStripShowPointsOnLine_Click(object sender, EventArgs e)
        {
            int __x;
            int __y;
            //Graphics GR;
            //Pen PT;
            //PT = new Pen(Color.Blue, 2);
            //GR = pictureBox1.CreateGraphics();
            CalculatSideCut();
            for (int h = 0; h < HightPointsBetweenLine.Length; h = h + 2)
            {
                __x = Convert.ToInt16(HightPointsBetweenLine[h] * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth)));
                __y = Convert.ToInt16(HightPointsBetweenLine[h + 1] * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight)));
                // GR.DrawRectangle(PT, __x, __y, 1, 1);
                Draw1.Rectangle(__x, __y, 1, 1, Color.Blue);
                //GR.DrawRectangle(PT, __y - 10, rectDrawAraay[3] - 10, 20, 20);
            }
        }

        private void toolStripButtonAddTargetandSource_Click(object sender, EventArgs e)
        {
            AddHightToTarget_And_Source();
        }

        private void AddHightToTarget_And_Source()
        {
            AscArraySideCat[0] = AscArraySideCat[0] + Convert.ToInt16(toolStripTextBoxTargetObserv.Text);
            AscArraySideCat[AscArraySideCat.Length - 1] = AscArraySideCat[AscArraySideCat.Length - 1] + Convert.ToInt16(toolStripTextBoxSourceObserv.Text);
        }

        private void AddHightToSource(int SourceHieght)
        {
           // AscArraySideCat[0] = AscArraySideCat[0] + TargetHieght;
            //if i=1 then we dont add SourceHieght to Target
            //if i=0 then we will add SourceHieght to Target
            for (int i = 0; i < AscArraySideCat.Length ; i++)
            {
                AscArraySideCat[i] = AscArraySideCat[i] + SourceHieght;   
            }
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            int x2_temp, y2_temp;
            Double R;
            double X_Circle, Y_Circle;
            x2_temp = rectDrawAraay[2];
            y2_temp = rectDrawAraay[3];
            R = Math.Sqrt(Math.Pow(Convert.ToDouble(Math.Abs(x1 - x2)), 2.0) + Math.Pow(Convert.ToDouble(Math.Abs(y1 - y2)), 2.0));
            for (double i = 0; i < 360; i++)
            {
                //double cosi,yyy;
                //cosi = Math.Cos(Math.PI * i / 180.0);
               // yyy = Math.Sin(Math.PI * i / 180.0);
                X_Circle = Math.Cos(Math.PI * i / 180.0) * R + x1;  //convert from Radian to Degree
                Y_Circle = Math.Sin(Math.PI * i / 180.0) * R + y1;
                // PT = new Pen(Color.Blue,2);
               // Draw1.Line(x1, y1, Convert.ToInt16(X_Circle), Convert.ToInt16(Y_Circle),Color.Red,1);
               //GR.DrawRectangle(PT, Convert.ToInt16(X_Circle), Convert.ToInt16(Y_Circle), 1, 1);
                Draw1.Rectangle(Convert.ToInt16(X_Circle), Convert.ToInt16(Y_Circle), 1, 1, Color.Blue, 1);
                rectDrawAraay[2] = Convert.ToInt16(X_Circle);
                rectDrawAraay[3] = Convert.ToInt16(Y_Circle);
                CalculatePositionScreenInAscArray(rectDrawAraay[2], rectDrawAraay[3], true);
                //}
                CalculatSideCut();
                AddHightToTarget_And_Source();
                CalculateVisibilityBetweenToPoints();
            }

            //שחזור שתי הנקודות הראשונות שנבחרו על מנת להציג חתך צד גם לאחר הצדת הרדיוס
            rectDrawAraay[2] = x2_temp;
            rectDrawAraay[3] = y2_temp;
            CalculatePositionScreenInAscArray(rectDrawAraay[2], rectDrawAraay[3], true);
            CalculatSideCut();
            AddHightToTarget_And_Source();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

            if ((TerrainHeight>300) || (TerrainWidth>300))
            {
                MessageBox.Show("Canwt load 3D because Terrain Width or Heifgt is bigger then 300 rows/cols.");
                return;
            }

            if (TerrainHeight!=TerrainWidth)
            {
                MessageBox.Show("Can't load 3D because Terrain Width is different then Terrain Height.");
                return;
            }
            
                System.IO.File.Copy(strPathAsc, strFileAsc, true);
                System.IO.File.Delete(strFileImg);
                Draw1.SavePic(strFileImg);

                string _Exe = "XNATerrainTexturing.exe";
                System.Diagnostics.Process.Start(_Exe);
            
        }

        
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            Draw1 = new ClsGrafix(pictureBox1);
            _GenerateReches();
            _GenerateNahal();
            DrawKipa_Reches_Nahal();
        }



        private void _GenerateReches()
        {

            int[] heightDataRechesPointIndex = new int[2];
            heightDataReches = new int[TerrainHeight, TerrainWidth];
            float MinData, MaxData;
            //Initilize the heightDataReches
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                for (int j = 1; j < TerrainWidth - 1; j++)
                {
                    heightDataReches[i, j] = 0;
                }
            }
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            //search 2 point lower than the point we focus now

            //search Kipa and update the heightDataReches with true - in the upSide when i=0 only
            //for (int i = 0; i < TerrainHeight; i++)
            //{
            //    for (int j = 0; j < TerrainWidth - 1; j++)
            //    {
            //        x_Reches = j * (pictureBox1Width / TerrainWidth);
            //        y_Reches = i * (pictureBox1Height / TerrainHeight);
            //        if (heightData[i, j] > heightData[i, j - 1] && heightData[i, j] > heightData[i, j + 1] && heightData[i, j] > heightData[i + 1, j - 1] && heightData[i, j] > heightData[i + 1, j + 1] && heightData[i, j] > heightData[i + 1, j ])
            //        {
            //            heightDataReches[i, j] = true;
            //            GR.DrawRectangle(selPenKipa, x_Reches, y_Reches, 2, 2);

            //        }
            //    }
            //}


            //1.search Kipa and update the heightDataReches with value 2 - in the up-Side
            for (int j = 1; j < TerrainWidth - 1; j++)
            {
                if (heightData[0, j] > heightData[0, j - 1] && heightData[0, j] > heightData[0, j + 1] && heightData[0, j] > heightData[1, j] && heightData[0, j] > heightData[1, j - 1] && heightData[0, j] > heightData[1, j + 1])
                {
                    heightDataReches[0, j] = 1;
                }
            }


            //2.search Kipa and update the heightDataReches with value 2 - in the Bottom
            for (int j = 1; j < TerrainWidth - 1; j++)
            {
                if (heightData[TerrainHeight - 1, j] > heightData[TerrainHeight - 1, j - 1] && heightData[TerrainHeight - 1, j] > heightData[TerrainHeight - 1, j + 1] && heightData[TerrainHeight - 1, j] > heightData[TerrainHeight - 2, j] && heightData[TerrainHeight - 1, j] > heightData[TerrainHeight - 2, j - 1] && heightData[TerrainHeight - 1, j] > heightData[TerrainHeight - 2, j + 1])
                {
                    heightDataReches[TerrainHeight - 1, j] = 1;
                }
            }

            //3.search Kipa and update the heightDataReches with value 2 - in the Left
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                if ((heightData[i, 0] > heightData[i - 1, 0] && heightData[i, 0] > heightData[i + 1, 0]) || heightData[i, 0] > heightData[i + 1, 1] || heightData[i, 0] > heightData[i - 1, 1] || heightData[i, 0] > heightData[i, 1])
                {
                    heightDataReches[i, 0] = 1;
                }
            }

            //4.search Kipa and update the heightDataReches with value 2 - in the Right
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                if (heightData[i, TerrainWidth - 1] > heightData[i - 1, TerrainWidth - 1] && heightData[i, TerrainWidth - 1] > heightData[i + 1, TerrainWidth - 1] && heightData[i, TerrainWidth - 1] > heightData[i, TerrainWidth - 2] && heightData[i, TerrainWidth - 1] > heightData[i - 1, TerrainWidth - 2] && heightData[i, TerrainWidth - 1] > heightData[i + 1, TerrainWidth - 2])
                {
                    heightDataReches[i, 0] = 1;
                }
            }

            //check Parasht-Myim in the center of Map
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                for (int j = 1; j < TerrainWidth - 1; j++)
                {

                    if ((heightData[i, j] > heightData[i, j - 1] && heightData[i, j] > heightData[i, j + 1]))
                        heightDataReches[i, j] = 1;
                    if (heightData[i, j] > heightData[i - 1, j] && heightData[i, j] > heightData[i + 1, j])
                        heightDataReches[i, j] = 1;
                    if (heightData[i, j] > heightData[i - 1, j + 1] && heightData[i, j] > heightData[i + 1, j - 1])
                        heightDataReches[i, j] = 1;
                    if (heightData[i, j] > heightData[i - 1, j - 1] && heightData[i, j] > heightData[i + 1, j + 1])
                        heightDataReches[i, j] = 1;
                }
            }

            MinData = FindMin();
            MaxData = FindMax();
            if (toolStripTextBoxReches.Text == "") toolStripTextBoxReches.Text = "0";
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                for (int j = 1; j < TerrainWidth - 1; j++)
                {
                    if (heightDataReches[i, j] == 1 && heightData[i, j] > (MinData + MaxData) / 2 + Convert.ToDouble(toolStripTextBoxReches.Text))
                        heightDataReches[i, j] = 1;
                    else if (heightDataReches[i, j] == 1 && heightData[i, j] < (MinData + MaxData) / 2 + Convert.ToDouble(toolStripTextBoxReches.Text))
                        heightDataReches[i, j] = 0;
                }
            }
        }

        private void _GenerateNahal()
        {
            //check Nahal in the center of Map
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                for (int j = 1; j < TerrainWidth - 1; j++)
                {

                    if ((heightData[i, j] < heightData[i, j - 1] && heightData[i, j] < heightData[i, j + 1]))
                        heightDataReches[i, j] = 2;
                    if (heightData[i, j] < heightData[i - 1, j] && heightData[i, j] < heightData[i + 1, j])
                        heightDataReches[i, j] = 2;
                    if (heightData[i, j] < heightData[i - 1, j + 1] && heightData[i, j] < heightData[i + 1, j - 1])
                        heightDataReches[i, j] = 2;
                    if (heightData[i, j] < heightData[i - 1, j - 1] && heightData[i, j] < heightData[i + 1, j + 1])
                        heightDataReches[i, j] = 2;
                }
            }

            float MinData = FindMin();
            float MaxData = FindMax();
            if (toolStripTextBoxReches.Text == "") toolStripTextBoxReches.Text = "0";
            for (int i = 1; i < TerrainHeight - 1; i++)
            {
                for (int j = 1; j < TerrainWidth - 1; j++)
                {
                    if (heightDataReches[i, j] == 2 && heightData[i, j] < (MinData + MaxData) / 2 + Convert.ToDouble(toolStripTextBoxReches.Text))
                        heightDataReches[i, j] = 2;
                    else if (heightDataReches[i, j] == 2 && heightData[i, j] > (MinData + MaxData) / 2 + Convert.ToDouble(toolStripTextBoxReches.Text))
                        heightDataReches[i, j] = 0;
                }
            }
        }

        private float FindMax()
        {
            float Maxdata;
            Maxdata = heightData[0, 0];
            for (int i = 0; i < TerrainHeight - 1; i++)
            {
                for (int j = 0; j < TerrainWidth - 1; j++)
                {

                    if (heightData[i, j] > Maxdata)
                        Maxdata = heightData[i, j];
                }
            }
            return Maxdata;
        }

        private float FindMin()
        {
            float Mindata;
            Mindata = heightData[0, 0];
            for (int i = 0; i < TerrainHeight - 1; i++)
            {
                for (int j = 0; j < TerrainWidth - 1; j++)
                {
                    if (heightData[i, j] > 0)
                    {
                        if (heightData[i, j] < Mindata)
                            Mindata = heightData[i, j];
                    }
                }
            }

            return Mindata;
        }


        private void DrawKipa_Reches_Nahal()
        {
            SolidBrush Breches = new SolidBrush(Color.Brown);
            SolidBrush BKipa = new SolidBrush(Color.BlueViolet);
            double x_Reches = 0, y_Reches = 0;
            for (int i = 0; i < TerrainHeight - 1; i++)
            {
                for (int j = 0; j < TerrainWidth - 1; j++)
                {
                    x_Reches = j * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth));
                    y_Reches = i * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight));

                    if (heightDataReches[i, j] == 2) //Nahal
                        Draw1.FillRectangle(BKipa, Convert.ToInt32(x_Reches), Convert.ToInt32(y_Reches), pictureBox1Width / TerrainWidth, pictureBox1Width / TerrainHeight, Color.BlueViolet);
                    //   Draw1.FillRectangle(BKipa, Convert.ToInt32(x_Reches), Convert.ToInt32(y_Reches), 3,3, Color.Blue);
                    else if (heightDataReches[i, j] == 1) //Reches
                        Draw1.FillRectangle(Breches, Convert.ToInt32(x_Reches), Convert.ToInt32(y_Reches), pictureBox1Width / TerrainWidth, pictureBox1Width / TerrainHeight, Color.Brown);
                    //  Draw1.FillRectangle(Breches, Convert.ToInt32(x_Reches), Convert.ToInt32(y_Reches), 3, 3, Color.Red);

                }
            }
        }
        private void _DrawKipa_Reches()
        {

            SolidBrush BKipa = new SolidBrush(Color.Blue);
            double x_Reches = 0, y_Reches = 0;
            for (int i = 0; i < TerrainHeight - 1; i++)
            {
                for (int j = 0; j < TerrainWidth - 1; j++)
                {
                    x_Reches = j * (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth));
                    y_Reches = i * (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight));
                    if (heightDataReches[i, j] == 2)
                        Draw1.FillRectangle(BKipa, Convert.ToInt32(x_Reches), Convert.ToInt32(y_Reches), 3, 3, Color.Blue);

                }
            }
        }

        public int[] checkBigFrom2Point(int i1, int j1, int i2, int j2)
        {
            int[] heightDataRechesPointIndex = new int[2];
            if (heightData[i1, j1] >= heightData[i2, j2])
            {
                heightDataRechesPointIndex[0] = i1;
                heightDataRechesPointIndex[1] = j1;
                return heightDataRechesPointIndex;
            }
            else
            {
                heightDataRechesPointIndex[0] = i2;
                heightDataRechesPointIndex[1] = j2;
                return heightDataRechesPointIndex;
            }
        }

        public int[] checkBigFrom3Point(int i1, int j1, int i2, int j2, int i3, int j3)
        {
            int[] heightDataRechesPointIndex = new int[2];
            //if (i1 == -1) i1 = 0;
            //if (i2 == -1) i2 = 0;
            //if (i3 == -1) i3 = 0;
            //if (j1 == -1) j1 = 0;
            //if (j2 == -1) j2 = 0;
            //if (j3 == -1) j3 = 0;

            //if (i1 >= TerrainHeight) i1 = TerrainHeight - 1;
            //if (i2 >= TerrainHeight) i2 = TerrainHeight - 1;
            //if (i3 >= TerrainHeight) i3 = TerrainHeight - 1;
            //if (j1 >= TerrainHeight) j1 = TerrainWidth - 1;
            //if (j2 >= TerrainHeight) j2 = TerrainWidth - 1;
            //if (j3 >= TerrainHeight) j3 = TerrainWidth - 1;


            if (heightDataReches[i1, j1] == 1)
            {
                heightDataRechesPointIndex[0] = i1;
                heightDataRechesPointIndex[1] = j1;
                return heightDataRechesPointIndex;
            }
            else if (heightDataReches[i2, j2] == 1)
            {
                heightDataRechesPointIndex[0] = i2;
                heightDataRechesPointIndex[1] = j2;
                return heightDataRechesPointIndex;
            }
            else if (heightDataReches[i3, j3] == 1)
            {
                heightDataRechesPointIndex[0] = i3;
                heightDataRechesPointIndex[1] = j3;
                return heightDataRechesPointIndex;
            }


            if (heightData[i1, j1] >= heightData[i2, j2] && heightData[i1, j1] >= heightData[i3, j3])
            {
                heightDataRechesPointIndex[0] = i1;
                heightDataRechesPointIndex[1] = j1;
                return heightDataRechesPointIndex;
            }
            else if (heightData[i2, j2] >= heightData[i1, j1] && heightData[i2, j2] >= heightData[i3, j3])
            {
                heightDataRechesPointIndex[0] = i2;
                heightDataRechesPointIndex[1] = j2;
                return heightDataRechesPointIndex;
            }
            else
            {
                heightDataRechesPointIndex[0] = i3;
                heightDataRechesPointIndex[1] = j3;
                return heightDataRechesPointIndex;

            }

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                x = Cursor.Position.X; ;
                y = Cursor.Position.Y; ;
                double i, j;
                j = Convert.ToDouble(x - this.Left - pictureBox1.Left - 4) / (Convert.ToDouble(pictureBox1Width) / Convert.ToDouble(TerrainWidth));
                i = Convert.ToDouble(y - this.Top - pictureBox1.Top - 92) / (Convert.ToDouble(pictureBox1Height) / Convert.ToDouble(TerrainHeight));
                //TxtCord.Text = "  H:" + heightData[Convert.ToInt16(i - 1), Convert.ToInt16(j - 1)].ToString() + " X:" + j.ToString("0.00") + " Y:" + i.ToString("0.00");
                toolStripStatusLabelCord.Text = "  H:" + heightData[Convert.ToInt16(i - 1), Convert.ToInt16(j - 1)].ToString() + " X:" + j.ToString("0.00") + " Y:" + i.ToString("0.00");
                toolStripStatusLabelllcorner.Text = "UTM(NAD83)-(" + (xllcorner + CellSize * j).ToString("0.000") + "," + (yllcorner + CellSize * i).ToString("0.000") + ")";
                Graphics G = pictureBox1.CreateGraphics();
            }
            catch { }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            pictureBox2.Visible = !pictureBox2.Visible;
        }

        private void exportMountainToJpgToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _GenerateReches();
            _GenerateNahal();
            SolidBrush Breches = new SolidBrush(Color.White);
            Draw1.FillRectangle(Breches, 1, 1, pictureBox1Width, pictureBox1Height);
            DrawKipa_Reches_Nahal();
            Draw1.SavePic("HeightMap.png", ImageFormat.Png);
        }

        private void manageFirearmsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            FrmFirearms FFirearms = new FrmFirearms();
            FFirearms.MdiParent = MdiParent;
            FFirearms.Show();
        }

        private void demoOfGettingFirearmsDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripShowPointsOnLine.Enabled = false;
            toolStripVisibility.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute1.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute2.Enabled = false;
            FrmSelFirearm FSelFirearms = new FrmSelFirearm();
            FSelFirearms.MdiParent = MdiParent;
            FSelFirearms.Show();
            FlagTarget = true;
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            Draw1 = new ClsGrafix(pictureBox1);
            //Ds.GetMaxRange(ClsGlobal.SelFireArm);
            // Ds.GetMinRange(ClsGlobal.SelFireArm);
            //Ds.GetMaxRange(ClsGlobal.SelFireArm);
        }

        private void DrawTarget() //Draw Zlav
        {
            Double R_Int,R_Ext, Cell,RangeFire;
           
            Cell = pictureBox1Width / TerrainWidth;

            R_Int = Convert.ToDouble(Ds.GetMinRange(ClsGlobal.SelFireArm)) / CellSize;
            R_Int = Cell * R_Int;  //In Pixels

            R_Ext = Convert.ToDouble(Ds.GetMaxRange(ClsGlobal.SelFireArm)) / CellSize;
            R_Ext = Cell * R_Ext;  //In Pixels

            FlagTarget = false;
            if (FlagShowElements == false)
            {
                x1 = x - this.Left - pictureBox1.Left - 4;
                y1 = y - this.Top - pictureBox1.Top - 92;
            }

            RangeFire = R_Ext - R_Int;
            ArrayRangeFireTarget[0] = Convert.ToInt32(x1 + R_Int) / (Convert.ToInt32(pictureBox1Width) / Convert.ToInt32(TerrainWidth));
            ArrayRangeFireTarget[1] = Convert.ToInt32(y1 ) / (Convert.ToInt32(pictureBox1Height) / Convert.ToInt32(TerrainHeight));

            ArrayRangeFireTarget[2] = Convert.ToInt32(x1 + R_Ext) / (Convert.ToInt32(pictureBox1Width) / Convert.ToInt32(TerrainWidth));
            ArrayRangeFireTarget[3] = Convert.ToInt32(y1) / (Convert.ToInt32(pictureBox1Height) / Convert.ToInt32(TerrainHeight));


            //Draw1.Rectangle(ArrayRangeFireTarget[0], ArrayRangeFireTarget[1], 3, 3,Color.Red);
            //Draw1.Rectangle(ArrayRangeFireTarget[2], ArrayRangeFireTarget[3], 3, 3, Color.Red); 

            Draw1.Line(x1 - 15, y1, x1 - 5, y1, Color.Black, 3);
            Draw1.Line(x1 + 5, y1, x1 + 15, y1, Color.Black, 3);
            Draw1.Line(x1, y1 - 5, x1, y1 - 15, Color.Black, 3);
            Draw1.Line(x1, y1 + 5, x1, y1 + 15, Color.Black, 3); 

             DrawInternalRaduisTarget();
             DrawExternalRaduisTarget();
             CalculateVisibilityRangeFire(R_Int,R_Ext);
        }

        private void DrawExternalRaduisTarget()
        {
            Double R, Cell;
            double X_Circle, Y_Circle;
            bool FlagDrawPointOnCircute = true;
           // R = Math.Sqrt(Math.Pow(Convert.ToDouble(Math.Abs(x1 - x2)), 2.0) + Math.Pow(Convert.ToDouble(Math.Abs(y1 - y2)), 2.0));
           // R = Convert.ToDouble(Ds.GetMaxRange(ClsGlobal.SelFireArm)) / CellSize;
            Cell = pictureBox1Width / TerrainWidth;
            R = Convert.ToDouble(Ds.GetMaxRange(ClsGlobal.SelFireArm)) / CellSize;
            R = Cell * R;
            for (double i = 0; i < 360; i++)
            {
                FlagDrawPointOnCircute = true;
                X_Circle = Math.Cos(Math.PI * i / 180.0) * R + x1;
                Y_Circle = Math.Sin(Math.PI * i / 180.0) * R + y1;
                if(FlagDrawPointOnCircute)
                 Draw1.Rectangle(Convert.ToInt16(X_Circle), Convert.ToInt16(Y_Circle), 1, 1, Color.Black, 1);
                if(FlagDrawPointOnCircute) FlagDrawPointOnCircute=false;
                else FlagDrawPointOnCircute = true;
                
            }
        }

        private void DrawInternalRaduisTarget()
        {
            Double R, Cell;
            double X_Circle, Y_Circle;
            bool FlagDrawPointOnCircute = true;
            Cell = pictureBox1Width / TerrainWidth;
            R = Convert.ToDouble(Ds.GetMinRange(ClsGlobal.SelFireArm)) / CellSize;
            R = Cell * R;
            for (double i = 0; i < 360; i++)
            {
                X_Circle = Math.Cos(Math.PI * i / 180.0) * R + x1;
                Y_Circle = Math.Sin(Math.PI * i / 180.0) * R + y1;
                if (FlagDrawPointOnCircute)
                    Draw1.Rectangle(Convert.ToInt16(X_Circle), Convert.ToInt16(Y_Circle), 1, 1, Color.Black, 1);
                if (FlagDrawPointOnCircute) FlagDrawPointOnCircute = false;
                else FlagDrawPointOnCircute = true;      
            }
        }

        private void CalculateVisibilityRangeFire(Double R_Int,Double  R_Ext)
        {
            Double Cell;
            ArrayRangeFireVisibility = new bool[360, Convert.ToInt32(R_Ext) / (pictureBox1Width / TerrainWidth) + 1];
            ArrayRangeFireAttack = new bool[360, Convert.ToInt32(R_Ext) / (pictureBox1Width / TerrainWidth) + 1];
            HightPointsBetweenLineForAllFireRange = new int[360, (Convert.ToInt32(R_Ext) / (pictureBox1Width / TerrainWidth) + 1) * 2];
            Double  X_Circle,Y_Circle;

            int SourceHieght;
            Cell = pictureBox1Width / TerrainWidth;
            CalculatePositionScreenInAscArray(x1, y1, false);
            if (IndexTrafficRouteFireArmTable > -1)
                SourceHieght = Convert.ToInt16(ClsGlobal.FireArmTable[IndexTrafficRouteFireArmTable, 4]); //Height from DB
            else
               SourceHieght = Convert.ToInt16(Ds.GetHeight(ClsGlobal.SelFireArm));
              _GenerateReches();
              _GenerateNahal();
            for (double i = 0; i < 360; i++)
            {
                X_Circle = Math.Cos(Math.PI * i / 180.0) * R_Ext + x1;  //convert from Radian to Degree
                Y_Circle = Math.Sin(Math.PI * i / 180.0) * R_Ext + y1;
                CalculatePositionScreenInAscArray(Convert.ToInt16(X_Circle), Convert.ToInt16(Y_Circle), true);
                CalculatSideCut();
                AddHightToSource(SourceHieght);
                AddHightToTarget_And_Source();
                CalculateVisibilityBetweenToPointsForFireRange(Convert.ToInt16(i), Convert.ToInt16(R_Int/Cell));     
                       
            }
            if (CounterTrafficRoute < 2)
            {
                DrawInternalRaduisTarget();
                Draw1.Line(x1 - 15, y1, x1 - 5, y1, Color.Black, 3);
                Draw1.Line(x1 + 5, y1, x1 + 15, y1, Color.Black, 3);
                Draw1.Line(x1, y1 - 5, x1, y1 - 15, Color.Black, 3);
                Draw1.Line(x1, y1 + 5, x1, y1 + 15, Color.Black, 3);
            //For Gabi to Draw Line-Attack
             //   callTgtPos(CalaTrueInArrayRangeFireAttack(Convert.ToInt16(R_Ext / (pictureBox1Width / TerrainWidth))), Convert.ToInt16(R_Ext / (pictureBox1Width / TerrainWidth)));
            }
            callTgtPos(CalaTrueInArrayRangeFireAttack(Convert.ToInt16(R_Ext / (pictureBox1Width / TerrainWidth))), Convert.ToInt16(R_Ext / (pictureBox1Width / TerrainWidth)));
            DrawTgts();
        }

        private int CalaTrueInArrayRangeFireAttack(int R_Ext)
        {
            int counter=0;
            for (int i = 0; i <360 ; i++)
            {
                for (int j= 0; j < R_Ext; j++)
                {
                    if (ArrayRangeFireAttack[i,j])
                        counter += 1;
                }  
            }
            return counter;
        }

        private void callTgtPos(int RownNum, int R_Ext)
        {
            TgtPos = new int[RownNum,3];
            int Row_index = 0;
            for (int i = 0; i < 360; i++)
            {
                for (int j = 0; j <R_Ext; j++)
                {
                    if (ArrayRangeFireAttack[i, j])
                    {
                        TgtPos[Row_index, 0] = HightPointsBetweenLineForAllFireRange[i, j * 2];
                        TgtPos[Row_index, 1] = HightPointsBetweenLineForAllFireRange[i, j * 2 + 1];
                        Row_index += 1;
                    }
                   
                }
            }
        }

        private void toolStripComboZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            //switch (toolStripComboZoom.SelectedItem.ToString())
            //{
            //    case "x1":
            //        pictureBox1Height = TerrainHeight * 1;
            //        pictureBox1Width = TerrainWidth * 1;
            //        break;
            //    case "x2":
            //        pictureBox1Height = TerrainHeight * 2;
            //        pictureBox1Width = TerrainWidth * 2;
            //        break;
            //    case "x4":
            //        pictureBox1Height = TerrainHeight * 4;
            //        pictureBox1Width = TerrainWidth * 4;
            //        break;
            //    case "x6":
            //        pictureBox1Height = TerrainHeight * 6;
            //        pictureBox1Width = TerrainWidth * 6;
            //        break;
            //    case "x8":
            //        pictureBox1Height = TerrainHeight * 8;
            //        pictureBox1Width = TerrainWidth * 8;
            //        break;
            //    case "x10":
            //        pictureBox1Height = TerrainHeight * 10;
            //        pictureBox1Width = TerrainWidth * 10;
            //        break;
            //    case "x20":
            //        pictureBox1Height = TerrainHeight * 20;
            //        pictureBox1Width = TerrainWidth * 20;
            //        break;
            //    default:
            //        pictureBox1Height = TerrainHeight * 1;
            //        pictureBox1Width = TerrainWidth * 1;
            //        break;
            //}
            //pictureBox1.Height = pictureBox1Height;
            //pictureBox1.Width = pictureBox1Width;
            //this.Width = pictureBox1Width;
            //this.Height = pictureBox1Height + 50 ; 
        }

        private void toolStripButtonShowElementsOnRaduis_Click(object sender, EventArgs e)
        {
            FlagShowElements = true;
            DrawTarget();
            FlagShowElements = false;
        }



        private void DrawTgtLogic(int bX,int bY)
        {
            int dx =Math.Abs( bX - xg);
            int dy = Math.Abs(bY - yg);
            double C = Math.Sqrt(dx *dx + dy *dy);
            //double c = Math.Atan (45*Math.PI/180);
            double A = Math.Asin(dy/C)*180/Math.PI;
            System.Diagnostics.Debug.Print (A.ToString());
           

            if ((bX>xg) && (bY<yg))
            {
                if ((A >= 0) && (A <= 22.49))
                {
                    DrawTgt(bX, bY, 3);
                }
                if ((A >= 22.5) && (A <= 67.49))
                {
                    DrawTgt(bX, bY, 2);
                }
                if ((A >= 67.5) && (A <= 90)) 
                {
                    DrawTgt(bX, bY, 12);
                }
            }
            if ((bX > xg) && (bY> yg))
            {
                if ((A >= 0) && (A <= 22.49))
                {
                    DrawTgt(bX, bY, 3);
                }
                if ((A >= 22.5) && (A <= 67.49))
                {
                    DrawTgt(bX, bY, 4);
                }
                if ((A >= 67.5) && (A <= 90))
                {
                    DrawTgt(bX, bY, 6);
                }
            }

            if ((bX < xg) && (bY < yg))
            {
                if ((A >= 0) && (A <= 22.49))
                {
                    DrawTgt(bX, bY, 9);
                }
                if ((A >= 22.5) && (A <= 67.49))
                {
                    DrawTgt(bX, bY, 10);
                }
                if ((A >= 67.5) && (A <= 90))
                {
                    DrawTgt(bX, bY, 12);
                }
            }

            if ((bX < xg) && (bY > yg))
            {
                if ((A >= 0) && (A <= 22.49))
                {
                    DrawTgt(bX, bY, 9);
                }
                if ((A >= 22.5) && (A <= 67.49))
                {
                    DrawTgt(bX, bY, 7);
                }
                if ((A >= 67.5) && (A <= 90))
                {
                    DrawTgt(bX, bY, 6);
                }
            }
 
        }

        private void DrawTgt(int bX,int bY,int Clock)
        {
            if (FlagAnalysisPointOnRoute)
                colAttackTrafficRoute = Color.Blue;
            if (Clock == 12)
            {
                Draw1.Line(bX - 13, bY - 4, bX - 10, bY + 3, colAttackTrafficRoute, 3);
                Draw1.Line(bX + 13, bY - 4, bX + 10, bY + 3, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 10, bY + 3, bX + 10, bY + 3, colAttackTrafficRoute, 3);
            }

            if (Clock == 6)
            {
                Draw1.Line(bX - 12, bY + 3, bX - 10, bY - 3, colAttackTrafficRoute, 3);
                Draw1.Line(bX + 12, bY + 3, bX + 10, bY - 3, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 10, bY - 3, bX + 10, bY - 3, colAttackTrafficRoute, 3);
            }

            if (Clock == 3)
            {
                Draw1.Line(bX + 3, bY - 12, bX - 3, bY - 10, colAttackTrafficRoute, 3);
                Draw1.Line(bX + 3, bY + 12, bX - 3, bY + 10, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 3, bY - 10, bX - 3, bY + 10, colAttackTrafficRoute, 3);
            }

            if (Clock == 9)
            {
                Draw1.Line(bX - 3, bY - 12, bX + 3, bY - 10, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 3, bY + 12, bX + 3, bY + 10, colAttackTrafficRoute, 3);
                Draw1.Line(bX + 3, bY - 10, bX + 3, bY + 10, colAttackTrafficRoute, 3);
            }

            if ((Clock == 1) || (Clock == 2))
            {
                Draw1.Line(bX + 5, bY + 12, bX + 13, bY + 11, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY - 4, bX + 2, bY - 10, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY - 4, bX + 5, bY + 12, colAttackTrafficRoute, 3);
            }

            if ((Clock == 10) || (Clock == 11))
            {
                Draw1.Line(bX + 4, bY - 4, bX - 1, bY - 11, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY + 12, bX - 12, bY + 12, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY + 12, bX + 5, bY - 4, colAttackTrafficRoute, 3);
            }

            if ((Clock == 7) || (Clock == 8))
            {
                Draw1.Line(bX + 5, bY + 12, bX - 1, bY + 21, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY - 4, bX - 14, bY - 5, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY - 4, bX + 5, bY + 12, colAttackTrafficRoute, 3);
            }

            if ((Clock == 4) || (Clock == 5))
            {
                Draw1.Line(bX + 5, bY - 4, bX + 15, bY - 3, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY + 12, bX + 3, bY + 18, colAttackTrafficRoute, 3);
                Draw1.Line(bX - 4, bY + 12, bX + 5, bY - 4, colAttackTrafficRoute, 3);

            }

        }

        private void DrawTgts()
        {
            Color ColAttackLine = Color.Green;
            SolidBrush bAttackLine = new SolidBrush(ColAttackLine);
            for (int C = 0; C < TgtPos.Length / 3; C += Convert.ToInt16(SetAttackLocation1.Text))
            {
                //Draw1.FillRectangle(bAttackLine, TgtPos[C, 0]-5, TgtPos[C, 1]-5, 10, 10, ColAttackLine);
                DrawTgtLogic(TgtPos[C, 0], TgtPos[C, 1]);
            }
        }

       

        private void selectMultiplyFirearmsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripShowPointsOnLine.Enabled = false;
            toolStripVisibility.Enabled = false;
            FlagAnalysisPointOnRoute = false;
            FrmMFirearm FMFirearm = new FrmMFirearm();
            FMFirearm.MdiParent = MdiParent;
            FMFirearm.Show();
        }
    
        private void SetTargetForTrafficRoute(Double MinRange, Double MaxRange) //Draw Zlav
        {
            Double R_Int, R_Ext, Cell, RangeFire;

            Cell = pictureBox1Width / TerrainWidth;

            R_Int = MinRange / CellSize;
           // R_Int = Convert.ToDouble(MinRange) / CellSize;
            R_Int = Cell * R_Int;  //In Pixels

            R_Ext = MaxRange / CellSize;
            //R_Ext = Convert.ToDouble(MaxRange) / CellSize;
            R_Ext = Cell * R_Ext;  //In Pixels

           // FlagTarget = false;
           // if (FlagShowElements == false)
           // {
           //     x1 = x - this.Left - pictureBox1.Left - 4;
               // y1 = y - this.Top - pictureBox1.Top - 92;
           // }

            RangeFire = R_Ext - R_Int;
            ArrayRangeFireTarget[0] = Convert.ToInt32(x1 + R_Int) / (Convert.ToInt32(pictureBox1Width) / Convert.ToInt32(TerrainWidth));
            ArrayRangeFireTarget[1] = Convert.ToInt32(y1) / (Convert.ToInt32(pictureBox1Height) / Convert.ToInt32(TerrainHeight));

            ArrayRangeFireTarget[2] = Convert.ToInt32(x1 + R_Ext) / (Convert.ToInt32(pictureBox1Width) / Convert.ToInt32(TerrainWidth));
            ArrayRangeFireTarget[3] = Convert.ToInt32(y1) / (Convert.ToInt32(pictureBox1Height) / Convert.ToInt32(TerrainHeight));


            //Draw1.Rectangle(ArrayRangeFireTarget[0], ArrayRangeFireTarget[1], 3, 3,Color.Red);
            //Draw1.Rectangle(ArrayRangeFireTarget[2], ArrayRangeFireTarget[3], 3, 3, Color.Red); 

            //Draw1.Line(x1 - 15, y1, x1 - 5, y1, Color.Black, 3);
            //Draw1.Line(x1 + 5, y1, x1 + 15, y1, Color.Black, 3);
            //Draw1.Line(x1, y1 - 5, x1, y1 - 15, Color.Black, 3);
            //Draw1.Line(x1, y1 + 5, x1, y1 + 15, Color.Black, 3);

            //DrawInternalRaduisTarget();
            //DrawExternalRaduisTarget();
            CalculateVisibilityRangeFire(R_Int, R_Ext);
        }

        private void selectTrafficRouteEndToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            rectDrawAraay[0] = -10;
            rectDrawAraay[1] = -10;
            rectDrawAraay[2] = -10;
            rectDrawAraay[3] = -10;

            CounterTrafficRoute = 0;
            toolStripShowPointsOnLine.Enabled = false;
            toolStripVisibility.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute1.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute2.Enabled = false;

            FlagTarget = false;
            ArrayTrafficRoute = new int[ConstArrayTrafficRoute];
            IndexTrafficRouteFireArmTable = -1;
            FlagTrafficRoute = true;
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            Draw1 = new ClsGrafix(pictureBox1);
 
        }

    

        private void ExaminTrafficRouteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            int PrgBar = 0;
            toolStripStatusLabel1.Visible = true;
            System.Windows.Forms.Application.DoEvents();
            for (int index = 0; index < ClsGlobal.FireArmTable.Length / 6; index++)
            {
                if (ClsGlobal.FireArmTable[index, 5] == "True" || ClsGlobal.FireArmTable[index, 5] == "true")
                {
                    PrgBar++;
                }
            }
            //toolStripProgressBar1.Show();
            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Maximum = CounterTrafficRoute * PrgBar;
            toolStripProgressBar1.Step = 1;
            toolStripProgressBar1.Value = 0;
           //toolStripProgressBar1.Visible = true;
          
            //here will check every point in the ArrayTrafficRoute
            toolStripShowPointsOnLine.Enabled = false;
            toolStripVisibility.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute1.Enabled = true;
            toolStripMenuItemAnalysisPointOnRoute2.Enabled = true;
            FlagAnalysisPointOnRoute = false;
            FlagTrafficRoute = true;
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            Draw1 = new ClsGrafix(pictureBox1);
           // DrawLineBetweenTrafficPoints();
           // int j = 1;
            //for (int i = 0; i < CounterTrafficRoute; i = i + 2)
            //{
            //    Label lblNum = new Label();
            //    lblNum.Text = (j).ToString();
            //    lblNum.ForeColor = Color.Red;
            //    lblNum.AutoSize = true;
            //    lblNum.BackColor = Color.Lime;
            //    lblNum.Size = new System.Drawing.Size(10, 15);
            //    lblNum.AutoSize = true;
            //    lblNum.Location = new System.Drawing.Point(ArrayTrafficRoute[i]+2, ArrayTrafficRoute[i + 1]+3);
            //    pictureBox1.Controls.Add(lblNum);
            //    j += 1;
            //}
          
            
            for (int index = 0; index < ClsGlobal.FireArmTable.Length / 6; index++)
            {
                  IndexTrafficRouteFireArmTable = index;
                  if (ClsGlobal.FireArmTable[index, 5] == "True" || ClsGlobal.FireArmTable[index, 5] == "true")
                  {
                    for (int i = 0; i < CounterTrafficRoute; i = i + 2)
                    {
                        toolStripProgressBar1.Value = toolStripProgressBar1.Value + 2;
                        x1 = ArrayTrafficRoute[i];
                        y1 = ArrayTrafficRoute[i + 1];
                        SetTargetForTrafficRoute(Convert.ToInt16(ClsGlobal.FireArmTable[index, 1]), Convert.ToInt16(ClsGlobal.FireArmTable[index, 2]));
                        // toolStripButtonShowElementsOnRaduis.Enabled = true;
                        // FlagTarget = false;
                    }
                  }
               
            }
            DrawLineBetweenTrafficPoints();
            //toolStripProgressBar1.Value = 0;
            toolStripStatusLabel1.Visible = false;
            //toolStripShowPointsOnLine.Enabled = false;
            //toolStripVisibility.Enabled = false;
            //CounterTrafficRoute = 0;
            //ArrayTrafficRoute = new int[ConstArrayTrafficRoute];
            //FlagTrafficRoute = false;
            //IndexTrafficRouteFireArmTable = -1;
        }

        private void DrawLineBetweenTrafficPoints()
        {
            int h=0;
            for (int i = 1; i < CounterTrafficRoute/2; i++)
            {
                Draw1.Rectangle(ArrayTrafficRoute[h], ArrayTrafficRoute[h+1],2,2, Color.Black, 2);
                Draw1.Line(ArrayTrafficRoute[h], ArrayTrafficRoute[h+1], ArrayTrafficRoute[h+2], ArrayTrafficRoute[h+3], Color.Blue, 2);
                h = h + 2;
            }
        }

        private void endTrafficRouteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            toolStripShowPointsOnLine.Enabled = false;
            toolStripVisibility.Enabled = false;
            FlagAnalysisPointOnRoute = false;
            toolStripMenuItemAnalysisPointOnRoute1.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute2.Enabled = false;
            CounterTrafficRoute = 0;
            ArrayTrafficRoute = new int[ConstArrayTrafficRoute];
            FlagTrafficRoute = false;
            IndexTrafficRouteFireArmTable = -1;
         
           // pictureBox1_Click(sender, e); 
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            Draw1 = new ClsGrafix(pictureBox1);
          
        }

        private void exportToHeightMapPngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "*.png|*.png";
            saveFileDialog1.ShowDialog();
            Draw1.SavePic(saveFileDialog1.FileName , ImageFormat.Png);
        }

       

        private void createSideCutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FlagMatrix = false;
            FlagDrawLine = false;
            FlagShowElements = false;
            FlagTrafficRoute = false;
            FlagDrawRectForSideCut = false;
            FlagTarget = false;
            toolStripMenuItemAnalysisPointOnRoute1.Enabled = false;
            toolStripMenuItemAnalysisPointOnRoute2.Enabled = false;
            FlagDrawRectForSideCut = true;
            rectDrawAraay[0] = -10;
            rectDrawAraay[1] = -10;
            rectDrawAraay[2] = -10;
            rectDrawAraay[3] = -10;
            FlagTrafficRoute = false;
            IndexTrafficRouteFireArmTable = -1;
        }

        private void selectSingleFirearmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            demoOfGettingFirearmsDetailsToolStripMenuItem_Click(sender, e);
        }

        private void selectMultipleFirearmsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectMultiplyFirearmsToolStripMenuItem_Click(sender, e);
        }

        private void manageFirearmsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            manageFirearmsToolStripMenuItem_Click(sender, e);
        }

        private void exportToHeightMapPngToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            exportToHeightMapPngToolStripMenuItem_Click(sender, e);
        }

        private void exportMountainONLYToHeigthMapPngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exportMountainToJpgToolStripMenuItem_Click(sender, e);
        }

        private void createRouteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectTrafficRouteEndToolStripMenuItem_Click_1(sender, e);
        }

        private void analysisRouteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExaminTrafficRouteToolStripMenuItem_Click_1(sender, e);
        }

        private void endRouteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            endTrafficRouteToolStripMenuItem1_Click(sender, e);
        }

        private void createSideCutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            createSideCutToolStripMenuItem_Click(sender, e);
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            txtBoxSetDisTrafficRoute.Text = toolStripTextBox1.Text;
        }

        private void txtBoxSetDisTrafficRoute_TextChanged(object sender, EventArgs e)
        {
          toolStripTextBox1.Text=  txtBoxSetDisTrafficRoute.Text ;
        }

        private void toolStripButtonClearMap_Click(object sender, EventArgs e)
        {
            endTrafficRouteToolStripMenuItem1_Click(sender, e);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            FlagTrafficRoute = false;
            FlagAnalysisPointOnRoute = true;
           
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            toolStripMenuItem4_Click(sender, e);
        }

        private void SetAttackLocation1_TextChanged(object sender, EventArgs e)
        {
            SetAttackLocation2.Text = SetAttackLocation1.Text;
        }

        private void SetAttackLocation2_TextChanged(object sender, EventArgs e)
        {
            SetAttackLocation1.Text = SetAttackLocation2.Text;
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            printDocument1.OriginAtMargins = true;
            printDocument1.DocumentName = "Terrain Optinal Coverage";

            printDialog1.Document = printDocument1;
            //printDialog1.ShowDialog();
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {

            e.Graphics.DrawImage(pictureBox1.Image, 0, 0);
        }

        private void createSideCutWithDistanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            Draw1 = new ClsGrafix(pictureBox1);
            createSideCutToolStripMenuItem_Click(sender, e);
            FlagcreateSideCutWithDistance = true;

        }

    }
}
