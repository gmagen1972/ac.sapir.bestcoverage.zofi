using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame5
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public struct VertexPositionColorNormal
        {
            public Vector3 Position;
            public Color Color;
            public Vector3 Normal;

            public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
            (
                new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                new VertexElement(sizeof(float) * 3, VertexElementFormat.Color, VertexElementUsage.Color, 0),
                new VertexElement(sizeof(float) * 3 + 4, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0)
            );
        }


        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GraphicsDevice device;

        Effect effect;
        VertexPositionColorNormal[] vertices;
        //VertexPositionColor[] vertices;
        Matrix viewMatrix;
        Matrix projectionMatrix;
        Int16[] indices;
        Texture2D heightMap;
        string HeightMapName;
        bool H = false;
        SpriteFont font;
        string T="F1-Help";
        string HeightMapFile = ".\\maps\\kisufim.asc";
        Vector2 SpritePos = new Vector2(10, 370);
        Texture2D SpriteTexture;
        FillMode FMode = FillMode.WireFrame;

        Texture2D sceneryTexture;

        private float angleX = 0f;
        private float angleY = 50f;
        private float angleZ = -25f;

        private int terrainWidth ;
        private int terrainHeight;
        private float[,] heightData;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            Window.Title = "Terrain 3D Scrolling";
            HeightMapName = "heightmap";
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            device = graphics.GraphicsDevice;

            effect = Content.Load<Effect>("effects"); 
            SetUpCamera();

            heightMap = Content.Load<Texture2D>(HeightMapName); 
            //LoadHeightData(heightMap);
            LoadHeightDataAscFile(HeightMapFile);

            SetUpVertices();
            SetUpIndices();

            font = Content.Load<SpriteFont>("SpriteFont1");

            SpriteTexture = this.Content.Load<Texture2D>("KeyboardArrows");
            CalculateNormals();

            //sceneryTexture = Content.Load<Texture2D>("texturemap");
        }

        protected override void UnloadContent()
        {
        }


       

        private void SetUpVertices()
        {

            float minHeight = float.MaxValue;
            float maxHeight = float.MinValue;
            for (int x = 0; x < terrainWidth; x++)
            {
                for (int y = 0; y < terrainHeight; y++)
                {
                    if (heightData[x, y] < minHeight)
                        minHeight = heightData[x, y];
                    if (heightData[x, y] > maxHeight)
                        maxHeight = heightData[x, y];
                }
            }

            //vertices = new VertexPositionColor[terrainWidth * terrainHeight];
            vertices = new VertexPositionColorNormal[terrainWidth * terrainHeight];
            for (int x = 0; x < terrainWidth; x++)
            {
                for (int y = 0; y < terrainHeight; y++)
                {
                    //vertices[x + y * terrainWidth].Position = new Vector3(x, heightData[x, y], -y);
                    //vertices[x + y * terrainWidth].Color = Color.White;

                    vertices[x + y * terrainWidth].Position = new Vector3(x, heightData[x, y], -y);
                 

                    if (heightData[x, y] < minHeight + (maxHeight - minHeight) / 4)
                        vertices[x + y * terrainWidth].Color = Color.Blue;
                    else if (heightData[x, y] < minHeight + (maxHeight - minHeight) * 2 / 4)
                        vertices[x + y * terrainWidth].Color = Color.Green;
                    else if (heightData[x, y] < minHeight + (maxHeight - minHeight) * 3 / 4)
                        vertices[x + y * terrainWidth].Color = Color.Brown;
                    else
                        vertices[x + y * terrainWidth].Color = Color.White;
                }
            }
            
        }

        private void CalculateNormals()
        {
            for (int i = 0; i < vertices.Length; i++)
                vertices[i].Normal = new Vector3(0, 0, 0);

            for (int i = 0; i < indices.Length / 3; i++)
            {
                int index1 = indices[i * 3];
                int index2 = indices[i * 3 + 1];
                int index3 = indices[i * 3 + 2];

                Vector3 side1 = vertices[index1].Position - vertices[index3].Position;
                Vector3 side2 = vertices[index1].Position - vertices[index2].Position;
                Vector3 normal = Vector3.Cross(side1, side2);

                vertices[index1].Normal += normal;
                vertices[index2].Normal += normal;
                vertices[index3].Normal += normal;
            }

            for (int i = 0; i < vertices.Length; i++)
                vertices[i].Normal.Normalize();
        }


        private void SetUpIndices()
        {
            indices = new Int16[(terrainWidth - 1) * (terrainHeight - 1) * 6];
            int counter = 0;
            for (int y = 0; y < terrainHeight - 1; y++)
            {
                for (int x = 0; x < terrainWidth - 1; x++)
                {
                    int lowerLeft =  (x + y * terrainWidth);
                    int lowerRight = ((x + 1) + y * terrainWidth);
                    int topLeft =  ( x + (y + 1) * terrainWidth);
                    int topRight =  ((x + 1) + (y + 1) * terrainWidth);

                        indices[counter++] = Convert.ToInt16(topLeft);
                        indices[counter++] = Convert.ToInt16(lowerRight);
                        indices[counter++] = Convert.ToInt16(lowerLeft);

                        indices[counter++] = Convert.ToInt16(topLeft);
                        indices[counter++] = Convert.ToInt16(topRight);
                        indices[counter++] = Convert.ToInt16(lowerRight);
                    
                }
            }
        }

        private void LoadHeightDataAscFile(string file)
        {
            //terrainWidth =  heightMap.Width;
            //terrainHeight =  heightMap.Height;

            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols","");
            _T = _T.Replace(" ", "");
            terrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            terrainHeight = Convert.ToInt16(_T);
            heightData = new float[terrainWidth, terrainHeight];
            
            for (int y = 0; y< terrainHeight; y++)
            {
                string[] _x= T[6 + y].Split();
                for (int x = 0; x < terrainWidth; x++)
                {
                    heightData[x, y] = Convert.ToSingle(_x[x])/2.5f;
                }
            }
        }

        private void LoadHeightData(Texture2D heightMap)
        {
            terrainWidth =  heightMap.Width;
            terrainHeight =  heightMap.Height;

            Color[] heightMapColors = new Color[terrainWidth * terrainHeight];
            heightMap.GetData(heightMapColors);

            heightData = new float[terrainWidth, terrainHeight];
            for (int x = 0; x < terrainWidth; x++)
                for (int y = 0; y < terrainHeight; y++)
                    heightData[x, y] = heightMapColors[x + y * terrainWidth].R / 5.0f;
                    //heightData[x, y] = 0f;
        }

        private void SetUpCamera()
        {
            viewMatrix = Matrix.CreateLookAt(new Vector3(60, angleY,angleZ), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, device.Viewport.AspectRatio, 1.0f, 300.0f);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState keyState = Keyboard.GetState();
                        
            if (keyState.IsKeyDown(Keys.Left )) angleX += 0.05f;
            if (keyState.IsKeyDown(Keys.Right )) angleX -= 0.05f;

            if (keyState.IsKeyDown(Keys.Up))
            {
                angleY += 0.5f;
                SetUpCamera();
            }
            if (keyState.IsKeyDown(Keys.Down))
            {
                angleY -= 0.5f;
                SetUpCamera();
            }

            if (keyState.IsKeyDown(Keys.RightControl))
            {
                angleZ -= 0.5f;
                SetUpCamera();
            }
            if (keyState.IsKeyDown(Keys.LeftControl))
            {
                angleZ += 0.5f;
                SetUpCamera();
            }

            if (keyState.IsKeyDown(Keys.S ))
            {
                FMode = FillMode.Solid;
            }

            if (keyState.IsKeyDown(Keys.W))
            {
                FMode = FillMode.WireFrame;
            }
            
            if (keyState.IsKeyDown(Keys.F1))
            {
                H=!H;
                System.Threading.Thread.Sleep(200);
                if (H == true) T = "F2-Kisufim\r\nF3-Modiin\r\nF4-EmekHaelah1\r\nF5-EmekHaelah2\r\nF6-Dimona\r\nOther Keys: RCtrl-LCrl-S-W ";
                if (H == false) T = "F1-Help";
            }

            
            if (keyState.IsKeyDown(Keys.F2))
            {
                HeightMapFile = ".\\maps\\kisufim.asc";
                LoadContent();
            }

            if (keyState.IsKeyDown(Keys.F3))
            {
                HeightMapFile = ".\\maps\\Modiin.asc";
                LoadContent();
            }

            if (keyState.IsKeyDown(Keys.F4))
            {
                HeightMapFile = ".\\maps\\EmekHaelah1.asc";
                LoadContent();
            }

            if (keyState.IsKeyDown(Keys.F5))
            {
                HeightMapFile = ".\\maps\\EmekHaelah2.asc";
                LoadContent();
            }

            if (keyState.IsKeyDown(Keys.F6))
            {
                HeightMapFile = ".\\maps\\Dimona.asc";
                LoadContent();
            }
            
            if (keyState.IsKeyDown(Keys.Escape )) this.Exit() ;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //device.Clear(Color.Black);
            device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1.0f, 0);
            //SetUpCamera();
            
            RasterizerState rs = new RasterizerState();
            rs.CullMode = CullMode.None;
            rs.FillMode = FMode;
            //rs.FillMode = FillMode.Solid;
            device.RasterizerState = rs;
            
            Matrix worldMatrix = Matrix.CreateTranslation(-terrainWidth / 2.0f , 0, terrainHeight / 2.0f) * Matrix.CreateRotationY(angleX) ;

            effect.CurrentTechnique = effect.Techniques["ColoredNoShading"];
            effect.Parameters["xView"].SetValue(viewMatrix);
            effect.Parameters["xProjection"].SetValue(projectionMatrix);
            effect.Parameters["xWorld"].SetValue(worldMatrix);

            effect.CurrentTechnique = effect.Techniques["Colored"];
            effect.Parameters["xView"].SetValue(viewMatrix);
            effect.Parameters["xProjection"].SetValue(projectionMatrix);
            effect.Parameters["xWorld"].SetValue(worldMatrix);
            //Vector3 lightDirection = new Vector3(1.0f, -1.0f, -1.0f);
            Vector3 lightDirection = new Vector3(1.0f, -3.0f, -1.0f);
            lightDirection.Normalize();
            effect.Parameters["xLightDirection"].SetValue(lightDirection);
            effect.Parameters["xAmbient"].SetValue(0.1f);
            effect.Parameters["xEnableLighting"].SetValue(true);   

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                //device.DrawUserIndexedPrimitives(PrimitiveType.TriangleList  , vertices, 0, Convert.ToInt16(  vertices.Length), indices, 0,Convert.ToInt16(  indices.Length / 3), VertexPositionColor.VertexDeclaration);
                //device.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, vertices, 0,vertices.Length, indices, 0, indices.Length / 3, VertexPositionColor.VertexDeclaration);
                device.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, vertices, 0, vertices.Length, indices, 0, indices.Length / 3, VertexPositionColorNormal.VertexDeclaration);
            }
            spriteBatch.Begin();
            spriteBatch.DrawString(font,T, new Vector2(10, 10), Color.GreenYellow);
            spriteBatch.DrawString(font,"X:"+Convert.ToString(angleX)+"\r\nY:"+Convert.ToString(angleY)+"\r\nZ:"+Convert.ToString(angleZ), new Vector2(220, 400), Color.GreenYellow);
            spriteBatch.DrawString(font, "Height Map:" +HeightMapFile , new Vector2(300, 420), Color.GreenYellow);
            spriteBatch.Draw(SpriteTexture, SpritePos, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);

        }
    }
}
