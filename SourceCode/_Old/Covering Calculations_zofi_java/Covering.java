import java.io.*;
import java.util.*;
import java.math.*;

public class Covering
{
	private int Ymax, Xmax;
	private AreaMap theMap;
	private Visibility visibility;
	int orderedList[][];
	public Covering(AreaMap map, int Y, int X,Visibility theVisibility  )
	{
		theMap = map;
		visibility = theVisibility;
		Ymax=Y;
		Xmax=X;
		orderedList=new int [(Ymax+1)*(Xmax+1)][3];
		createOrderedList();
	}
	
	public void createOrderedList()
	{
		//first stage: summing the total number of visible points for each point
		//copying the total values and initialyzing the array
		int sum;
		
		for(int i=0; i<(Ymax+1)*(Xmax+1);i++)
		{
			sum=0;
			
			for(int j=0; j<(Ymax+1)*(Xmax+1);j++)
			{
				sum=sum+visibility.visibilityMatrix[i][j];
			}
			orderedList[i][0]=i;
			orderedList[i][1]= sum; // the last cell contains the total of visible points
			orderedList[i][2]=0;
		}
		
		// sorting the values using bubble sort
		int tempName;
		int tempValue;
		
		for (int k=(Ymax+1)*(Xmax+1)-1;k>=0;k--)
		{
			for(int j=0; j<k;j++)
			{
				if (orderedList[j][1]<orderedList[j+1][1])
				{
					tempName=orderedList[j][0];
					tempValue=orderedList[j][1];
					orderedList[j][0]=orderedList[j+1][0];
					orderedList[j][1]=orderedList[j+1][1];
					orderedList[j+1][0]=tempName;
					orderedList[j+1][1]=tempValue;
				}
			}
		}
		//printing for validation:
		
		/*
		System.out.print("\n"+"Y  X  total");
		for (int i=0;i<(Ymax+1)*(Xmax+1); i++)
		{
			
			int y=orderedList[i][0]/(Xmax+1);
			int x=orderedList[i][0]-((Xmax+1)*y);
			System.out.print("\n"+y+"  "+x+"  "+orderedList[i][1]);
		}
		
		*/
	
	}//end of function
	
	void findTheBestQpoints()
	{
		int counter=1;
		
		for (int i=0;i<(Ymax+1)*(Xmax+1); i++) // the number of iterations = the number of points (q)
		{
			if (orderedList[i][2]==0) // if the point is not visible from a point which was already chosen
			{
				int y=orderedList[i][0]/(Xmax+1);
				int x=orderedList[i][0]-((Xmax+1)*y);
				
				System.out.print("\n"+"point number "+counter+" is: "+y+" "+x);
				counter=counter+1;
				
				for(int j=0; j<(Ymax+1)*(Xmax+1);j++)  // updating 1 to all the points which are visible to it
				{
					if (visibility.visibilityMatrix[orderedList[i][0]][orderedList[j][0]]==1)
					{
						orderedList[j][2]=1;
					}

				}
			}
			//else
			//{
			//	while(orderedList[i][2]==1)
			//	{
			//		i=i+1;
			//	}
			//}
		}
	}//end of function
		
		
	
	
	
	
	
}// end of class
