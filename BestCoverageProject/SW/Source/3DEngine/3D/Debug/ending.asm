; Listing generated by Microsoft (R) Optimizing Compiler Version 13.10.3077 

	TITLE	.\ending.cpp
	.386P
include listing.inc
if @Version gt 510
.model FLAT
else
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
_DATA	SEGMENT DWORD USE32 PUBLIC 'DATA'
_DATA	ENDS
CONST	SEGMENT DWORD USE32 PUBLIC 'CONST'
CONST	ENDS
_BSS	SEGMENT DWORD USE32 PUBLIC 'BSS'
_BSS	ENDS
$$SYMBOLS	SEGMENT BYTE USE32 'DEBSYM'
$$SYMBOLS	ENDS
$$TYPES	SEGMENT BYTE USE32 'DEBTYP'
$$TYPES	ENDS
_TLS	SEGMENT DWORD USE32 PUBLIC 'TLS'
_TLS	ENDS
;	COMDAT _IsEqualGUID
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _==
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _acosl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _asinl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _atanl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _atan2l
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _ceill
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _cosl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _coshl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _expl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _fabsl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _floorl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _fmodl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _frexpl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _ldexpl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _logl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _log10l
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _modfl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _powl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _sinl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _sinhl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _sqrtl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _tanl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _tanhl
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _frexpf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _ldexpf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _acosf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _asinf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _atanf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _atan2f
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _ceilf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _cosf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _coshf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _expf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _fabsf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _floorf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _fmodf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _logf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _log10f
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _modff
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _powf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _sinf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _sinhf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _sqrtf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _tanf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT _tanhf
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ??0Ending@@QAE@XZ
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ??1Ending@@QAE@XZ
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ?Render@Ending@@QAEXXZ
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ??$_Pow_int@N@@YANNH@Z
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ??$_Pow_int@H@@YAHHH@Z
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ??$_Pow_int@M@@YAMMH@Z
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
;	COMDAT ??$_Pow_int@O@@YAOOH@Z
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
sxdata	SEGMENT DWORD USE32 'SXDATA'
sxdata	ENDS
FLAT	GROUP _DATA, CONST, _BSS
	ASSUME	CS: FLAT, DS: FLAT, SS: FLAT
endif

INCLUDELIB LIBCMTD
INCLUDELIB OLDNAMES

_BSS	SEGMENT
?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA DD 018H DUP (?) ; `Ending::Render'::`2'::squareVerts
_BSS	ENDS
_DATA	SEGMENT
?squareRad@?1??Render@Ending@@QAEXXZ@4MA DD 042200000r ; 40 ; `Ending::Render'::`2'::squareRad
?squareDepth@?1??Render@Ending@@QAEXXZ@4MA DD 041200000r ; 10 ; `Ending::Render'::`2'::squareDepth
?squareQuads@?1??Render@Ending@@QAEXXZ@4PAY03KA DD 00H	; `Ending::Render'::`2'::squareQuads
	DD	01H
	DD	02H
	DD	03H
	DD	04H
	DD	00H
	DD	03H
	DD	07H
	DD	07H
	DD	03H
	DD	02H
	DD	06H
	DD	06H
	DD	02H
	DD	01H
	DD	05H
	DD	04H
	DD	05H
	DD	01H
	DD	00H
	DD	03H
	DD	02H
	DD	06H
	DD	07H
?nSquareQuads@?1??Render@Ending@@QAEXXZ@4KA DD 06H	; `Ending::Render'::`2'::nSquareQuads
_DATA	ENDS
PUBLIC	??0Ending@@QAE@XZ				; Ending::Ending
EXTRN	__RTC_InitBase:NEAR
EXTRN	__RTC_Shutdown:NEAR
;	COMDAT rtc$IMZ
; File c:\documents and settings\yair levy\����� ������\terrain engine\vc6project\ending.cpp
rtc$IMZ	SEGMENT
__RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ	ENDS
;	COMDAT rtc$TMZ
rtc$TMZ	SEGMENT
__RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
; Function compile flags: /Odt /RTCsu /ZI
rtc$TMZ	ENDS
;	COMDAT ??0Ending@@QAE@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
??0Ending@@QAE@XZ PROC NEAR				; Ending::Ending, COMDAT
; _this$ = ecx

; 15   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 16   : }

	mov	eax, DWORD PTR _this$[ebp]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
??0Ending@@QAE@XZ ENDP					; Ending::Ending
_TEXT	ENDS
PUBLIC	??1Ending@@QAE@XZ				; Ending::~Ending
; Function compile flags: /Odt /RTCsu /ZI
;	COMDAT ??1Ending@@QAE@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
??1Ending@@QAE@XZ PROC NEAR				; Ending::~Ending, COMDAT
; _this$ = ecx

; 19   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 20   : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
??1Ending@@QAE@XZ ENDP					; Ending::~Ending
_TEXT	ENDS
PUBLIC	?Render@Ending@@QAEXXZ				; Ending::Render
EXTRN	__RTC_CheckEsp:NEAR
EXTRN	__fltused:NEAR
EXTRN	__imp__glBegin@4:NEAR
EXTRN	__imp__glVertex3fv@4:NEAR
EXTRN	__imp__glColor4f@16:NEAR
EXTRN	__imp__glEnd@0:NEAR
_BSS	SEGMENT
?$S1@?1??Render@Ending@@QAEXXZ@4IA DD 01H DUP (?)	; `Ending::Render'::`2'::$S1
; Function compile flags: /Odt /RTCsu /ZI
_BSS	ENDS
;	COMDAT ?Render@Ending@@QAEXXZ
_TEXT	SEGMENT
_j$23479 = -32						; size = 4
_i$23475 = -20						; size = 4
_this$ = -8						; size = 4
?Render@Ending@@QAEXXZ PROC NEAR			; Ending::Render, COMDAT
; _this$ = ecx

; 25   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 228				; 000000e4H
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-228]
	mov	ecx, 57					; 00000039H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 26   : 
; 27   : 
; 28   : 	// a square object (not total 3d square, depth is smaller)
; 29   : 	static float squareRad = 40;
; 30   : 	static float squareDepth = 10;
; 31   : 
; 32   : 	static float squareVerts[8][3] = {
; 33   : 		{-squareRad, -squareRad, +squareDepth},	// 0

	mov	eax, DWORD PTR ?$S1@?1??Render@Ending@@QAEXXZ@4IA
	and	eax, 1
	jne	$L23470
	mov	eax, DWORD PTR ?$S1@?1??Render@Ending@@QAEXXZ@4IA
	or	eax, 1
	mov	DWORD PTR ?$S1@?1??Render@Ending@@QAEXXZ@4IA, eax
	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA
	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+4
	mov	eax, DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+8, eax

; 34   : 		{+squareRad, -squareRad, +squareDepth},	// 1

	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+12, eax
	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+16
	mov	eax, DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+20, eax

; 35   : 		{+squareRad, +squareRad, +squareDepth},	// 2

	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+24, eax
	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+28, eax
	mov	eax, DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+32, eax

; 36   : 		{-squareRad, +squareRad, +squareDepth},	// 3

	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+36
	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+40, eax
	mov	eax, DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+44, eax

; 37   : 		{-squareRad, -squareRad, -squareDepth},	// 4

	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+48
	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+52
	fld	DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+56

; 38   : 		{+squareRad, -squareRad, -squareDepth},	// 5

	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+60, eax
	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+64
	fld	DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+68

; 39   : 		{+squareRad, +squareRad, -squareDepth},	// 6

	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+72, eax
	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+76, eax
	fld	DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+80

; 40   : 		{-squareRad, +squareRad, -squareDepth}	// 7

	fld	DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+84
	mov	eax, DWORD PTR ?squareRad@?1??Render@Ending@@QAEXXZ@4MA
	mov	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+88, eax
	fld	DWORD PTR ?squareDepth@?1??Render@Ending@@QAEXXZ@4MA
	fchs
	fstp	DWORD PTR ?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA+92
$L23470:

; 41   : 	};
; 42   : 
; 43   : 	static dword squareQuads[6][4] = {
; 44   : 		{0,1,2,3},	// front
; 45   : 		{4,0,3,7},	// left
; 46   : 		{7,3,2,6},	// bottom
; 47   : 		{6,2,1,5},	// right
; 48   : 		{4,5,1,0},	// top
; 49   : 		{3,2,6,7}	// bottom
; 50   : 	};
; 51   : 
; 52   : 	static dword nSquareQuads = 6;
; 53   : 
; 54   : 	// draw a single square
; 55   : 	glBegin(GL_QUADS);

	mov	esi, esp
	push	7
	call	DWORD PTR __imp__glBegin@4
	cmp	esi, esp
	call	__RTC_CheckEsp

; 56   : 
; 57   : 		for (dword i=0; i<nSquareQuads; i++) {

	mov	DWORD PTR _i$23475[ebp], 0
	jmp	SHORT $L23476
$L23477:
	mov	eax, DWORD PTR _i$23475[ebp]
	add	eax, 1
	mov	DWORD PTR _i$23475[ebp], eax
$L23476:
	mov	eax, DWORD PTR _i$23475[ebp]
	cmp	eax, DWORD PTR ?nSquareQuads@?1??Render@Ending@@QAEXXZ@4KA
	jae	SHORT $L23478

; 58   : 			for (dword j=0; j<4; j++) {

	mov	DWORD PTR _j$23479[ebp], 0
	jmp	SHORT $L23480
$L23481:
	mov	eax, DWORD PTR _j$23479[ebp]
	add	eax, 1
	mov	DWORD PTR _j$23479[ebp], eax
$L23480:
	cmp	DWORD PTR _j$23479[ebp], 4
	jae	SHORT $L23482

; 59   : 				glColor4f(1, 1, 1, 0.5);

	mov	esi, esp
	push	1056964608				; 3f000000H
	push	1065353216				; 3f800000H
	push	1065353216				; 3f800000H
	push	1065353216				; 3f800000H
	call	DWORD PTR __imp__glColor4f@16
	cmp	esi, esp
	call	__RTC_CheckEsp

; 60   : 				glVertex3fv(squareVerts[ squareQuads[i][j] ]);

	mov	eax, DWORD PTR _i$23475[ebp]
	shl	eax, 4
	mov	ecx, DWORD PTR _j$23479[ebp]
	mov	edx, DWORD PTR ?squareQuads@?1??Render@Ending@@QAEXXZ@4PAY03KA[eax+ecx*4]
	imul	edx, 12					; 0000000cH
	add	edx, OFFSET FLAT:?squareVerts@?1??Render@Ending@@QAEXXZ@4PAY02MA
	mov	esi, esp
	push	edx
	call	DWORD PTR __imp__glVertex3fv@4
	cmp	esi, esp
	call	__RTC_CheckEsp

; 61   : 			}

	jmp	SHORT $L23481
$L23482:

; 62   : 		}

	jmp	SHORT $L23477
$L23478:

; 63   : 
; 64   : 	glEnd();

	mov	esi, esp
	call	DWORD PTR __imp__glEnd@0
	cmp	esi, esp
	call	__RTC_CheckEsp

; 65   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 228				; 000000e4H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?Render@Ending@@QAEXXZ ENDP				; Ending::Render
_TEXT	ENDS
END
