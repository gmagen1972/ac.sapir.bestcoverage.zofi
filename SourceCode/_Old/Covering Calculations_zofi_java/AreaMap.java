import java.io.*;
import java.util.*;
import java.math.*;

public class AreaMap
{
	public static AreaMap mapa;
	public int Max_Y,Max_X;
	public int cellSize;
	public int theMap[][];
	
	public AreaMap(String textFileName, int Max_Y_Coordinate, int Max_X_Coordinate, int cell_size) throws IOException
	{
		Max_Y = Max_Y_Coordinate;
		Max_X = Max_X_Coordinate;
		cellSize = cell_size;
		theMap = new int[Max_Y+1][Max_X+1];

		FileInputStream stream = new FileInputStream(textFileName); 
		InputStreamReader reader = new InputStreamReader(stream);
		StreamTokenizer tokens = new StreamTokenizer(reader);
 
		for (int i=Max_Y;i>=0;i--)
		{
			for (int j=0 ; j<=Max_X; j++)
			{
				 tokens.nextToken();
				 theMap[i][j] = (int)tokens.nval;
			}
		}
		stream.close(); 
	}	
	
	
	public void displayMap()
	{
		for (int i=Max_Y;i>=0;i--)
		{
			if(i>=10)
				System.out.print("\n"+"Y="+i+"  ");
			else
				System.out.print("\n"+"Y="+i+"   ");
				
			for (int j=0; j<=Max_X; j++)
		    System.out.print(theMap[i][j]+ " ");
			
		}
	}	
}
		

		
		
	