public class MovementTime
{
	private AreaMap theMap;
	private int Ymax, Xmax;
	private float dijkstraTable_Time[][];
	private int dijkstraTable_PathNeighborY[][];
	private int dijkstraTable_PathNeighborX[][];
	private int dijkstraTable_Asterisk[][];
	private int lastXUpdated, lastYUpdated;
	
	public  MovementTime(AreaMap map)//constructor
	{
		theMap = map;
		Ymax = theMap.Max_Y;
		Xmax = theMap.Max_X;
		dijkstraTable_Time = new float[Ymax+1][Xmax+1];
		dijkstraTable_PathNeighborY = new int[Ymax+1][Xmax+1];
		dijkstraTable_PathNeighborX = new int[Ymax+1][Xmax+1];
		dijkstraTable_Asterisk = new int[Ymax+1][Xmax+1];
	}
		
	public float movementTime(int Yorigin, int Xorigin, int Ytarget, int Xtarget)
	{
		initializeDijkstra();//seting the initial dijkstra values
		
		dijkstraTable_Time[Yorigin][Xorigin] = 0;
		dijkstraTable_PathNeighborY[Yorigin][Xorigin] = Yorigin;
		dijkstraTable_PathNeighborX[Yorigin][Xorigin] = Xorigin;
		dijkstraTable_Asterisk[Yorigin][Xorigin] = 1;
		
		lastXUpdated = Xorigin;
		lastYUpdated = Yorigin;
		
		while(dijkstraTable_Asterisk[Ytarget][Xtarget] != 1)
		{
			setValuesForAllNeighbors();
			makeTheSmallestValueConstant();
		}
	
		printPath(Yorigin,Xorigin,Ytarget,Xtarget);////////Remove//////////
		return dijkstraTable_Time[Ytarget][Xtarget];
	
	}//end of function
	
	
	private void setValuesForAllNeighbors()
	{
		for(int i = Ymax; i >= 0; i--)
		{
			for(int j = 0; j <= Xmax; j++)
			{
				if(dijkstraTable_Asterisk[i][j] != 1)
				{
					if((i+1 == lastYUpdated && j ==lastXUpdated) || (i+1 == lastYUpdated && j+1 ==lastXUpdated)
					   || (i+1 == lastYUpdated && j-1 ==lastXUpdated) || (i == lastYUpdated && j+1 ==lastXUpdated)
					   || (i == lastYUpdated && j-1 ==lastXUpdated) ||(i-1 == lastYUpdated && j ==lastXUpdated) 
					   || (i-1 == lastYUpdated && j+1 ==lastXUpdated) || (i-1 == lastYUpdated && j-1 ==lastXUpdated))
					{
						if(dijkstraTable_Time[lastYUpdated][lastXUpdated]+calculateTime(lastYUpdated,lastXUpdated,i,j)< dijkstraTable_Time[i][j])
						{
							dijkstraTable_Time[i][j]=dijkstraTable_Time[lastYUpdated][lastXUpdated]+calculateTime(lastYUpdated,lastXUpdated,i,j);
							dijkstraTable_PathNeighborY[i][j]=lastYUpdated;
							dijkstraTable_PathNeighborX[i][j]=lastXUpdated;
						}
					}
				}
			}
		}
	}
	
	private float calculateTime(int Yorigin , int Xorigin , int Ytarget , int Xtarget)
	{
		float a = 50*((float)Ytarget-(float)Yorigin);
		float b = 50*((float)Xtarget-(float)Xorigin);
		float airDistance = (float)Math.sqrt(a*a+b*b);
		float hightDif = theMap.theMap[Ytarget][Xtarget] - theMap.theMap[Yorigin][Xorigin];
		float angleTan = hightDif/airDistance;
		float groundDistance = (float)Math.sqrt(hightDif*hightDif + airDistance*airDistance);
		if(angleTan >= -0.839 && angleTan < -0.7)
			return (float)(groundDistance/0.5);
		if(angleTan >= -0.7 && angleTan < -0.577)
			return groundDistance/1;
		if(angleTan >= -0.577 && angleTan < -0.466)
			return groundDistance/2;
		if(angleTan >= -0.466 && angleTan < -0.363)
			return groundDistance/4;
		if(angleTan >= -0.363 && angleTan < -0.267)
			return groundDistance/5;
		if(angleTan >= -0.267 && angleTan < -0.176)
			return groundDistance/8;
		if(angleTan >= -0.176 && angleTan < -0.087)
			return groundDistance/11;
		if(angleTan >= -0.087 && angleTan < 0)
			return groundDistance/15;
		if(angleTan >= 0 && angleTan < 0.087)
			return groundDistance/13;
		if(angleTan >= 0.087 && angleTan < 0.176)
			return groundDistance/11;
		if(angleTan >= 0.176 && angleTan < 0.267)
			return groundDistance/8;
		if(angleTan >= 0.267 && angleTan < 0.363)
			return groundDistance/5;
		if(angleTan >= 0.363 && angleTan < 0.466)
			return groundDistance/3;
		if(angleTan >= 0.466 && angleTan < 0.577)
			return groundDistance/2;
		if(angleTan >= 0.577 && angleTan < 0.7)
			return (float)(groundDistance/0.5);
		
		return 100000;//case of unpassable angle
	}
	

private void makeTheSmallestValueConstant()
{
	float tempSmallestValue=100000;
	int tempXposition=0;
	int tempYposition=0;
	int flag=0;
	for(int i=Ymax; i>=0; i--)
	{
		for(int j=0; j<=Xmax; j++)
		{
			if((dijkstraTable_Asterisk[i][j]!= 1)&&(dijkstraTable_Time[i][j]<tempSmallestValue))
			{
				tempSmallestValue=dijkstraTable_Time[i][j];
				tempXposition=j;
				tempYposition=i;
				flag=1;
			}
		}
	}

	if(flag==1)
	{
		dijkstraTable_Asterisk[tempYposition][tempXposition]=1;//make value constant
		lastXUpdated=tempXposition;
		lastYUpdated=tempYposition;
	}
}

	
private void initializeDijkstra()
{
	for(int i = Ymax; i >= 0; i--)
	{
			for(int j = 0; j <= Xmax; j++)
			{
				dijkstraTable_Time[i][j] = 100000;
				dijkstraTable_PathNeighborY[i][j] = 0;
				dijkstraTable_PathNeighborX[i][j] = 0;
				dijkstraTable_Asterisk[i][j] = 0;
			}
	}
}


private void printPath( int Yorigin, int Xorigin, int Ytarget, int Xtarget)
{
	int Xtar = Xtarget; int Ytar = Ytarget;
	int i=0;
	System.out.print("\n");
	System.out.println("Origin: ("+Yorigin+", "+Xorigin+")");
	System.out.println("Destination: ("+Ytar+", "+Xtar+")");
	System.out.println("Total time to location: "+dijkstraTable_Time[Ytar][Xtar]);
	
	while(Ytar != Yorigin || Xtar != Xorigin)
	{
		i=i+1;
		Ytar = dijkstraTable_PathNeighborY[Ytar][Xtar];
		Xtar = dijkstraTable_PathNeighborX[Ytar][Xtar];
		System.out.print("Step " + i);
		System.out.print("("+Ytar+", "+Xtar+")");
		System.out.println("Time: "+dijkstraTable_Time[Ytar][Xtar]);
		
	}


}

}//endOfClass
