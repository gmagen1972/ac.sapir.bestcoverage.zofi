﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class Frm4Map : Form
    {
        int[] R;
        int[] G;
        int[] B;
        int x=120, y=100;
        bool Tmr = false;
        short[,] HeightData;
        short TerrainWidth, TerrainHeight;
        float CellSize;
        short HeightMin, HeightMax;
        Graphics GR,GT;
        Pen P,PT;
        short Zoom = 10;
        bool MouseKey=false;
        ClsShader Pal1,PalGS;
        public string MapFile;
        public string JpgMap;
        double xt, yt;

        struct Stat
        {
            public float min;
            public float max;
        }

        private void LoadHeightDataAscFile(string file)
        {
            toolStripProgressBar1.Minimum = 1;
            toolStripProgressBar1.Maximum = 150;

            int x = 0, y = 0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth = Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight = Convert.ToInt16(_T);
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle(_T);

            HeightData = new short[TerrainWidth, TerrainHeight];
            toolStripProgressBar1.Value = 50;

            int PBRes = (int) TerrainHeight / 100;
            //LoadAddData
            int C = 0;

            for (y = 0; y < TerrainHeight; y++)
            {
                C += 1;
                if (C > PBRes)
                {
                    Application.DoEvents();
                    toolStripProgressBar1.Value += 1;
                    C = 0;
                    toolStripStatusLabel1.Text = (toolStripProgressBar1.Value - 50).ToString() + "%";
                }

                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    __T = Convert.ToSingle(_x[x]);
                    if (__T != -9999F)
                    {
                        HeightData[x, y] = Convert.ToInt16 (__T*10);
                    }
                    //else
                    //{
                    //    HeightData[x, y] = 0;
                    //}

                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = HeightData[x, y];
                        HeightMin = HeightData[x, y];
                    }
                    HeightMin = Math.Min(HeightData[x, y], HeightMin);
                    HeightMax = Math.Max(HeightData[x, y], HeightMax);        
                }
            }



            //for (y = 0; y < TerrainHeight; y++)
            //{
            //    string[] _x = T[6 + y].Split();
            //    for (x = 0; x < TerrainWidth; x++)
            //    {
            //        HeightData[x, y] = Convert.ToSingle(_x[x]);
            //        if (HeightData[x, y] == -9999)
            //        {
            //            HeightData[x, y] = HeightMin;
            //        }
            //    }
            //}
        }

        private void LoadHeightDataAscFile2(string file)
        {
            try
            {
                toolStripProgressBar1.Minimum = 1;
                toolStripProgressBar1.Maximum = 100;

                int x = 0, y = 0;
                float __T;
                
                System.IO.StreamReader File = new System.IO.StreamReader(file);
                TerrainWidth = Convert.ToInt16(File.ReadLine().Replace("ncols", ""));
                TerrainHeight = Convert.ToInt16(File.ReadLine().Replace("nrows", ""));
                string H= File.ReadLine();
                H = File.ReadLine();
                H = File.ReadLine();
                H= File.ReadLine();

                string[] T = new string[TerrainHeight];
                HeightData = new short[TerrainWidth, TerrainHeight];
                
                int PBRes = (int)TerrainHeight / 100;
                
                int C = 0;
                for (y = 0; y < TerrainHeight ; y++)
                {
                    C += 1;
                    if (C > PBRes)
                    {
                        Application.DoEvents();
                        toolStripProgressBar1.Value += 1;
                        C = 0;
                        toolStripStatusLabel1.Text = (toolStripProgressBar1.Value).ToString() + "%";
                    }

                    T[6 + y] = File.ReadLine();
                    string[] _x = T[6 + y].Split();
                    for (x = 0; x < TerrainWidth; x++)
                    {
                        __T = Convert.ToSingle(_x[x]);
                        if (__T != -9999F)
                        {
                            HeightData[x, y] = Convert.ToInt16(__T * 10);
                        }

                        if ((x == 0) && (y == 0))
                        {
                            HeightMax = HeightData[x, y];
                            HeightMin = HeightData[x, y];
                        }
                        HeightMin = Math.Min(HeightData[x, y], HeightMin);
                        HeightMax = Math.Max(HeightData[x, y], HeightMax);
                    }
                }

                ClsShader Pal1 = new ClsShader("Pallete01.clr");
            }
            catch
            {

            }

        }

        
        private void ColorShaderBW(int _Min,int _Max)
        {
            int C;
            int W=Convert.ToInt16(_Max-_Min);
            R = new int[W];
            G = new int[W];
            B = new int[W];

            for (C=0;C<W;C++)
            {
                R[C] = Convert.ToInt16(C * 255 / W);
                G[C] = Convert.ToInt16(C * 255 / W);
                B[C] = Convert.ToInt16(C * 255 / W);
            }
        }

        public Frm4Map()
        {
            InitializeComponent();
        }

        private void MatchPix()
        {
            PicBox1.Top = 10;
            PicBox1.Left = 10;
            PicBox1.Width = this.Width - 30;
            PicBox1.Height = this.Height - 50;
        }

        private void ResizePix()
        {
            this.Width = Convert.ToInt16((TerrainWidth/10))+30;
            this.Height = Convert.ToInt16((TerrainHeight/10)) + 30;
            PicBox1.Top = 10;
            PicBox1.Left = 10;
            PicBox1.Width = this.Width - 30;
            PicBox1.Height = this.Height - 50;
        }

        private void DrawMap()
        {
            
            int C;

            for (int y = 0; y < TerrainHeight;y+=20 )
            {
                for (int x = 0; x < TerrainWidth;x+=20 )
                {
                    C = Convert.ToInt16((HeightData[x, y]-HeightMin) /200);
                    P = new Pen(Color.FromArgb(C,C,C),1);
                    GR.DrawRectangle(P,(int) x/20,(int)y/20,1,1); 
                }
            }
        }

       
        private void DrawTerrain(int Res,string PicName,ClsShader Pal)
        {
            PT = new Pen(Color.Turquoise, 1);
            GT = PicTerrain.CreateGraphics();

                Bitmap bmp = new Bitmap(PicTerrain.Image);
                GT = Graphics.FromImage(bmp);
            
            int CC;
            int HX, HY;
            
            ClsGlobal.HeightData = new float[200, 200];
            byte r,g,b;
            for (int _y = -200; _y < 200; _y += Res)
            {
                for (int _x = -200; _x < 200; _x += Res)
                {
                    HX = Convert.ToInt16(x * 29.59 + _x);
                    HY = Convert.ToInt16(y * 29.59 + _y);
                    int _a = (HeightData[HX, HY]) / 10;
                    int _b=Pal.GetMinHeightAbs();
                    CC = _a+_b;
                    r = Pal.ColorArr[CC].r;
                    g = Pal.ColorArr[CC].g;
                    b = Pal.ColorArr[CC].b;

                    System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(Color.FromArgb(r, g, b));
                    GT.FillRectangle(myBrush, (_x + 200) , (_y + 200) , Res, Res);
                }
            }
            PicTerrain.Image = bmp;
            if (PicName != "")
            {
                PicTerrain.Image.Save(PicName, ImageFormat.Jpeg);
            }
        }

        private void FrmAscMap_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            //LoadHeightDataAscFile2("isr50m-b.asc");
            PicBox1.Image = new Bitmap(JpgMap);
           
            LoadHeightDataAscFile2(MapFile );
           
                
            Application.DoEvents();
        }

        private void FrmAscMap_Load(object sender, EventArgs e)
        {
            P = new Pen(Color.Turquoise, 3);
            GR = PicBox1.CreateGraphics();
            //Pal1 = new ClsShader("Pallete01.clr",ClsShader.ShaderMode.NoSwipe);
            ClsGlobal.GPal.ThinLineEachLayersEvery(ClsGlobal.GContoursDivideEachLayerEvery,ClsGlobal.GContoursWidth,ClsGlobal.GContoursR,ClsGlobal.GContoursG,ClsGlobal.GContoursB);
            Pal1 = ClsGlobal.GPal;
            
            PalGS = Pal1;
            Application.DoEvents();
        }

        private void PicBox1_Click(object sender, EventArgs e)
        {
            //PicBox1.Image = Properties.Resources.isr50m_b2;
            Application.DoEvents();
            x = MousePosition.X - this.Left - PicBox1.Left-8;
            y = MousePosition.Y - this.Top - PicBox1.Top-95;
            GR.DrawRectangle(P,x-8,y-6, 20, 20);
            DrawTerrain(1,"",PalGS);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Tmr = !Tmr;
            if (Tmr == true)
            {
                GR.DrawRectangle(P, x - 10, y - 10, 20, 20);
                xt =ClsGlobal.XllCorner + ClsGlobal.GContoursDivideEachLayerEvery * x;
                yt = ClsGlobal.YllCorner + ClsGlobal.GContoursDivideEachLayerEvery * y;

                toolStripStatusLabel1.Text = "UTM (NAD83) - (" + xt.ToString("0.000") + "," + yt.ToString("0.000")+")";
            }
            else
            {
                //PicBox1.Image = Properties.Resources.isr50m_b2;
                PicBox1.Image = new Bitmap(JpgMap);
                Application.DoEvents();
            }
        }




        private void PicDown_MouseDown(object sender, MouseEventArgs e)
        {
            y += 1;
            DrawTerrain(1,"",PalGS);
        }

        private void PicLeft_MouseDown(object sender, MouseEventArgs e)
        {
            x -= 1;
            DrawTerrain(1,"",PalGS);
        }

        private void PicRight_MouseDown(object sender, MouseEventArgs e)
        {
            x += 1;
            DrawTerrain(1,"",PalGS);
        }


        private void PicUp_MouseDown(object sender, MouseEventArgs e)
        {
            y -= 1;
            DrawTerrain(1,"",PalGS);

        }

        private void BtnSideCut_Click(object sender, EventArgs e)
        {
            
            //PicTerrain.Image.Save("HeightMap_.Jpg", ImageFormat.Jpeg);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Stat _stat=GenerateDemAscFile("maps\\HeightMap.asc");
 
                PalGS = Pal1;
                DrawTerrain(1, "maps\\HeightMap.jpg", PalGS);
            

            FrmMap  FMap = new FrmMap();
            FMap.MdiParent = MdiParent;
            FMap.Show();
        }


        private Stat GenerateDemAscFile(string DemAscFile)
        {
            Stat _st;
            _st.max = -10000;
            _st.min = 10000;
            int HX, HY;
            string _t;
            //float _min=10000,_max=-10000;
            System.IO.StreamWriter _file = new System.IO.StreamWriter(DemAscFile);

            _file.WriteLine("ncols         200");
            _file.WriteLine("nrows         200");
            _file.WriteLine("xllcorner     "+xt.ToString("0.000"));
            _file.WriteLine("yllcorner     "+yt.ToString("0.000"));
            _file.WriteLine("cellsize      50.000");
            _file.WriteLine("NODATA_value  -9999");

            for (int _y = -100; _y < 100; _y++)
            {
                _t = "";
                for (int _x = -100; _x < 100; _x++)
                {
                    HX = Convert.ToInt16(x * 29.59 + _x);
                    HY = Convert.ToInt16(y * 29.59 + _y);
                    ClsGlobal.HeightData[_x + 100, _y + 100] = Convert.ToSingle(HeightData[HX, HY] / 10);
                    _t += ClsGlobal.HeightData[_x + 100, _y + 100];
                    if (_x != 99) _t += " ";

                    //find min/max
                    if (ClsGlobal.HeightData[_x + 100, _y + 100] >= _st.max) _st.max = ClsGlobal.HeightData[_x + 100, _y + 100];
                    if (ClsGlobal.HeightData[_x + 100, _y + 100] <= _st.min) _st.min = ClsGlobal.HeightData[_x + 100, _y + 100];
                }
                _file.WriteLine(_t);
            }

            _file.Close();

            
            string _buf = "Statistics:\r\nMin=" + _st.min.ToString() + "\r\nMax=" +  _st.max.ToString();
            System.IO.File.WriteAllText("maps\\HeightMap.txt", _buf);

            return _st;

        }

        private void CmbPallete_SelectedValueChanged(object sender, EventArgs e)
        {
            if (CmbPallete.Text == "Terrain") ClsGlobal.GPal = new ClsShader("Pal_Terrain.clr", ClsShader.ShaderMode.NoSwipe);
            if (CmbPallete.Text == "Greyscale and Red") ClsGlobal.GPal = new ClsShader("Pal_GreyRed.clr", ClsShader.ShaderMode.NoSwipe);
            ClsGlobal.GPal.ThinLineEachLayersEvery(ClsGlobal.GContoursDivideEachLayerEvery, ClsGlobal.GContoursWidth, ClsGlobal.GContoursR, ClsGlobal.GContoursG, ClsGlobal.GContoursB);
            Pal1 = ClsGlobal.GPal;
            PalGS = Pal1;
            DrawTerrain(1, "", PalGS);
        }


        //private void GenerateDemAscFile(string DemAscFile)
        //{
        //    int HX, HY;
        //    string _t;
        //    System.IO.StreamWriter _file = new System.IO.StreamWriter(DemAscFile);

        //    _file.WriteLine("ncols         400");
        //    _file.WriteLine("nrows         400");
        //    _file.WriteLine("xllcorner     120000.188");
        //    _file.WriteLine("yllcorner     1310025.188");
        //    _file.WriteLine("cellsize      50.000");
        //    _file.WriteLine("NODATA_value  -9999");

        //    for (int _y = -200; _y < 200; _y++)
        //    {
        //        _t = "";
        //        for (int _x = -100; _x < 100; _x++)
        //        {
        //            HX = Convert.ToInt16(x * 29.59 + _x);
        //            HY = Convert.ToInt16(y * 29.59 + _y);
        //            ClsGlobal.HeightData[_x + 100, _y + 100] = Convert.ToSingle(HeightData[HX, HY] / 10);
        //            _t += ClsGlobal.HeightData[_x + 100, _y + 100];
        //            if (_x != 99) _t += " ";
        //        }
        //        _file.WriteLine(_t);
        //    }

        //    _file.Close();

        //}

    }
}
