﻿namespace TerrainOptimalCoverage
{
    partial class FrmMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMap));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSideCat = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonShowMap = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonShowMatrix = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBrush = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabelX = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabelY = new System.Windows.Forms.ToolStripLabel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TerrainOptimalCoverage.Properties.Resources.About;
            this.pictureBox1.Location = new System.Drawing.Point(353, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSideCat,
            this.toolStripButtonShowMap,
            this.toolStripButtonShowMatrix,
            this.toolStripButtonBrush,
            this.toolStripLabelX,
            this.toolStripLabelY});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(655, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSideCat
            // 
            this.toolStripSideCat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSideCat.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSideCat.Image")));
            this.toolStripSideCat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSideCat.Name = "toolStripSideCat";
            this.toolStripSideCat.Size = new System.Drawing.Size(23, 22);
            this.toolStripSideCat.Text = "SideCat";
            this.toolStripSideCat.Click += new System.EventHandler(this.toolStripSideCat_Click);
            // 
            // toolStripButtonShowMap
            // 
            this.toolStripButtonShowMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowMap.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowMap.Image")));
            this.toolStripButtonShowMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowMap.Name = "toolStripButtonShowMap";
            this.toolStripButtonShowMap.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonShowMap.Text = "ShowMap";
            this.toolStripButtonShowMap.Click += new System.EventHandler(this.toolStripButtonShowMapTransparent_Click);
            // 
            // toolStripButtonShowMatrix
            // 
            this.toolStripButtonShowMatrix.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowMatrix.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowMatrix.Image")));
            this.toolStripButtonShowMatrix.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowMatrix.Name = "toolStripButtonShowMatrix";
            this.toolStripButtonShowMatrix.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonShowMatrix.Text = "ShowMatrix";
            this.toolStripButtonShowMatrix.Click += new System.EventHandler(this.toolStripButtonShowMatrix_Click);
            // 
            // toolStripButtonBrush
            // 
            this.toolStripButtonBrush.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBrush.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBrush.Image")));
            this.toolStripButtonBrush.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBrush.Name = "toolStripButtonBrush";
            this.toolStripButtonBrush.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonBrush.Text = "Brush";
            this.toolStripButtonBrush.Click += new System.EventHandler(this.toolStripButtonBrush_Click);
            // 
            // toolStripLabelX
            // 
            this.toolStripLabelX.Name = "toolStripLabelX";
            this.toolStripLabelX.Size = new System.Drawing.Size(15, 22);
            this.toolStripLabelX.Text = "x:";
            // 
            // toolStripLabelY
            // 
            this.toolStripLabelY.Name = "toolStripLabelY";
            this.toolStripLabelY.Size = new System.Drawing.Size(16, 22);
            this.toolStripLabelY.Text = "y:";
            // 
            // chart1
            // 
            this.chart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.BottomLeft;
            this.chart1.BorderlineWidth = 2;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(534, 42);
            this.chart1.Name = "chart1";
            this.chart1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(75, 103);
            this.chart1.TabIndex = 7;
            this.chart1.Text = "chart1";
            // 
            // FrmMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 421);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMap";
            this.Text = "FrmMap";
            this.Load += new System.EventHandler(this.FrmMap_Load);
            this.LocationChanged += new System.EventHandler(this.FrmMap_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripSideCat;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowMap;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowMatrix;
        private System.Windows.Forms.ToolStripButton toolStripButtonBrush;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ToolStripLabel toolStripLabelX;
        private System.Windows.Forms.ToolStripLabel toolStripLabelY;
    }
}