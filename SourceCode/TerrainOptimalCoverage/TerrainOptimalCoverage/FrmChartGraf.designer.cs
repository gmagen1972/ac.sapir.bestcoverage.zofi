﻿namespace TerrainOptimalCoverage
{
    partial class FrmChartGraf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartSideCut = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chartSideCut)).BeginInit();
            this.SuspendLayout();
            // 
            // chartSideCut
            // 
            chartArea1.Name = "ChartArea1";
            this.chartSideCut.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartSideCut.Legends.Add(legend1);
            this.chartSideCut.Location = new System.Drawing.Point(54, 52);
            this.chartSideCut.Name = "chartSideCut";
            this.chartSideCut.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "  ";
            this.chartSideCut.Series.Add(series1);
            this.chartSideCut.Size = new System.Drawing.Size(300, 300);
            this.chartSideCut.TabIndex = 1;
            this.chartSideCut.Text = "chart1";
            // 
            // FrmChartGraf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 385);
            this.Controls.Add(this.chartSideCut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmChartGraf";
            this.Text = "SideCut";
            this.Load += new System.EventHandler(this.FrmChartGraf_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartSideCut)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartSideCut;


    }
}