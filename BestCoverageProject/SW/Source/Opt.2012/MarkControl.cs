//Nothing to see here, it's only a red square :D Personalize it (circles,cross, etc)

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Opt1._1
{
	
	public class MarkControl : UserControl
	{
		private Container components = null;

		public MarkControl()
		{		
			InitializeComponent();
			this.BackColor = Color.Black;	
		}

		public Point Center
		{
			get{return new Point(Location.X+4,Location.Y+4);}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			//
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		
		private void InitializeComponent()
		{
			// 
			// MarkControl
			// 
			this.Name = "MarkControl";
			this.Size = new System.Drawing.Size(8, 8);
			this.Load += new System.EventHandler(this.MarkControl_Load);

		}
		#endregion

		private void MarkControl_Load(object sender, System.EventArgs e)
		{
			
		
		}
	}
}
