﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace TerrainOptimalCoverage
{
    public partial class FrmChartGraf : Form
    {
        public FrmChartGraf(float[] AscArraySideCat)
        {
            InitializeComponent();
            chartSideCut.Top = 0;
            chartSideCut.Left = 0;
           

            chartSideCut.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            chartSideCut.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height);

            //System.Windows.Forms.DataVisualization.Charting.Series series = new System.Windows.Forms.DataVisualization.Charting.Series();
            //series.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Grayscale;
           


            chartSideCut.Series[0].Name = "SideCut";
            chartSideCut.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.SplineArea;
            chartSideCut.Series[0].Color = Color.ForestGreen;
            chartSideCut.ChartAreas[0].Area3DStyle.Enable3D = true; 
              

            for (int i = 0; i < AscArraySideCat.Length; i++)
            {
                chartSideCut.Series[0].Points.AddXY(0,AscArraySideCat[i]);
               
                //series = chartSideCut.Series.Add(i.ToString());
                //series.Points.Add(AscArraySideCat[i]);
                //series.ToolTip = AscArraySideCat[i].ToString();
                //series.IsVisibleInLegend = false;

                
              
            }
        
        }



        private void FrmChartGraf_Load(object sender, EventArgs e)
        {

        }
    }
}
