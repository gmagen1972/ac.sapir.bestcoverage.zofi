﻿namespace TerrainOptimalCoverage
{
    partial class FrmMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMap));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSideCat = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonShowMatrix = new System.Windows.Forms.ToolStripButton();
            this.toolStripShowPointsOnLine = new System.Windows.Forms.ToolStripButton();
            this.toolStripVisibility = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAddTargetandSource = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxTargetObserv = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxSourceObserv = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxReches = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButtonClearMap = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonShowElementsOnRaduis = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.selectSingleFirearmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectMultipleFirearmsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.manageFirearmsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToHeightMapPngToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMountainONLYToHeigthMapPngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawTargetFirearmPositionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.setRouteDistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.SetAttackLocation2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.createRouteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analysisRouteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemAnalysisPointOnRoute1 = new System.Windows.Forms.ToolStripMenuItem();
            this.endRouteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.createSideCutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelCord = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelllcorner = new System.Windows.Forms.ToolStripStatusLabel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.demoOfGettingFirearmsDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectMultiplyFirearmsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.manageFirearmsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToHeightMapPngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMountainToJpgToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.setTrafficRouteDistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtBoxSetDisTrafficRoute = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.SetAttackLocation1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.selectTrafficRouteEndToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.endTrafficRouteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemAnalysisPointOnRoute2 = new System.Windows.Forms.ToolStripMenuItem();
            this.endTrafficRouteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.createSideCutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createSideCutWithDistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setDistanceSideCutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxSideCutDistance = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ContextMenuStrip = this.contextMenuStrip1;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSideCat,
            this.toolStripButtonShowMatrix,
            this.toolStripShowPointsOnLine,
            this.toolStripVisibility,
            this.toolStripButtonAddTargetandSource,
            this.toolStripLabel2,
            this.toolStripTextBoxTargetObserv,
            this.toolStripLabel3,
            this.toolStripTextBoxSourceObserv,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripLabel1,
            this.toolStripTextBoxReches,
            this.toolStripButtonClearMap,
            this.toolStripButtonShowElementsOnRaduis,
            this.toolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(655, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSideCat
            // 
            this.toolStripSideCat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSideCat.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSideCat.Image")));
            this.toolStripSideCat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSideCat.Name = "toolStripSideCat";
            this.toolStripSideCat.Size = new System.Drawing.Size(23, 22);
            this.toolStripSideCat.Text = "SideCut";
            this.toolStripSideCat.Click += new System.EventHandler(this.toolStripSideCut_Click);
            // 
            // toolStripButtonShowMatrix
            // 
            this.toolStripButtonShowMatrix.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowMatrix.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowMatrix.Image")));
            this.toolStripButtonShowMatrix.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowMatrix.Name = "toolStripButtonShowMatrix";
            this.toolStripButtonShowMatrix.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonShowMatrix.Text = "ShowMatrix";
            this.toolStripButtonShowMatrix.Click += new System.EventHandler(this.toolStripButtonShowMatrix_Click);
            // 
            // toolStripShowPointsOnLine
            // 
            this.toolStripShowPointsOnLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripShowPointsOnLine.Image = ((System.Drawing.Image)(resources.GetObject("toolStripShowPointsOnLine.Image")));
            this.toolStripShowPointsOnLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripShowPointsOnLine.Name = "toolStripShowPointsOnLine";
            this.toolStripShowPointsOnLine.Size = new System.Drawing.Size(23, 22);
            this.toolStripShowPointsOnLine.Text = "ShowPointsOnLine";
            this.toolStripShowPointsOnLine.Click += new System.EventHandler(this.toolStripShowPointsOnLine_Click);
            // 
            // toolStripVisibility
            // 
            this.toolStripVisibility.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripVisibility.Image = ((System.Drawing.Image)(resources.GetObject("toolStripVisibility.Image")));
            this.toolStripVisibility.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripVisibility.Name = "toolStripVisibility";
            this.toolStripVisibility.Size = new System.Drawing.Size(23, 22);
            this.toolStripVisibility.Text = "Visibility";
            this.toolStripVisibility.Click += new System.EventHandler(this.toolStripVisibility_Click);
            // 
            // toolStripButtonAddTargetandSource
            // 
            this.toolStripButtonAddTargetandSource.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddTargetandSource.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddTargetandSource.Image")));
            this.toolStripButtonAddTargetandSource.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddTargetandSource.Name = "toolStripButtonAddTargetandSource";
            this.toolStripButtonAddTargetandSource.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAddTargetandSource.Text = "Set_Observ";
            this.toolStripButtonAddTargetandSource.Click += new System.EventHandler(this.toolStripButtonAddTargetandSource_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel2.Text = "Target:";
            // 
            // toolStripTextBoxTargetObserv
            // 
            this.toolStripTextBoxTargetObserv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBoxTargetObserv.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBoxTargetObserv.ForeColor = System.Drawing.SystemColors.WindowText;
            this.toolStripTextBoxTargetObserv.Name = "toolStripTextBoxTargetObserv";
            this.toolStripTextBoxTargetObserv.Size = new System.Drawing.Size(30, 25);
            this.toolStripTextBoxTargetObserv.Text = "1";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(46, 22);
            this.toolStripLabel3.Text = "Source:";
            // 
            // toolStripTextBoxSourceObserv
            // 
            this.toolStripTextBoxSourceObserv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBoxSourceObserv.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBoxSourceObserv.Name = "toolStripTextBoxSourceObserv";
            this.toolStripTextBoxSourceObserv.Size = new System.Drawing.Size(30, 25);
            this.toolStripTextBoxSourceObserv.Text = "1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "ShowRadius";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "3D";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.ToolTipText = "Mountain";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "toolStripButton4";
            this.toolStripButton4.ToolTipText = "HeatMap";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(62, 22);
            this.toolStripLabel1.Text = "SetMount:";
            // 
            // toolStripTextBoxReches
            // 
            this.toolStripTextBoxReches.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBoxReches.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBoxReches.Name = "toolStripTextBoxReches";
            this.toolStripTextBoxReches.Size = new System.Drawing.Size(28, 25);
            this.toolStripTextBoxReches.Text = "0";
            // 
            // toolStripButtonClearMap
            // 
            this.toolStripButtonClearMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClearMap.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClearMap.Image")));
            this.toolStripButtonClearMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClearMap.Name = "toolStripButtonClearMap";
            this.toolStripButtonClearMap.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonClearMap.Text = "toolStripButton5";
            this.toolStripButtonClearMap.ToolTipText = "ClearMap";
            this.toolStripButtonClearMap.Click += new System.EventHandler(this.toolStripButtonClearMap_Click);
            // 
            // toolStripButtonShowElementsOnRaduis
            // 
            this.toolStripButtonShowElementsOnRaduis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowElementsOnRaduis.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowElementsOnRaduis.Image")));
            this.toolStripButtonShowElementsOnRaduis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowElementsOnRaduis.Name = "toolStripButtonShowElementsOnRaduis";
            this.toolStripButtonShowElementsOnRaduis.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonShowElementsOnRaduis.Text = "ShowElementsOnRaduis";
            this.toolStripButtonShowElementsOnRaduis.Click += new System.EventHandler(this.toolStripButtonShowElementsOnRaduis_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectSingleFirearmToolStripMenuItem,
            this.selectMultipleFirearmsToolStripMenuItem,
            this.toolStripSeparator1,
            this.manageFirearmsToolStripMenuItem1,
            this.toolStripSeparator2,
            this.exportToHeightMapPngToolStripMenuItem1,
            this.exportMountainONLYToHeigthMapPngToolStripMenuItem,
            this.drawTargetFirearmPositionToolStripMenuItem,
            this.toolStripSeparator3,
            this.setRouteDistanceToolStripMenuItem,
            this.toolStripMenuItem5,
            this.toolStripSeparator8,
            this.createRouteToolStripMenuItem,
            this.analysisRouteToolStripMenuItem,
            this.toolStripSeparator9,
            this.toolStripMenuItemAnalysisPointOnRoute1,
            this.endRouteToolStripMenuItem,
            this.toolStripSeparator5,
            this.createSideCutToolStripMenuItem1});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(49, 22);
            this.toolStripDropDownButton1.Text = "Tools";
            // 
            // selectSingleFirearmToolStripMenuItem
            // 
            this.selectSingleFirearmToolStripMenuItem.Name = "selectSingleFirearmToolStripMenuItem";
            this.selectSingleFirearmToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.selectSingleFirearmToolStripMenuItem.Text = "Select Single Firearm";
            this.selectSingleFirearmToolStripMenuItem.Click += new System.EventHandler(this.selectSingleFirearmToolStripMenuItem_Click);
            // 
            // selectMultipleFirearmsToolStripMenuItem
            // 
            this.selectMultipleFirearmsToolStripMenuItem.Name = "selectMultipleFirearmsToolStripMenuItem";
            this.selectMultipleFirearmsToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.selectMultipleFirearmsToolStripMenuItem.Text = "Select Multiple Firearms";
            this.selectMultipleFirearmsToolStripMenuItem.Click += new System.EventHandler(this.selectMultipleFirearmsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(294, 6);
            // 
            // manageFirearmsToolStripMenuItem1
            // 
            this.manageFirearmsToolStripMenuItem1.Name = "manageFirearmsToolStripMenuItem1";
            this.manageFirearmsToolStripMenuItem1.Size = new System.Drawing.Size(297, 22);
            this.manageFirearmsToolStripMenuItem1.Text = "Manage Firearms";
            this.manageFirearmsToolStripMenuItem1.Click += new System.EventHandler(this.manageFirearmsToolStripMenuItem1_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(294, 6);
            // 
            // exportToHeightMapPngToolStripMenuItem1
            // 
            this.exportToHeightMapPngToolStripMenuItem1.Name = "exportToHeightMapPngToolStripMenuItem1";
            this.exportToHeightMapPngToolStripMenuItem1.Size = new System.Drawing.Size(297, 22);
            this.exportToHeightMapPngToolStripMenuItem1.Text = "Export to HeightMap.Png";
            this.exportToHeightMapPngToolStripMenuItem1.Click += new System.EventHandler(this.exportToHeightMapPngToolStripMenuItem1_Click);
            // 
            // exportMountainONLYToHeigthMapPngToolStripMenuItem
            // 
            this.exportMountainONLYToHeigthMapPngToolStripMenuItem.Name = "exportMountainONLYToHeigthMapPngToolStripMenuItem";
            this.exportMountainONLYToHeigthMapPngToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.exportMountainONLYToHeigthMapPngToolStripMenuItem.Text = "Export Mountain ONLY to HeigthMap.Png";
            this.exportMountainONLYToHeigthMapPngToolStripMenuItem.Click += new System.EventHandler(this.exportMountainONLYToHeigthMapPngToolStripMenuItem_Click);
            // 
            // drawTargetFirearmPositionToolStripMenuItem
            // 
            this.drawTargetFirearmPositionToolStripMenuItem.Name = "drawTargetFirearmPositionToolStripMenuItem";
            this.drawTargetFirearmPositionToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.drawTargetFirearmPositionToolStripMenuItem.Text = "Draw Target Firearm position";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(294, 6);
            // 
            // setRouteDistanceToolStripMenuItem
            // 
            this.setRouteDistanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.setRouteDistanceToolStripMenuItem.Name = "setRouteDistanceToolStripMenuItem";
            this.setRouteDistanceToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.setRouteDistanceToolStripMenuItem.Text = "Set Route Distance";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "100";
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SetAttackLocation2});
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(297, 22);
            this.toolStripMenuItem5.Text = "Set Attack";
            // 
            // SetAttackLocation2
            // 
            this.SetAttackLocation2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.SetAttackLocation2.Name = "SetAttackLocation2";
            this.SetAttackLocation2.Size = new System.Drawing.Size(100, 23);
            this.SetAttackLocation2.Text = "20";
            this.SetAttackLocation2.TextChanged += new System.EventHandler(this.SetAttackLocation2_TextChanged);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(294, 6);
            // 
            // createRouteToolStripMenuItem
            // 
            this.createRouteToolStripMenuItem.Name = "createRouteToolStripMenuItem";
            this.createRouteToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.createRouteToolStripMenuItem.Text = "Create Route";
            this.createRouteToolStripMenuItem.Click += new System.EventHandler(this.createRouteToolStripMenuItem_Click);
            // 
            // analysisRouteToolStripMenuItem
            // 
            this.analysisRouteToolStripMenuItem.Name = "analysisRouteToolStripMenuItem";
            this.analysisRouteToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.analysisRouteToolStripMenuItem.Text = "Analysis Route";
            this.analysisRouteToolStripMenuItem.Click += new System.EventHandler(this.analysisRouteToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(294, 6);
            // 
            // toolStripMenuItemAnalysisPointOnRoute1
            // 
            this.toolStripMenuItemAnalysisPointOnRoute1.Name = "toolStripMenuItemAnalysisPointOnRoute1";
            this.toolStripMenuItemAnalysisPointOnRoute1.Size = new System.Drawing.Size(297, 22);
            this.toolStripMenuItemAnalysisPointOnRoute1.Text = "Analysis Point On Route";
            this.toolStripMenuItemAnalysisPointOnRoute1.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // endRouteToolStripMenuItem
            // 
            this.endRouteToolStripMenuItem.Name = "endRouteToolStripMenuItem";
            this.endRouteToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.endRouteToolStripMenuItem.Text = "End Route";
            this.endRouteToolStripMenuItem.Click += new System.EventHandler(this.endRouteToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(294, 6);
            // 
            // createSideCutToolStripMenuItem1
            // 
            this.createSideCutToolStripMenuItem1.Name = "createSideCutToolStripMenuItem1";
            this.createSideCutToolStripMenuItem1.Size = new System.Drawing.Size(297, 22);
            this.createSideCutToolStripMenuItem1.Text = "Create SideCut";
            this.createSideCutToolStripMenuItem1.Click += new System.EventHandler(this.createSideCutToolStripMenuItem1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(13, 58);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(124, 349);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(399, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabelCord,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabelllcorner});
            this.statusStrip1.Location = new System.Drawing.Point(0, 399);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(655, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(150, 16);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(57, 17);
            this.toolStripStatusLabel1.Text = "Analyze...";
            this.toolStripStatusLabel1.Visible = false;
            // 
            // toolStripStatusLabelCord
            // 
            this.toolStripStatusLabelCord.Name = "toolStripStatusLabelCord";
            this.toolStripStatusLabelCord.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel2.Text = "             ";
            // 
            // toolStripStatusLabelllcorner
            // 
            this.toolStripStatusLabelllcorner.Name = "toolStripStatusLabelllcorner";
            this.toolStripStatusLabelllcorner.Size = new System.Drawing.Size(0, 17);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // demoOfGettingFirearmsDetailsToolStripMenuItem
            // 
            this.demoOfGettingFirearmsDetailsToolStripMenuItem.Name = "demoOfGettingFirearmsDetailsToolStripMenuItem";
            this.demoOfGettingFirearmsDetailsToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.demoOfGettingFirearmsDetailsToolStripMenuItem.Text = "Select Single Firearm";
            this.demoOfGettingFirearmsDetailsToolStripMenuItem.Click += new System.EventHandler(this.demoOfGettingFirearmsDetailsToolStripMenuItem_Click);
            // 
            // selectMultiplyFirearmsToolStripMenuItem
            // 
            this.selectMultiplyFirearmsToolStripMenuItem.Name = "selectMultiplyFirearmsToolStripMenuItem";
            this.selectMultiplyFirearmsToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.selectMultiplyFirearmsToolStripMenuItem.Text = "Select Multiple Firearms";
            this.selectMultiplyFirearmsToolStripMenuItem.Click += new System.EventHandler(this.selectMultiplyFirearmsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(294, 6);
            // 
            // manageFirearmsToolStripMenuItem
            // 
            this.manageFirearmsToolStripMenuItem.Name = "manageFirearmsToolStripMenuItem";
            this.manageFirearmsToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.manageFirearmsToolStripMenuItem.Text = "Manage Firearms";
            this.manageFirearmsToolStripMenuItem.Click += new System.EventHandler(this.manageFirearmsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(294, 6);
            // 
            // exportToHeightMapPngToolStripMenuItem
            // 
            this.exportToHeightMapPngToolStripMenuItem.Name = "exportToHeightMapPngToolStripMenuItem";
            this.exportToHeightMapPngToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.exportToHeightMapPngToolStripMenuItem.Text = "Save Picture";
            this.exportToHeightMapPngToolStripMenuItem.Click += new System.EventHandler(this.exportToHeightMapPngToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(297, 22);
            this.toolStripMenuItem6.Text = "Print Picture";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // exportMountainToJpgToolStripMenuItem
            // 
            this.exportMountainToJpgToolStripMenuItem.Name = "exportMountainToJpgToolStripMenuItem";
            this.exportMountainToJpgToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.exportMountainToJpgToolStripMenuItem.Text = "Export Mountain ONLY to HeigthMap.Png";
            this.exportMountainToJpgToolStripMenuItem.Visible = false;
            this.exportMountainToJpgToolStripMenuItem.Click += new System.EventHandler(this.exportMountainToJpgToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(294, 6);
            // 
            // setTrafficRouteDistanceToolStripMenuItem
            // 
            this.setTrafficRouteDistanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtBoxSetDisTrafficRoute});
            this.setTrafficRouteDistanceToolStripMenuItem.Name = "setTrafficRouteDistanceToolStripMenuItem";
            this.setTrafficRouteDistanceToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.setTrafficRouteDistanceToolStripMenuItem.Text = "Set Route Distance";
            // 
            // txtBoxSetDisTrafficRoute
            // 
            this.txtBoxSetDisTrafficRoute.Name = "txtBoxSetDisTrafficRoute";
            this.txtBoxSetDisTrafficRoute.Size = new System.Drawing.Size(100, 23);
            this.txtBoxSetDisTrafficRoute.Text = "100";
            this.txtBoxSetDisTrafficRoute.TextChanged += new System.EventHandler(this.txtBoxSetDisTrafficRoute_TextChanged);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SetAttackLocation1});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(297, 22);
            this.toolStripMenuItem4.Text = "Set Attack";
            // 
            // SetAttackLocation1
            // 
            this.SetAttackLocation1.Name = "SetAttackLocation1";
            this.SetAttackLocation1.Size = new System.Drawing.Size(100, 23);
            this.SetAttackLocation1.Text = "20";
            this.SetAttackLocation1.TextChanged += new System.EventHandler(this.SetAttackLocation1_TextChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(294, 6);
            // 
            // selectTrafficRouteEndToolStripMenuItem
            // 
            this.selectTrafficRouteEndToolStripMenuItem.Name = "selectTrafficRouteEndToolStripMenuItem";
            this.selectTrafficRouteEndToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.selectTrafficRouteEndToolStripMenuItem.Text = "Create Route";
            this.selectTrafficRouteEndToolStripMenuItem.Click += new System.EventHandler(this.selectTrafficRouteEndToolStripMenuItem_Click_1);
            // 
            // endTrafficRouteToolStripMenuItem
            // 
            this.endTrafficRouteToolStripMenuItem.Name = "endTrafficRouteToolStripMenuItem";
            this.endTrafficRouteToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.endTrafficRouteToolStripMenuItem.Text = "Analysis Route";
            this.endTrafficRouteToolStripMenuItem.Click += new System.EventHandler(this.ExaminTrafficRouteToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(294, 6);
            // 
            // toolStripMenuItemAnalysisPointOnRoute2
            // 
            this.toolStripMenuItemAnalysisPointOnRoute2.Name = "toolStripMenuItemAnalysisPointOnRoute2";
            this.toolStripMenuItemAnalysisPointOnRoute2.Size = new System.Drawing.Size(297, 22);
            this.toolStripMenuItemAnalysisPointOnRoute2.Text = "Analysis Point On Route";
            this.toolStripMenuItemAnalysisPointOnRoute2.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // endTrafficRouteToolStripMenuItem1
            // 
            this.endTrafficRouteToolStripMenuItem1.Name = "endTrafficRouteToolStripMenuItem1";
            this.endTrafficRouteToolStripMenuItem1.Size = new System.Drawing.Size(297, 22);
            this.endTrafficRouteToolStripMenuItem1.Text = "End Route";
            this.endTrafficRouteToolStripMenuItem1.Click += new System.EventHandler(this.endTrafficRouteToolStripMenuItem1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(294, 6);
            // 
            // createSideCutToolStripMenuItem
            // 
            this.createSideCutToolStripMenuItem.Name = "createSideCutToolStripMenuItem";
            this.createSideCutToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.createSideCutToolStripMenuItem.Text = "Create SideCut";
            this.createSideCutToolStripMenuItem.Click += new System.EventHandler(this.createSideCutToolStripMenuItem_Click);
            // 
            // createSideCutWithDistanceToolStripMenuItem
            // 
            this.createSideCutWithDistanceToolStripMenuItem.Name = "createSideCutWithDistanceToolStripMenuItem";
            this.createSideCutWithDistanceToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.createSideCutWithDistanceToolStripMenuItem.Text = "Create SideCut With Distance";
            this.createSideCutWithDistanceToolStripMenuItem.Click += new System.EventHandler(this.createSideCutWithDistanceToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.demoOfGettingFirearmsDetailsToolStripMenuItem,
            this.selectMultiplyFirearmsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.manageFirearmsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exportToHeightMapPngToolStripMenuItem,
            this.toolStripMenuItem6,
            this.exportMountainToJpgToolStripMenuItem,
            this.toolStripMenuItem3,
            this.setTrafficRouteDistanceToolStripMenuItem,
            this.toolStripMenuItem4,
            this.toolStripSeparator6,
            this.selectTrafficRouteEndToolStripMenuItem,
            this.endTrafficRouteToolStripMenuItem,
            this.toolStripSeparator7,
            this.toolStripMenuItemAnalysisPointOnRoute2,
            this.endTrafficRouteToolStripMenuItem1,
            this.toolStripSeparator4,
            this.createSideCutToolStripMenuItem,
            this.setDistanceSideCutToolStripMenuItem,
            this.createSideCutWithDistanceToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(298, 392);
            // 
            // setDistanceSideCutToolStripMenuItem
            // 
            this.setDistanceSideCutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBoxSideCutDistance});
            this.setDistanceSideCutToolStripMenuItem.Name = "setDistanceSideCutToolStripMenuItem";
            this.setDistanceSideCutToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.setDistanceSideCutToolStripMenuItem.Text = "Set Distance Side Cut";
            // 
            // toolStripTextBoxSideCutDistance
            // 
            this.toolStripTextBoxSideCutDistance.Name = "toolStripTextBoxSideCutDistance";
            this.toolStripTextBoxSideCutDistance.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBoxSideCutDistance.Text = "0";
            // 
            // FrmMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 421);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FrmMap";
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmMap_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripSideCat;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowMatrix;
        private System.Windows.Forms.ToolStripButton toolStripShowPointsOnLine;
        private System.Windows.Forms.ToolStripButton toolStripVisibility;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddTargetandSource;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxTargetObserv;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSourceObserv;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxReches;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowElementsOnRaduis;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem selectSingleFirearmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectMultipleFirearmsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageFirearmsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exportToHeightMapPngToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportMountainONLYToHeigthMapPngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawTargetFirearmPositionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem createRouteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setRouteDistanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analysisRouteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem endRouteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem createSideCutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButtonClearMap;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAnalysisPointOnRoute1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripTextBox SetAttackLocation2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelCord;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelllcorner;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem demoOfGettingFirearmsDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectMultiplyFirearmsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem manageFirearmsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportToHeightMapPngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem exportMountainToJpgToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem setTrafficRouteDistanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtBoxSetDisTrafficRoute;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripTextBox SetAttackLocation1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem selectTrafficRouteEndToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem endTrafficRouteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAnalysisPointOnRoute2;
        private System.Windows.Forms.ToolStripMenuItem endTrafficRouteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem createSideCutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDistanceSideCutToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSideCutDistance;
        private System.Windows.Forms.ToolStripMenuItem createSideCutWithDistanceToolStripMenuItem;
    }
}