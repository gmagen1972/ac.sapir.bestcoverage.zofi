﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class FrmMountain : Form
    {

        ClsGrafix Draw;
        ClsAscMap AscMap;
        ClsShader Pal;
        public string AscFileName;

        public FrmMountain()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = !pictureBox1.Visible;
        }

        private void FrmMountain_Load(object sender, EventArgs e)
        {
            
            AscMap = new ClsAscMap();
            AscMap.LoadHeightDataAscFile(AscFileName);

            //Pal = new ClsShader("Pal_GreyRed.clr");
            Pal = new ClsShader(Convert.ToInt16(AscMap.HeightMin-5),Convert.ToInt16( AscMap.HeightMax+5), 0, 0, 0, 255, 255, 255);

            Draw = new ClsGrafix(this.pictureBox2,1,AscMap.TerrainWidth,AscMap.TerrainHeight);
            pictureBox2.Width = AscMap.TerrainWidth;
            pictureBox2.Height=AscMap.TerrainHeight;
            pictureBox2.Top = 30;
            pictureBox2.Left = 1;
            this.Width = pictureBox2.Width+10;
            this.Height = pictureBox2.Height + 55;
            Application.DoEvents();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Draw.SavePic("HeightMap.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        private void FrmMountain_Shown(object sender, EventArgs e)
        {
            int c = 0;
            for (int x = 0; x < AscMap.TerrainWidth; x++)
            {
                c += 1;
                if (c == 30)
                {
                    Application.DoEvents();
                    c = 0;
                }
                int ci,r,g,b;
                for (int y = 0; y < AscMap.TerrainHeight; y++)
                {
                    //Draw.Rectangle(x, y, 1, 1, Color.Red, 1);

                    ci = Convert.ToInt16(AscMap.heightData[x, y]) - Pal.GetMinHeight();
                    r = Pal.ColorArr[ci].r;
                    g = Pal.ColorArr[ci].g;
                    b = Pal.ColorArr[ci].b;

                    System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(Color.FromArgb(r, g, b));
                    Draw.FillRectangle(myBrush, x, y,1, 1);
                }
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            AscMap.SaveRawFile("heightmap.txt"); 
        }

     
    }
}
