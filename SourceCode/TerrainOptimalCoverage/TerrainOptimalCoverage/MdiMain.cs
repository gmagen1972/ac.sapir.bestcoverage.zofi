﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerrainOptimalCoverage
{
    public partial class MdiMain : Form
    {
        private int childFormNumber = 0;
        
        public MdiMain()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
                      
            Load_Photo();
            FrmMap  FMap = new FrmMap();
            FMap.MdiParent = this;
            try
            {
                FMap.Show();
            }
            catch
            {

            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAbout FAbout = new FrmAbout();
            FAbout.MdiParent = this;
            FAbout.Show();

        }

        private void MdiMain_Load(object sender, EventArgs e)
        {
            if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
            {
                MessageBox.Show("TerrainOptimaCoverage.exe is Aleady loaded,\r\nCan't run exe Twice.\r\n\r\nExiting...");
                Application.Exit();
            }
           // System.Diagnostics.Process.Start("XNATerrainTexturing.exe");
            this.Text += " - " +ClsGlobal.VerNTimestamp;
        }

        private void Load_Photo1()
        {
            
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string[] StrFiles = new string[2];
            string[] FilesName = new string[] { "HeightMap.asc", "HeightMap.jpg" };
            int i;
            string destFile;
           // string str = "XNATerrainTexturing.exe";
            destFile = Application.StartupPath + "\\Maps";
            // Set filter options and filter index.
            openFileDialog1.Filter = "Photo (*.asc;*.jpg)|*.asc;*.jpg";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;
            openFileDialog1.ShowDialog();
            StrFiles = openFileDialog1.FileNames;
            try
            {
                i = 0;
                foreach (string s in StrFiles)
                {
                    // Use static Path methods to extract only the file name from the path.
                    //fileName = System.IO.Path.GetFileName(s);
                    //destFile = System.IO.Path.Combine(targetPath, fileName);
                    try
                    {
                        System.IO.File.Copy(s, destFile + "\\" + FilesName[i], true);
                        i++;
                    }
                    catch (System.IO.IOException err)
                    {
                        System.Windows.Forms.MessageBox.Show(err.Message);
                        return;
                    }
                    if (i == 2)
                    {
                        // System.Windows.Forms.MessageBox.Show("All Files have been uploaded successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // process1.StartInfo.FileName = str;
                        //process1.Start();
                    }
                }
            }
            catch
            {
                MessageBox.Show("There was a problem with files loading...");
            }
        }

        private void Load_Photo()
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string[] StrFiles = new string[2];
            string[] FilesNameAray = new string[] { "", "" };
            int i, len;
            string destFile;
            string strFileName;

            destFile = Application.StartupPath + "\\Maps";
            // Set filter options and filter index.
            openFileDialog1.Filter = "Photo (*.asc;*.jpg)|*.asc;*.jpg";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;
            openFileDialog1.ShowDialog();


            StrFiles[0] = openFileDialog1.FileName;
          try
            {
            if (StrFiles[0].EndsWith(".asc"))
            {
                len = StrFiles[0].Length;
                strFileName = StrFiles[0].Remove(len - 3);
                StrFiles[1] = strFileName + "jpg";
                FilesNameAray[0] = "HeightMap.asc";
                FilesNameAray[1] = "HeightMap.jpg";
            }
            else
            {
                len = StrFiles[0].Length;
                strFileName = StrFiles[0].Remove(len - 3);
                StrFiles[1] = strFileName + "asc";
                FilesNameAray[0] = "HeightMap.jpg";
                FilesNameAray[1] = "HeightMap.asc";
            }

            i = 0;
            foreach (string s in StrFiles)
            {
                // Use static Path methods to extract only the file name from the path.
                //fileName = System.IO.Path.GetFileName(s);
                //destFile = System.IO.Path.Combine(targetPath, fileName);
                try
                {
                    System.IO.File.Copy(s, destFile + "\\" + FilesNameAray[i], true);
                    i++;
                }
                catch (System.IO.IOException err)
                {
                    System.Windows.Forms.MessageBox.Show(err.Message);
                    return;
                }
                if (i == 2)
                {
                    // System.Windows.Forms.MessageBox.Show("All Files have been uploaded successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // process1.StartInfo.FileName = str;
                    //process1.Start();

                }
            }

            }
          catch
          {
              MessageBox.Show("There was a problem with files loading...");
          }
        }

       

        private void MnuFileOpenAsc_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Heightmap (*.asc)|*.asc";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;
            openFileDialog1.ShowDialog();

            string AscFile = openFileDialog1.FileName;
            if (AscFile != "")
            {
                if (System.IO.File.Exists(AscFile))
                {
                    FrmMountain FMountain = new FrmMountain();
                    FMountain.MdiParent = this;
                    FMountain.AscFileName = AscFile;
                    FMountain.Show();
                    
                }
            }
        }

        private void firearmsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFirearms FFirearms = new FrmFirearms();
            FFirearms.MdiParent = this;
            FFirearms.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Heightmap (*.asc)|*.asc";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;
            openFileDialog1.ShowDialog();

            string AscFile = openFileDialog1.FileName;
            if (AscFile != "")
            {
                if (System.IO.File.Exists(AscFile))
                {
                    FrmNav FNav = new FrmNav();
                    FNav.MdiParent = this;
                    FNav.AscFileName = AscFile;
                    FNav.Show();
                }
            }
        }

        private void loadTerrainMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm4Map FAscMap = new Frm4Map();
            FAscMap.MdiParent = this;
            FAscMap.MapFile = "isr50m-b.asc";
            FAscMap.JpgMap = "isr50m-b.bmp";
            ClsGlobal.GContoursDivideEachLayerEvery = 50;
            ClsGlobal.GContoursWidth = 5;
            ClsGlobal.XllCorner = 26282.747;
            ClsGlobal.YllCorner = 347638.463;
            FAscMap.Show();
        }

        private void terrainIsraelSouthNorth50mToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm4Map FAscMap = new Frm4Map();
            FAscMap.MdiParent = this;
            FAscMap.MapFile = "4-10m.asc";
            FAscMap.JpgMap = "4-100m.jpg";
            ClsGlobal.GContoursDivideEachLayerEvery = 10;
            ClsGlobal.GContoursWidth = 1;
            ClsGlobal.XllCorner = 616230.000;
            ClsGlobal.YllCorner = 3443108.000;
            
            FAscMap.Show();
        }

        private void terrainIsraelSouthSouth50mToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm4Map FAscMap = new Frm4Map();
            FAscMap.MdiParent = this;
            FAscMap.MapFile = "5-10m.asc";
            FAscMap.JpgMap = "5-100m.jpg";
            ClsGlobal.GContoursDivideEachLayerEvery = 10;
            ClsGlobal.GContoursWidth = 1;
            ClsGlobal.XllCorner = 617819.000;
            ClsGlobal.YllCorner = 3386698.000;
 
            FAscMap.Show();
        }

        private void manageContoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmContours FContours = new FrmContours();
            FContours.MdiParent = this;
            FContours.Show();
        }


    }
}
