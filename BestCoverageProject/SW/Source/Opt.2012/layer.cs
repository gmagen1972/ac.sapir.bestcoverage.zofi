using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Runtime.InteropServices;

namespace Opt1._1
{
	/// <summary>
	/// Summary description for layer.
	/// </summary>
	public class layer
	{		
		public bool fileLoaded;
		public bool visibilityCalculated=false;
		public string FileName;
		public float Max;
		public float Min;
		public bool [,]EnemyArray;
		public bool [,]aps;
		public int size;
		public int cHeight;
		public AreaMap theMap;
		private bool [,]visibilityMatrix;
		private bool[,]solutionMatrix;
		public ArrayList Solution=new ArrayList();
		public ArrayList astPoints=new ArrayList();
		public bool solved=false;
		public bool border=false;
		public Point[] borderArray;
		public int obNum=5;
		public int pointNum;
		public int[,]pList;
		public int count = 0;
		public String Load;
		public bool strategic=false;
		public int observerHeight=1;
		public int targetHeight=1;
		public bool catalog;
		public int xlcorner;
		public int ylcorner;
		public String[]observers;
		ContainerControl m_sender = null;
		int m_totalMessages = 0;
		Delegate m_senderDelegate = null;
		/// <summary>
		/// Constructor called by calle using ThreadPool OR ThreadStart
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="totalMessages"></param>
		/// <param name="sp"></param>
		public layer(ContainerControl sender, Delegate senderDelegate, int totalMessages)
		{
			m_sender = sender;
			m_senderDelegate = senderDelegate;
			m_totalMessages = totalMessages;
			visibilityCalculated=false;
			size=5;
			cHeight=10;
		}
		public void calculateVisibility()
		{
			bool s = true;
			try
			{
				visibilityMatrix = new bool[(theMap.Max_Y+1)*(theMap.Max_X+1),(theMap.Max_Y+1)*(theMap.Max_X+1)];
			}
			catch (Exception e)
			{
				MessageBox.Show("File Corrupted / too Big to fit in memory");
				s = false;
			}
			finally
			{
				if (s)
				{
					generateMatrix();
					visibilityCalculated=true;
				}
			}
			//Thread.CurrentThread.Abort();
		}
		public void CSV_Parse()
		{
			System.IO.StreamReader myReader = new System.IO.StreamReader(FileName);
			String data;
			int Cols=0;
			int Rows=0;
			Max=0;
			Min=0;
			ArrayList arr = new ArrayList();
			StreamReader reader = File.OpenText(FileName);
			try
			{				
				int Err=-1;
				int count=0;
				Load="";
				while ((data = reader.ReadLine())!=null)
				{
					if ((count<6)/*&&(catalog)*/)
					{
						count++;
						Load=Load+data;
						Load=Load+"	";
						if (count==3)
						{
							String[]xl=data.Split(' ');
							xlcorner=(int)Single.Parse(xl[5]);
						}
						if (count==4)
						{
							String[]yl=data.Split(' ');
							ylcorner=(int)Single.Parse(yl[5]);
						}
					}
					else
					{
						String[]dataArr=data.Split(' ');
						if (dataArr.Length>Cols)
						{
							Err++;
							Cols=dataArr.Length;
						}
						Rows++;
						arr.Add(dataArr);
					}
				}
				reader.Close();
				float [,]FloatArray=new float[Cols,Rows];
				EnemyArray=new bool [Cols,Rows];
				for (int i=0;i<Rows;i++)
				{
					String[]dataArr=(String[])arr[i];
					for (int j=0;j<Cols;j++)
					{
						FloatArray[j,i]=Single.Parse(dataArr[j]);
						if (FloatArray[j,i]==-9999.0)
						{
							FloatArray[j,i] = Min;
						}
						EnemyArray[j,i]=false;
						if (FloatArray[j,i]!=-9999.0)
						{
							Max=Math.Max(Max,FloatArray[j,i]);
							Min=Math.Min(Min,FloatArray[j,i]);
						}
					}
				}
				//theMap = new AreaMap(FloatArray,Cols-1,Rows-1,25);
                theMap = new AreaMap(FloatArray,Cols-1,Rows-1,75);
				if (Err>0) MessageBox.Show("Input file is not a N X N array , this might cause errors");
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
			finally
			{
				if (null != reader) reader.Close();
				fileLoaded=true;
			}
		}
		public Bitmap createMap(int mode)
		{
			if ((fileLoaded==true)&&(size>0))
			{
				Bitmap bmp = new Bitmap((theMap.Max_Y+1)*size,(theMap.Max_X+1)*size);
				Graphics g = Graphics.FromImage(bmp);
				for (int i=0;i<theMap.Max_Y+1;i++)
				{
					for (int j=0;j<theMap.Max_X+1;j++)
					{
						int s=(int)(255*(theMap.theMap[i,j]-Min)/(Max-Min));
						Color c = Color.FromArgb(s,s,s);
						if (mode==1)
						{
							c = Color.FromArgb(s,s,s);
						}
						else if ((mode==2)&&(cHeight>0))
						{
							int sc=(s/cHeight)*cHeight;	
							c = Color.FromArgb(sc,sc,sc);
						}
						SolidBrush b=new SolidBrush(c);
						g.FillRectangle(b,i*size,j*size,size,size);
					}
				}
				return bmp;
			}
			return null;
			
		}
		public Bitmap viewShed(int originY,int originX,Bitmap bmp)
		{
			//Bitmap bmp = new Bitmap((theMap.Max_Y+1)*size,(theMap.Max_X+1)*size);
			Graphics g = Graphics.FromImage(bmp);
			count=0;
			for (int i=0;i<(theMap.Max_Y+1);i++)
			{
				for (int j=0;j<(theMap.Max_X+1);j++)
				{
					SolidBrush b = new SolidBrush(Color.FromArgb(40,0,0,255));
					if (checkVisibility(originY,originX,i,j))
					{
						g.FillRectangle(b,i*size,j*size,size,size);
						count++;
					}
				}
			}
			return bmp;
		}
		public void generateMatrix()
		{
			Thread.CurrentThread.IsBackground = true;
			for(int i = 0; i <= theMap.Max_Y; i++)
			{
				if ((i%2)==0)
				{
					Thread.Sleep(1);
					m_totalMessages = (100*i)/theMap.Max_Y;
					m_sender.BeginInvoke( m_senderDelegate, new object[] { m_totalMessages, false } );
				}
				for(int j = 0; j <= theMap.Max_X; j++)
				{					
					calculatePositionVisibility(i , j);
				}				
			}
			m_sender.BeginInvoke( m_senderDelegate, new object[] { m_totalMessages, true } );
		}	
	
		public void calculatePositionVisibility(int y, int x)
		{
			for(int i = 0; i <= theMap.Max_Y;i++)
			{
				generateSideCut(y,x,i,0);
				generateSideCut(y,x,i,theMap.Max_X);
			}
			for(int i = 0; i <= theMap.Max_X;i++)
			{
				generateSideCut(y,x,0,i);
				generateSideCut(y,x,theMap.Max_Y,i);
			}
		
		
		}		
			
		public void generateSideCut(int Y_origin , int X_origin, int Y_target,int X_target)
		{
			int [,]sideCut;
			if (X_origin == X_target && Y_origin == Y_target)
			{
				visibilityMatrix[(theMap.Max_X+1)*Y_origin + X_origin,(theMap.Max_X+1)*Y_origin + X_origin] = true;
				return;
			}
		
			if (X_origin == X_target && Y_origin > Y_target)//upper line
			{
				sideCut=new int[Y_origin - Y_target +1,2];
				for (int i = 0; i <= Y_origin - Y_target; i++)
				{
					sideCut[i,0] = X_origin;
					sideCut[i,1] = Y_origin - i;
				}
				calculateVisibility (sideCut , Y_origin - Y_target);
				return;
			}
		
			if (X_origin == X_target && Y_origin < Y_target)//lower line
			{
				sideCut=new int[Y_target - Y_origin +1,2];
				for (int i = 0; i <= Y_target - Y_origin; i++)
				{
					sideCut[i,0] = X_origin;
					sideCut[i,1] = Y_origin + i;
				}
				calculateVisibility (sideCut , Y_target - Y_origin);
				return;
			}
		
			//here no division by Zero accures
			float a = (float)(Y_target-Y_origin)/(float)(X_target - X_origin); //calculate a
			float b = (float)Y_origin - a * (float)X_origin;
		
			if (X_origin < X_target && Math.Abs(a) < 1)//Right Triangle
			{
				sideCut=new int[X_target - X_origin + 1,2];
				for (int i = 0; i <= X_target - X_origin; i++)
				{
					sideCut[i,0] = X_origin + i;
					sideCut[i,1] = (int)Math.Round(a*(float)(X_origin+i)+b);
				}
				calculateVisibility (sideCut , X_target - X_origin);
				return;
			}
		
			if (X_origin > X_target && Math.Abs(a) < 1)//Left Triangle
			{
				sideCut=new int[X_origin - X_target + 1,2];
				for (int i = 0; i <= X_origin - X_target; i++)
				{
					sideCut[i,0] = X_origin - i;
					sideCut[i,1] = (int)Math.Round(a*(float)(X_origin-i)+b);
				}
				calculateVisibility (sideCut , X_origin - X_target);
				return;
			}
		
			if (Y_origin < Y_target && Math.Abs(a) >= 1)//lower Triangle
			{
				sideCut=new int[Y_target - Y_origin + 1,2];
				for (int i = 0; i <= Y_target - Y_origin; i++)
				{
					sideCut[i,0] = (int)Math.Round(( (float)(Y_origin+i) - b) / a) ;
					sideCut[i,1] = Y_origin + i;
				}
				calculateVisibility (sideCut , Y_target - Y_origin);
				return;
			}
		
			if (Y_origin > Y_target && Math.Abs(a) >= 1)//upper Triangle
			{
				sideCut=new int[Y_origin - Y_target + 1,2];
				for (int i = 0; i <= Y_origin - Y_target; i++)
				{
					sideCut[i,0] = (int)Math.Round(((float)(Y_origin-i) - b) / a) ;
					sideCut[i,1] = Y_origin - i;
				}
				calculateVisibility (sideCut , Y_origin - Y_target);
				return;
			}
		}
	
		public void calculateVisibility(int [,]sCut , int length)
		{//calculates the visible cells along the sidCut
			int lastVisible = 1;
			visibilityMatrix[sCut[0,1]*(theMap.Max_X+1) + sCut[0,0],sCut[0,1]*(theMap.Max_X+1) + sCut[0,0]] = true;
			visibilityMatrix[sCut[0,1]*(theMap.Max_X+1) + sCut[0,0],sCut[1,1]*(theMap.Max_X+1) + sCut[1,0]] = true;		
			for(int i = 2; i <= length; i++)
			{
				if( (theMap.cellSize * lastVisible)  * 
					(theMap.theMap[sCut[i,1],sCut[i,0]]-theMap.theMap[sCut[0,1],sCut[0,0]]-(observerHeight+targetHeight)) - 
					(theMap.theMap[sCut[lastVisible,1],sCut[lastVisible,0]]-theMap.theMap[sCut[0,1],sCut[0,0]]-(observerHeight+targetHeight)) * 
					(theMap.cellSize * i) >= 0)
				{
					visibilityMatrix[sCut[0,1]*(theMap.Max_X+1) + sCut[0,0],sCut[i,1]*(theMap.Max_X+1) + sCut[i,0]] = true;
					lastVisible = i;
				}
			}	
		}
		public bool checkVisibility(int originY,int originX,int sourceY,int sourceX)
		{
			int oIndex = originY*(theMap.Max_X+1) + originX;
			int sIndex = sourceY*(theMap.Max_X+1) + sourceX;
			if (this.visibilityMatrix[oIndex,sIndex])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public void createBorder(GraphicsPath path,Point[] p)
		{
			borderArray=p;
			border=true;
			for (int y=0;y<theMap.Max_Y;y++)
			{
				for (int x=0;x<theMap.Max_X;x++)
				{
					if (path.IsVisible(((float)x),((float)y)))
					{
						EnemyArray[y,x]=true;
					}
				}
			}
		}
		public void Solve()
		{
			Thread.CurrentThread.IsBackground = true;
			Solution = new ArrayList();
			m_totalMessages = 0;
			pointNum=(theMap.Max_X+1)*(theMap.Max_Y+1);
			solutionMatrix=visibilityMatrix;
			aps = new bool[theMap.Max_X+1,theMap.Max_Y+1];
			for (int y=0;y<theMap.Max_Y;y++)
			{
				for (int x=0;x<theMap.Max_X;x++)
				{
					aps[x,y]=false;
				}
			}
			for (int i=0;i<astPoints.Count;i++)
			{
				Point current =(Point)astPoints[i];
				aps[current.Y,current.X]=true;
			}
			pList = new int[pointNum,3];
			observers = new String[obNum];
			for (int i=0;i<obNum;i++)
			{	
				m_totalMessages = (i*100)/obNum;
				vSum(i);				
			}
			solved=true;
			m_sender.BeginInvoke( m_senderDelegate, new object[] { 100, true } );
		}
		public void vSum(int ii)
		{
			int max=0;
			Point p = new Point(0,0);
			bool valid=false;
			for (int i=0;i<pointNum;i++)
			{
				if ((i%100)==0)
				{
					Thread.Sleep(1);
					int temp = (i*10)/(2*(pointNum));
					m_sender.BeginInvoke( m_senderDelegate, new object[] { m_totalMessages + temp, false } );
				}
				int sum=0;
				int x = i/(theMap.Max_X+1);
				int y = i%(theMap.Max_X+1);
				if ((EnemyArray[y,x])||(border==false))
				{
					for (int j=0;j<pointNum;j++)
					{
						if (solutionMatrix[i,j])
						{
							sum++;
							int xx = j/(theMap.Max_X+1);
							int yy = j%(theMap.Max_X+1);
							if (aps[yy,xx])
							{
								sum=sum+pointNum;
							}
						}

					}
				}
				if (sum>0)
				{
					pList[i,0]=sum;
					pList[i,1]=i/(theMap.Max_X+1);
					pList[i,2]=i%(theMap.Max_X+1);
					if (pList[i,0]>max)
					{
						max=pList[i,0];
						p = new Point(pList[i,1],pList[i,2]);
						valid=true;
					}
					
				}
			}
			if (valid)
			{
				Solution.Add(p);
				String ppp = (xlcorner+p.X).ToString()+","+(ylcorner+p.Y).ToString();
				if (observers!=null)
				{
					observers[ii]=ppp;
				}
				clearPoint(p.X,p.Y);
			}
		}

		public void clearPoint(int y, int x)
		{
			int index=y*(theMap.Max_X+1)+x;
			for (int i=0;i<pointNum;i++)
			{
				if ((i%100)==0)
				{
					Thread.Sleep(1);
					int temp = 5+(i*10)/(2*(pointNum));
					m_sender.BeginInvoke( m_senderDelegate, new object[] { m_totalMessages + temp, false } );
				}
				if(solutionMatrix[index,i])
				{
					for (int j=0;j<pointNum;j++)
					{
						solutionMatrix[j,i]=false;
					}
				}
				solutionMatrix[index,i]=false;
			}
		}
		public Bitmap viewSolution(Image original)
		{
			Point[]sizedBorder = new Point[5];
			Bitmap bmp = new Bitmap(original);
			Graphics g = Graphics.FromImage(bmp);
			for (int i=0;i<Solution.Count;i++)
			{
				Color c = Color.Blue;
				SolidBrush b = new SolidBrush(c);
				Pen p = new Pen(c,size);
				Point center = (Point)Solution[i];
				center.X=center.X*size;
				center.Y=center.Y*size;
				Point[]triangle=new Point[3];
				triangle[0] = new Point(center.X,center.Y-size*2);
				triangle[1] = new Point(center.X+size*2,center.Y+size*2);
				triangle[2] = new Point(center.X-size*2,center.Y+size*2);
				g.FillPolygon(b,triangle);
			}
			if (strategic)
			{
				for (int i=0;i<astPoints.Count;i++)
				{
					Point current =(Point)astPoints[i];
					int x=current.X*size;
					int y=current.Y*size;
					Color c = Color.Blue;
					Pen p = new Pen(c,size/2);				
					Point up1 = new Point (x,y-size); 
					Point up3 = new Point(x,y-3*size);
					Point down1 = new Point(x,y+size);
					Point down3 = new Point(x,y+3*size);
					Point left1 = new Point (x-size,y); 
					Point left3 = new Point(x-3*size,y);
					Point right1 = new Point(x+size,y);
					Point right3 = new Point(x+3*size,y);
					g.DrawLine(p,up1,up3);
					g.DrawLine(p,down1,down3);
					g.DrawLine(p,left1,left3);
					g.DrawLine(p,right1,right3);
				}
			}
			if (border)
			{
				for (int i=0;i<5;i++)
				{
					sizedBorder[i].X=(borderArray[i].X)*size;
					sizedBorder[i].Y=(borderArray[i].Y)*size;
				}
				Brush r=new SolidBrush(Color.Red);
				Brush b=new SolidBrush(Color.LightBlue);
				Pen myPen = new Pen(r,10f);
				Pen myPen2 = new Pen(b,10f);
				float[]ar = new float[4];
				ar[0] = 0.0f;
				ar[1] = 0.4f;
				ar[2] = 1f;
				ar[3] = 1f;
				myPen.CompoundArray=ar;
				float[]ar2 = new float[4];
				ar2[0] = 0.0f;
				ar2[1] = 0.0f;
				ar2[2] = 0.6f;
				ar2[3] = 1f;
				myPen2.CompoundArray=ar2;
				g.DrawClosedCurve(myPen, sizedBorder);
				g.DrawClosedCurve(myPen2,sizedBorder);			
				SolidBrush bsh = new SolidBrush( Color.FromArgb(40,0,100,255) );
				g.FillClosedCurve(bsh,sizedBorder,System.Drawing.Drawing2D.FillMode.Alternate);
			}
			return bmp;
		}

		public void AddAstPoint(Point p)
		{
			astPoints.Add(p);
		}
		unsafe public Bitmap create8bpp()
		{
			if ((fileLoaded==true)&&(size>0))
			{
				Bitmap bmp = new Bitmap((theMap.Max_Y+1),(theMap.Max_X+1),PixelFormat.Format8bppIndexed);
				Rectangle rec= new Rectangle(0,0,bmp.Width,bmp.Height);
				BitmapData bmd  = new BitmapData();
				bmd = bmp.LockBits(rec , ImageLockMode.ReadWrite, bmp.PixelFormat);
				ColorPalette gs = bmp.Palette;
				for(int i = 0; i < 256; i++)
				{
					gs.Entries[i] = Color.FromArgb( 255, i, i, i );
				}
				bmp.Palette = gs;
				for (int i=0;i<theMap.Max_Y+1;i++)
				{
					for (int j=0;j<theMap.Max_X+1;j++)
					{
						Byte* p = (Byte*)bmd.Scan0.ToPointer();
						int offset=j*bmd.Stride+i;
						int s=(int)(255*(theMap.theMap[i,j]-Min)/(Max-Min));
						p[offset] = (Byte)s;
					}
				}
				bmp.UnlockBits(bmd);
				return bmp;
			}
			return null;	
			
		}

	}


}
