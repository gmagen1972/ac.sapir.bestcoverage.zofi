﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerrainOptimalCoverage
{
    public static class ClsGlobal
    {
        public static float[,] HeightData;
        public static string SelFireArm;
        public static string VerNTimestamp = "v"+System.Windows.Forms.Application.ProductVersion+" - 28/May/2013 22:32";
        public static string[,] FireArmTable;
        public static DataSet2TableAdapters.FirearmsTableAdapter Ds = new DataSet2TableAdapters.FirearmsTableAdapter();
        public static ClsShader GPal = new ClsShader("Pal_Terrain.clr", ClsShader.ShaderMode.NoSwipe);
        public static byte GContoursWidth=5;
        public static byte GContoursDivideEachLayerEvery=70;
        public static byte GContoursR=0;
        public static byte GContoursG=0;
        public static byte GContoursB=0;
        public static double XllCorner = 202.202;
        public static double YllCorner = 101.101;

        public static void LoadFireArmsTable()
        {
            int Size = Convert.ToInt16(Ds.GetCount());
            ClsGlobal.FireArmTable = new string[Size, 6];

            for (int x = 0; x < Size; x++)
            {
                ClsGlobal.FireArmTable[x, 0] = Ds.GetData()[x].FName;
                ClsGlobal.FireArmTable[x, 1] = Ds.GetData()[x].MinRange;
                ClsGlobal.FireArmTable[x, 2] = Ds.GetData()[x].MaxRange;
                ClsGlobal.FireArmTable[x, 3] = Ds.GetData()[x].Color;
                ClsGlobal.FireArmTable[x, 4] = Ds.GetData()[x].Height; ;
            }
            System.Diagnostics.Debug.Print( ClsGlobal.FireArmTable[2, 5]);
        }
        //private int _width, _height;

        
        //public ClsGlobal()
        //{
        //    HeightData = new int[0,0];
        //}

        //public int GetWidth()
        //{
        //    return _width;
        //}

        //public void SetWidth(int W)
        //{
        //    _width=W;
        //}

        //public int GetHeight()
        //{
        //    return _height;
        //}

        //public void SetHeight(int H)
        //{
        //    _height = H;
        //}

    }
}
