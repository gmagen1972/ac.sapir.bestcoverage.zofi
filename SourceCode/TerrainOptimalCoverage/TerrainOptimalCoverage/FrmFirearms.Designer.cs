﻿namespace TerrainOptimalCoverage
{
    partial class FrmFirearms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fNameLabel;
            System.Windows.Forms.Label minRangeLabel;
            System.Windows.Forms.Label maxRangeLabel;
            System.Windows.Forms.Label colorLabel;
            System.Windows.Forms.Label iDLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFirearms));
            System.Windows.Forms.Label heightLabel;
            this.dataSet2 = new TerrainOptimalCoverage.DataSet2();
            this.firearmsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.firearmsTableAdapter = new TerrainOptimalCoverage.DataSet2TableAdapters.FirearmsTableAdapter();
            this.tableAdapterManager = new TerrainOptimalCoverage.DataSet2TableAdapters.TableAdapterManager();
            this.firearmsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.firearmsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.firearmsDataGridView = new System.Windows.Forms.DataGridView();
            this.fNameTextBox = new System.Windows.Forms.TextBox();
            this.minRangeTextBox = new System.Windows.Forms.TextBox();
            this.maxRangeTextBox = new System.Windows.Forms.TextBox();
            this.colorTextBox = new System.Windows.Forms.TextBox();
            this.iDLabel1 = new System.Windows.Forms.Label();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Height = new System.Windows.Forms.DataGridViewTextBoxColumn();
            fNameLabel = new System.Windows.Forms.Label();
            minRangeLabel = new System.Windows.Forms.Label();
            maxRangeLabel = new System.Windows.Forms.Label();
            colorLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            heightLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firearmsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firearmsBindingNavigator)).BeginInit();
            this.firearmsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firearmsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // fNameLabel
            // 
            fNameLabel.AutoSize = true;
            fNameLabel.Location = new System.Drawing.Point(16, 67);
            fNameLabel.Name = "fNameLabel";
            fNameLabel.Size = new System.Drawing.Size(38, 13);
            fNameLabel.TabIndex = 4;
            fNameLabel.Text = "Name:";
            // 
            // minRangeLabel
            // 
            minRangeLabel.AutoSize = true;
            minRangeLabel.Location = new System.Drawing.Point(16, 93);
            minRangeLabel.Name = "minRangeLabel";
            minRangeLabel.Size = new System.Drawing.Size(62, 13);
            minRangeLabel.TabIndex = 6;
            minRangeLabel.Text = "Min Range:";
            // 
            // maxRangeLabel
            // 
            maxRangeLabel.AutoSize = true;
            maxRangeLabel.Location = new System.Drawing.Point(16, 119);
            maxRangeLabel.Name = "maxRangeLabel";
            maxRangeLabel.Size = new System.Drawing.Size(65, 13);
            maxRangeLabel.TabIndex = 8;
            maxRangeLabel.Text = "Max Range:";
            // 
            // colorLabel
            // 
            colorLabel.AutoSize = true;
            colorLabel.Location = new System.Drawing.Point(16, 145);
            colorLabel.Name = "colorLabel";
            colorLabel.Size = new System.Drawing.Size(34, 13);
            colorLabel.TabIndex = 10;
            colorLabel.Text = "Color:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(16, 38);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(21, 13);
            iDLabel.TabIndex = 11;
            iDLabel.Text = "ID:";
            // 
            // dataSet2
            // 
            this.dataSet2.DataSetName = "DataSet2";
            this.dataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // firearmsBindingSource
            // 
            this.firearmsBindingSource.DataMember = "Firearms";
            this.firearmsBindingSource.DataSource = this.dataSet2;
            // 
            // firearmsTableAdapter
            // 
            this.firearmsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.FirearmsTableAdapter = this.firearmsTableAdapter;
            this.tableAdapterManager.UpdateOrder = TerrainOptimalCoverage.DataSet2TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // firearmsBindingNavigator
            // 
            this.firearmsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.firearmsBindingNavigator.BindingSource = this.firearmsBindingSource;
            this.firearmsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.firearmsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.firearmsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.firearmsBindingNavigatorSaveItem});
            this.firearmsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.firearmsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.firearmsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.firearmsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.firearmsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.firearmsBindingNavigator.Name = "firearmsBindingNavigator";
            this.firearmsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.firearmsBindingNavigator.Size = new System.Drawing.Size(607, 25);
            this.firearmsBindingNavigator.TabIndex = 0;
            this.firearmsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // firearmsBindingNavigatorSaveItem
            // 
            this.firearmsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.firearmsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("firearmsBindingNavigatorSaveItem.Image")));
            this.firearmsBindingNavigatorSaveItem.Name = "firearmsBindingNavigatorSaveItem";
            this.firearmsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.firearmsBindingNavigatorSaveItem.Text = "Save Data";
            this.firearmsBindingNavigatorSaveItem.Click += new System.EventHandler(this.firearmsBindingNavigatorSaveItem_Click);
            // 
            // firearmsDataGridView
            // 
            this.firearmsDataGridView.AutoGenerateColumns = false;
            this.firearmsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.firearmsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.Height});
            this.firearmsDataGridView.DataSource = this.firearmsBindingSource;
            this.firearmsDataGridView.Location = new System.Drawing.Point(12, 177);
            this.firearmsDataGridView.Name = "firearmsDataGridView";
            this.firearmsDataGridView.Size = new System.Drawing.Size(575, 190);
            this.firearmsDataGridView.TabIndex = 1;
            // 
            // fNameTextBox
            // 
            this.fNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.firearmsBindingSource, "FName", true));
            this.fNameTextBox.Location = new System.Drawing.Point(87, 64);
            this.fNameTextBox.Name = "fNameTextBox";
            this.fNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.fNameTextBox.TabIndex = 5;
            // 
            // minRangeTextBox
            // 
            this.minRangeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.firearmsBindingSource, "MinRange", true));
            this.minRangeTextBox.Location = new System.Drawing.Point(87, 90);
            this.minRangeTextBox.Name = "minRangeTextBox";
            this.minRangeTextBox.Size = new System.Drawing.Size(100, 20);
            this.minRangeTextBox.TabIndex = 7;
            // 
            // maxRangeTextBox
            // 
            this.maxRangeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.firearmsBindingSource, "MaxRange", true));
            this.maxRangeTextBox.Location = new System.Drawing.Point(87, 116);
            this.maxRangeTextBox.Name = "maxRangeTextBox";
            this.maxRangeTextBox.Size = new System.Drawing.Size(100, 20);
            this.maxRangeTextBox.TabIndex = 9;
            // 
            // colorTextBox
            // 
            this.colorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.firearmsBindingSource, "Color", true));
            this.colorTextBox.Location = new System.Drawing.Point(87, 142);
            this.colorTextBox.Name = "colorTextBox";
            this.colorTextBox.Size = new System.Drawing.Size(100, 20);
            this.colorTextBox.TabIndex = 11;
            // 
            // iDLabel1
            // 
            this.iDLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.firearmsBindingSource, "ID", true));
            this.iDLabel1.Location = new System.Drawing.Point(84, 38);
            this.iDLabel1.Name = "iDLabel1";
            this.iDLabel1.Size = new System.Drawing.Size(100, 23);
            this.iDLabel1.TabIndex = 12;
            this.iDLabel1.Text = "label1";
            // 
            // heightLabel
            // 
            heightLabel.AutoSize = true;
            heightLabel.Location = new System.Drawing.Point(206, 67);
            heightLabel.Name = "heightLabel";
            heightLabel.Size = new System.Drawing.Size(41, 13);
            heightLabel.TabIndex = 12;
            heightLabel.Text = "Height:";
            // 
            // heightTextBox
            // 
            this.heightTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.firearmsBindingSource, "Height", true));
            this.heightTextBox.Location = new System.Drawing.Point(253, 64);
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(100, 20);
            this.heightTextBox.TabIndex = 13;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FName";
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MinRange";
            this.dataGridViewTextBoxColumn3.HeaderText = "Min Range";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MaxRange";
            this.dataGridViewTextBoxColumn4.HeaderText = "Max Range";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Color";
            this.dataGridViewTextBoxColumn5.HeaderText = "Color";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // Height
            // 
            this.Height.DataPropertyName = "Height";
            this.Height.HeaderText = "Height";
            this.Height.Name = "Height";
            // 
            // FrmFirearms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 387);
            this.Controls.Add(heightLabel);
            this.Controls.Add(this.heightTextBox);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDLabel1);
            this.Controls.Add(fNameLabel);
            this.Controls.Add(this.fNameTextBox);
            this.Controls.Add(minRangeLabel);
            this.Controls.Add(this.minRangeTextBox);
            this.Controls.Add(maxRangeLabel);
            this.Controls.Add(this.maxRangeTextBox);
            this.Controls.Add(colorLabel);
            this.Controls.Add(this.colorTextBox);
            this.Controls.Add(this.firearmsDataGridView);
            this.Controls.Add(this.firearmsBindingNavigator);
            this.Name = "FrmFirearms";
            this.Text = "Manage Firearms";
            this.Load += new System.EventHandler(this.FrmFirearms_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firearmsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firearmsBindingNavigator)).EndInit();
            this.firearmsBindingNavigator.ResumeLayout(false);
            this.firearmsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firearmsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet2 dataSet2;
        private System.Windows.Forms.BindingSource firearmsBindingSource;
        private DataSet2TableAdapters.FirearmsTableAdapter firearmsTableAdapter;
        private DataSet2TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator firearmsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton firearmsBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView firearmsDataGridView;
        private System.Windows.Forms.TextBox fNameTextBox;
        private System.Windows.Forms.TextBox minRangeTextBox;
        private System.Windows.Forms.TextBox maxRangeTextBox;
        private System.Windows.Forms.TextBox colorTextBox;
        private System.Windows.Forms.Label iDLabel1;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Height;
    }
}