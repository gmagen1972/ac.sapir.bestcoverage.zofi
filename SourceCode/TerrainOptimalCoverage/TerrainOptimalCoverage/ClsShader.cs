﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerrainOptimalCoverage
{
    public class ClsShader
    {
        struct PalNHeight
        {
            public byte r, g, b;
            public int Height;
        }

        public struct PalColor
        {
            public byte r, g, b;
        }

        public enum ShaderMode
        {
            Swipe,
            NoSwipe
        }

        PalNHeight[] _Pal1;
        string[] FileRaw;
        public PalColor[] ColorArr;
        int _MinHeight,_MinAbs;
        string PalFile="None";

        public void ThinLineEachLayersEvery(int Meters=50,byte width=5,byte r=0,byte g=0,byte b=0)
        {
            for (int c = 0; c < ColorArr.Length; c+=Meters)
            {

                for (int x = 0; x < width; x++)
                {
                    ColorArr[c+x].r = r;
                    ColorArr[c+x].g = g;
                    ColorArr[c+x].b = b;
                }

            }
        }

        public ClsShader(string File, ShaderMode Mode = ShaderMode.Swipe)
        {
            FileRaw = System.IO.File.ReadAllLines(File);
            PalFile = File;
            int c = FileRaw.Length;
            _Pal1 = new PalNHeight[c-1];
            int x;

                try
                {
                    for (x = 0; x < c - 1; x++)
                    {
                        string _T;
                        string[] __T;
                        _T = FileRaw[x + 1];
                        __T = _T.Split();
                        _Pal1[x].Height = Convert.ToInt16(__T[0].Replace(".000000", ""));
                        //_Pal1[x].Height = Convert.ToInt16(__T[0]);
                        _Pal1[x].r = Convert.ToByte(__T[1]);
                        _Pal1[x].g = Convert.ToByte(__T[2]);
                        _Pal1[x].b = Convert.ToByte(__T[3]);
                    }

                    if (Mode == ShaderMode.Swipe) GenShaderFullArray();
                    if (Mode == ShaderMode.NoSwipe) GenShaderNoSwipe();
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Error while loading " + File);
                }
        }


        private void GenShaderNoSwipe()
        {
            //Calculate Max Height
            int AbsoluteHeight = _Pal1[_Pal1.Length - 1].Height - _Pal1[0].Height;
            ColorArr = new PalColor[AbsoluteHeight + 1];
            _MinAbs = Math.Abs(_Pal1[0].Height);
            _MinHeight = _Pal1[0].Height;

            for (int c = 0; c < _Pal1.Length - 1; c++)
            {
                ColorArr[c].r = _Pal1[c].r;
                ColorArr[c].g = _Pal1[c].g;
                ColorArr[c].b = _Pal1[c].b;

            }
        }

        public ClsShader(string File)
        {
            PalFile = File;
            FileRaw = System.IO.File.ReadAllLines(File);
            int c = FileRaw.Length;
            _Pal1 = new PalNHeight[c - 1];
            int x;
            try
            {
                for (x = 0; x < c - 1; x++)
                {
                    string _T;
                    string[] __T;
                    _T = FileRaw[x + 1];
                    __T = _T.Split();
                    _Pal1[x].Height = Convert.ToInt16(__T[0].Replace(".000000", ""));
                    //_Pal1[x].Height = Convert.ToInt16(__T[0]);
                    _Pal1[x].r = Convert.ToByte(__T[1]);
                    _Pal1[x].g = Convert.ToByte(__T[2]);
                    _Pal1[x].b = Convert.ToByte(__T[3]);
                }

                GenShaderFullArray();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Error while loading " + File);
            }
        }

        public ClsShader(int HeightMin, int HeightMax, byte rmin, byte gmin, byte bmin, byte rmax, byte gmax, byte bmax)
        {
            _Pal1 = new PalNHeight[2];

            _Pal1[0].Height = HeightMin;
            _Pal1[0].r = rmin;
            _Pal1[0].g = gmin;
            _Pal1[0].b = bmin;

            _Pal1[1].Height = HeightMax;
            _Pal1[1].r = rmax;
            _Pal1[1].g = gmax;
            _Pal1[1].b = bmax;

            GenShaderFullArray();
        }

        public int GetMinHeight()
        {
            return _MinHeight;
        }

        public int GetMinHeightAbs()
        {
            return _MinAbs;
        }

        public string GetPalFileName()
        {
            return PalFile;
        }

        private void GenShaderFullArray()
        {
            //Calculate Max Height
            int AbsoluteHeight = _Pal1[_Pal1.Length-1].Height-_Pal1[0].Height;
            ColorArr = new PalColor[AbsoluteHeight+1];
            float sr,sg,sb;
            _MinAbs = Math.Abs( _Pal1[0].Height);
            _MinHeight = _Pal1[0].Height;

            for (int c = 1; c <= _Pal1.Length - 1; c++)
            {
                float rpace, gpace, bpace;

                rpace = (float)(_Pal1[c].r - _Pal1[c - 1].r) / (_Pal1[c].Height - _Pal1[c - 1].Height);
                gpace = (float)(_Pal1[c].g - _Pal1[c - 1].g) / (_Pal1[c].Height - _Pal1[c - 1].Height);
                bpace = (float)(_Pal1[c].b - _Pal1[c - 1].b) / (_Pal1[c].Height - _Pal1[c - 1].Height);

                sr=_Pal1[c - 1].r;
                sg=_Pal1[c - 1].g;
                sb=_Pal1[c - 1].b;

                for (int d = _Pal1[c - 1].Height; d < _Pal1[c].Height; d++)
                {
                    ColorArr[d - _MinHeight].r = Convert.ToByte(sr);
                    ColorArr[d - _MinHeight].g = Convert.ToByte(sg);
                    ColorArr[d - _MinHeight].b = Convert.ToByte(sb);

                    sr += rpace;
                    sg += gpace;
                    sb += bpace;
                }
            }


        }
    }
}
