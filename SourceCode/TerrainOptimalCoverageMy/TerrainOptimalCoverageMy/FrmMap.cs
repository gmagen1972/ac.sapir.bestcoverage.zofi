﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
//using System.Windows.Forms.DataVisualization.Charting;

namespace TerrainOptimalCoverage
{
    public partial class FrmMap : Form
    {

        float HeightMin, HeightMax;
        float ScaleHeightTo = 500;
        private float CellSize;
        private int TerrainWidth;
        private int TerrainHeight;
        public float[,] heightData;
        int x1;
        int y1 ;
        int x2;
        int y2;
        float degel=2;
        int x = 0;
        int y = 0;
        int[] XYarrayAscPosition=new int[4];
        int[] HightPointsBetweenLine;
        float[] AscArraySideCat;
        bool FlagMatrix = false;
        bool FlagDrawLine = false;
        int[] rectDrawAraay=new int[] {-10,-10,-10,-10};
        public FrmMap()
        {
            InitializeComponent();
        }

        private void FrmMap_Load(object sender, EventArgs e)
        {
            string strPathImg=Application.StartupPath + "\\Maps\\";
            string strFileImg = Application.StartupPath + "\\Maps\\" + "HeightMap.jpg";
            string strPathAsc=Application.StartupPath + "\\Maps\\";
            string strFileAsc = Application.StartupPath + "\\Maps\\" + "HeightMap.asc";
            string strDate = DateTime.Now.ToString();

            chart1.Visible = false;
            toolStripSideCat.Enabled = false;
            toolStripButtonBrush.Enabled = false;
            toolStripButtonShowMap.Enabled = false;
            strDate = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString(); 

            strPathImg = strPathImg + "HeightMap" + strDate + ".jpg";
            strPathAsc = strPathAsc + "HeightMap"+ strDate + ".asc";
            System.IO.File.Move(strFileAsc, strPathAsc);
            System.IO.File.Move(strFileImg, strPathImg);

            
            pictureBox1.Image = Bitmap.FromFile(strPathImg);
            pictureBox1.Top = 25;
            pictureBox1.Left = 0;
           

            this.Size = new System.Drawing.Size(pictureBox1.Size.Width, pictureBox1.Size.Height+50); 
            this.Text="HeightMap" + strDate + ".asc"; //form name
            LoadHeightDataAscFile(strPathAsc, ScaleHeightTo);
        }

        private void LoadHeightDataAscFile(string file, float ScaleTo)
        {
            //terrainWidth =  heightMap.Width;
            //terrainHeight =  heightMap.Height;
            int x = 0, y = 0;
            float __T;
            string[] T = System.IO.File.ReadAllLines(file);//);
            string _T = T[0].Replace("ncols", "");
            _T = _T.Replace(" ", "");
            TerrainWidth =  Convert.ToInt16(_T);
            _T = T[1].Replace("nrows", "");
            _T = _T.Replace(" ", "");
            TerrainHeight =Convert.ToInt16(_T);
            _T = T[4].Replace("cellsize", "");
            _T = _T.Replace(" ", "");
            CellSize = Convert.ToSingle(_T);

            heightData = new float[TerrainWidth, TerrainHeight];

            //LoadAddData

            for (y = 0; y < TerrainHeight; y++)
            {
                string[] _x = T[6 + y].Split();
                for (x = 0; x < TerrainWidth; x++)
                {
                    __T = Convert.ToSingle(_x[x]);
                    if (__T != -9999F) heightData[x, y] = __T;
                    if ((x == 0) && (y == 0))
                    {
                        HeightMax = heightData[x, y];
                        HeightMin = heightData[x, y];
                    }
                    HeightMin = Math.Min(heightData[x, y], HeightMin);
                    HeightMax = Math.Max(heightData[x, y], HeightMax);
                }
            }

            //reduce minimum and Scale to 1024
            for (x = 0; x < TerrainHeight; x++)
            {
                string[] _x = T[6 + x].Split();
                for (y = 0; y < TerrainWidth; y++)
                {
                    heightData[x, y] = Convert.ToSingle(_x[y]); //* ScaleHeightTo / (HeightMax - HeightMin) - HeightMin;

                }
            }

        }

        private void toolStripSideCat_Click(object sender, EventArgs e)
        {
            chart1.Visible = false;
            CalculateKavYashar();
            this.Width = 1000;
            CallGrafChart();
            toolStripButtonShowMap.Enabled = true;
            toolStripSideCat.Enabled = false;
            toolStripButtonBrush.Enabled = false;
            FlagMatrix = false;
        }

        private void CallGrafChart()
        {
            //chart1.Palette
         
            chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            chart1.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height);
            chart1.Top = 25;
            chart1.Left = 0;
            int index=0;
            // chart1.PreferredSize.Width  
            for (int i = 0; i < AscArraySideCat.Length; i++)
            {
            
                System.Windows.Forms.DataVisualization.Charting.Series series = chart1.Series.Add(i.ToString());
                series.Points.Add(AscArraySideCat[i]);
                index++;
            }
            chart1.Visible = true;       
        }

        private void CalculateKavYashar()
        {
            double _x1, _y1, _x2, _y2; //   בעולם ההוקטור שאנו מכירים X,Y מייצג את האינדקסים במטריצת 
            double  _y, a, b;
            int z;
            HightPointsBetweenLine = new int[Math.Abs((XYarrayAscPosition[0] - XYarrayAscPosition[2])*2)+2];
            _x1=XYarrayAscPosition[0];
            _x2=XYarrayAscPosition[2];
            //לוקטר כפי שאנחנו רגילים מהמציאות למציאת נקודות על הקו הישר y הפיכת ה-
            _y1=TerrainHeight-XYarrayAscPosition[1]-1;
            _y2=TerrainHeight-XYarrayAscPosition[3]-1;
           // if (_x1 == _x2)
            //    _x2=_x2+1;
            //else
            b = (_y1 - _y2) / (_x1 - _x2);
            a = _y1 - b * _x1;
            z = 0;

            if (_x1 < _x2)
            {
                for (double i = _x1; i <= _x2; i++)
                {
                    _y = a + b * i;
                    HightPointsBetweenLine[z] =Convert.ToInt16( i);    //for x
                    z++;
                    HightPointsBetweenLine[z] = Convert.ToInt16(_y);    //for y;
                    z++;
                }
            }
            else
            {
                z=Math.Abs(XYarrayAscPosition[0] - XYarrayAscPosition[2])*2+1;
                for (double i = _x2; i <= _x1; i++)
                {
                    _y = a + b * i;
                    HightPointsBetweenLine[z] = Convert.ToInt16(_y);    //for y;
                   z--;
                    HightPointsBetweenLine[z] = Convert.ToInt16(i);    //for x
                    z--;
                   ;
                }

            }

            ///  האמיתיים כפי שהם מוצגים במערך האמיתי yנשחזר רק את ה
            for (int i = 1; i <=HightPointsBetweenLine.Length ; i = i + 2)
            {
                HightPointsBetweenLine[i] = TerrainHeight - HightPointsBetweenLine[i] - 1;   
            }




         ///   
            AscArraySideCat = new float[HightPointsBetweenLine.Length/2];
            //int w = Math.Abs((XYarrayAscPosition[0] - XYarrayAscPosition[2])) ;
            int LocalX;
            int LocalY;
            int index = 0;
            for (int i = 0; index < HightPointsBetweenLine.Length / 2; i=i+2)
            {
                 LocalX=HightPointsBetweenLine[i];
                 LocalY=HightPointsBetweenLine[i + 1];
                 if (LocalY == TerrainHeight)
                     LocalY = TerrainHeight - 1;
                 if (LocalX == TerrainWidth)
                     LocalX = TerrainWidth - 1;

                 AscArraySideCat[index] = heightData[LocalX, LocalY];
                 index++;
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            FlagDrawLine = false;
            x = Cursor.Position.X; ;
            y = Cursor.Position.Y; ; 
            
            if (degel % 2 == 0)
            {
                x1 = x- this.Left - pictureBox1.Left - 4;
                y1 = y- this.Top - pictureBox1.Top - 92;
                toolStripLabelX.Text = "X=" + x1;
                toolStripLabelY.Text = "Y=" + y1;
                CalculatePositionScreenInAscArray(x1, y1, false);
                rectDrawAraay[0] = x1;// -this.Left - pictureBox1.Left - 4;
                rectDrawAraay[1] = y1;// -this.Left - pictureBox1.Left - 92;
                pictureBox1.Refresh();
                DrawRect();
            }
            else
            {
                x2 = x- this.Left - pictureBox1.Left - 4;
                y2 = y- this.Top - pictureBox1.Top - 92;
                toolStripLabelX.Text = "X=" + x2;
                toolStripLabelY.Text = "Y=" + y2;
                CalculatePositionScreenInAscArray(x2, y2, true);
                toolStripButtonBrush.Enabled = true;
                rectDrawAraay[2] = x2;// -this.Left - pictureBox1.Left - 4;
                rectDrawAraay[3] = y2;// -this.Left - pictureBox1.Left - 92;
                pictureBox1.Refresh();
                DrawRect();
            }
            degel++;
        }


        //*******************************SideCat*******************************************************//
        private void toolStripButtonBrush_Click(object sender, EventArgs e)
        {
           
                FlagDrawLine = true;
               //draw line between 2 points
                 DrawLineBetweenTwoPointsOnTerrian();
                //pictureBox1.Image.Save("c:\\temp\\stam.jpg");
                toolStripSideCat.Enabled = true;
        }

        private void CalculatePositionScreenInAscArray(int x_pos,int y_pos,bool ClickNum)
        { 
           
            if (ClickNum == false)
            {
                 XYarrayAscPosition[0] = Convert.ToInt16( Convert.ToDouble(x_pos) / ((Convert.ToDouble(pictureBox1.Width) / Convert.ToDouble(TerrainWidth)))); 
                 XYarrayAscPosition[1] = Convert.ToInt16(Convert.ToDouble(y_pos) / ((Convert.ToDouble(pictureBox1.Height) / Convert.ToDouble(TerrainHeight))));
            }
            else
            {
                XYarrayAscPosition[2] = Convert.ToInt16(Convert.ToDouble(x_pos) / ((Convert.ToDouble(pictureBox1.Width) / Convert.ToDouble(TerrainWidth))));
                XYarrayAscPosition[3] = Convert.ToInt16(Convert.ToDouble(y_pos) / ((Convert.ToDouble(pictureBox1.Height) / Convert.ToDouble(TerrainHeight))));
            }


        }

        private void DrawLineBetweenTwoPointsOnTerrian()
        {
            Pen selPen = new Pen(Color.Red);
            Graphics graphic = this.pictureBox1.CreateGraphics();
            Point coord1 = new Point(rectDrawAraay[0],rectDrawAraay[1]);
            Point coord2 = new Point(rectDrawAraay[2], rectDrawAraay[3]);
            selPen.Width = 3;
            graphic.DrawLine(selPen, coord1, coord2);
            degel = 2;
        }

        private void DrawRect()
        {
            Graphics GR;
            Pen PT;
            PT = new Pen(Color.Black, 2);
            GR = pictureBox1.CreateGraphics();
          if (degel % 2 == 0)
            {
                rectDrawAraay[0] = x1;// -this.Left - pictureBox1.Left - 4;
                rectDrawAraay[1] = y1;//- this.Top - pictureBox1.Top - 92;
            }
            else
            {
                rectDrawAraay[2] = x2;//- this.Left - pictureBox1.Left - 4;
                rectDrawAraay[3] = y2;//- this.Top - pictureBox1.Top - 92;
            }

            GR.DrawRectangle(PT, rectDrawAraay[0] - 10, rectDrawAraay[1] - 10, 20, 20);
            GR.DrawRectangle(PT, rectDrawAraay[2] - 10, rectDrawAraay[3] - 10, 20, 20);
        }


        private void toolStripButtonShowMapTransparent_Click(object sender, EventArgs e)
        {
            chart1.Visible = false;
            chart1.Series.Clear();
            this.Size = new System.Drawing.Size(pictureBox1.Size.Width, pictureBox1.Size.Height + 50);
            toolStripButtonBrush.Enabled = false;
            toolStripSideCat.Enabled = false;
            toolStripButtonShowMap.Enabled = false;
            rectDrawAraay[0]= - 10;
            rectDrawAraay[1] = -10;
            rectDrawAraay[2] = -10;
            rectDrawAraay[3] = -10;

        }

        private void toolStripButtonShowMatrix_Click(object sender, EventArgs e)
        {
            if (FlagMatrix == false)
            {
                FlagMatrix = true;
                pictureBox1.Refresh();
                DrawMatrix();
                DrawRect();
                if(FlagDrawLine ==true)
                DrawLineBetweenTwoPointsOnTerrian();
            }
            else
            {
                FlagMatrix = false;
                pictureBox1.Refresh();
                DrawRect();
                if (FlagDrawLine == true)
                DrawLineBetweenTwoPointsOnTerrian();
            }
            
        }

        private void DrawMatrix()
        {
           
                int SumCol = 0;
                int SumRow = 0;
                Pen selPen = new Pen(Color.LightGray);
                Graphics graphic = this.pictureBox1.CreateGraphics();
                selPen.Width = 1;
                Point coord1;
                Point coord2;
                for (int i = 0; i < TerrainWidth; i++)
                {
                    SumCol = SumCol + pictureBox1.Width / TerrainWidth;
                    SumRow = SumRow + pictureBox1.Height / TerrainHeight;
                    coord1 = new Point(SumCol, 0);
                    coord2 = new Point(SumCol, pictureBox1.Height);
                    graphic.DrawLine(selPen, coord1, coord2);

                    coord1 = new Point(0, SumRow);
                    coord2 = new Point(pictureBox1.Width, SumRow);
                    graphic.DrawLine(selPen, coord1, coord2);
                }

            }
        
        private void FrmMap_LocationChanged(object sender, EventArgs e)
        {
            //if (Flag)
            //{
            //    pictureBox1.Refresh();
            //   // DrawRect();
            //    DrawMatrix();
               
            //}
        }

       
          
     
    }
}
